#!/usr/bin/env python3

import sqlite3, pathlib, datetime

from compile import load_md

TAX = {
    "artists": "artist",
    "formats": "format",
    "genres": "genre",
    "languages": "language",
    "media": "medium",
    "places": "setting",
    "provenances": "provenance",
    "years": "year",
}

MEDIA_FORMATS = {
    "book": {
        "collection",
        "comic",
        "companion book",
        "cookbook",
        "graphic novel",
        "handbook",
        "novel",
        "novelette",
        "novella",
        "rulebook",
        "short story",
        "sourcebook",
        "triptych",
        "usage dictionary",
    },
    "game": {
        "video game",
        "computer game",
        "puzzle",
        "miniature wargame",
        "tabletop",
        "card game",
        "board game",
        "role-playing game",
        "interactive fiction",
    },
    "video": {
        "documentary",
        "film",
        "miniseries",
        "television",
    },
    "web": {
        "blog",
        "digital encyclopædia",
        "essay",
        "serial",
        "website",
        "wiki",
    },
}


def date2timestamp(date):
    return int(datetime.datetime.combine(date, datetime.time()).timestamp())


def process_entry(con, entry_path, table):
    data = load_md(entry_path)

    meta = data["meta"]
    titles = meta["titles"]

    name = titles[0]

    name = name.replace("’", "'")

    body = data["html"]

    if "description" in meta:
        body = f"*{meta['description']}*\n\n" + body

    body = (
        body.replace("’", "'")
        .replace("“", '"')
        .replace("”", '"')
        .replace("–", "--")
        .replace("—", "---")
    )

    try:
        added = date2timestamp(meta["dates"]["published"])
        cur.execute(
            f"INSERT INTO {table} (name, body, added) VALUES (?, ?, ?)",
            (name, body, added),
        )
    except KeyError:
        cur.execute(f"INSERT INTO {table} (name, body) VALUES (?, ?)", (name, body))

    id = cur.lastrowid

    for alias in titles[1:]:
        alias = alias.replace("’", "'")
        cur.execute(
            f"INSERT INTO {table}_alias ({table}_id, name) VALUES (?, ?)", (id, alias)
        )

    try:
        for link in meta["links"]:
            cur.execute(
                f"INSERT INTO {table}_link ({table}_id, url) VALUES (?, ?)", (id, link)
            )
    except KeyError:
        pass

    if "dates" in meta:
        dates = meta["dates"]
        if "experienced" in dates:
            for e in dates["experienced"]:
                cur.execute("INSERT INTO experience (entry_id) VALUES (?)", (id,))
                eid = cur.lastrowid

                if e == "?":
                    cur.execute("INSERT INTO complete (id) VALUES (?)", (eid,))
                else:
                    started = date2timestamp(e["started"])
                    cur.execute(
                        "INSERT INTO playpause (experience_id, timestamp) VALUES (?, ?)",
                        (eid, started),
                    )

                    if "finished" in e:
                        finished = date2timestamp(e["finished"])
                        cur.execute(
                            "INSERT INTO playpause (experience_id, timestamp) VALUES (?, ?)",
                            (eid, finished),
                        )
                        cur.execute("INSERT INTO complete (id) VALUES (?)", (eid,))
                    else:
                        if "completed" in meta and meta["completed"]:
                            cur.execute("INSERT INTO complete (id) VALUES (?)", (eid,))

                        if not ("active" in meta and meta["active"]):
                            cur.execute(
                                "INSERT INTO playpause (experience_id) VALUES (?)",
                                (eid,),
                            )

    try:
        for tax, lst in meta["taxonomies"].items():
            if tax == "media":
                continue

            tt = TAX[tax]
            for tag in lst:
                if tax == "years":
                    if tag == "unreleased":
                        continue
                    tag = int(tag)

                res = cur.execute(f"SELECT id from {tt} WHERE name = ?", (tag,))
                fet = res.fetchone()
                if fet is None:
                    cur.execute(f"INSERT INTO {tt} (name) VALUES (?)", (tag,))
                    cid = cur.lastrowid
                else:
                    cid = fet[0]

                cur.execute(
                    f"INSERT INTO entry_{tt} (entry_id, {tt}_id) VALUES (?, ?)",
                    (id, cid),
                )
    except KeyError:
        pass

    try:
        for col in meta["collections"]:
            cname = col["name"].replace("’", "'")

            if (
                fet := cur.execute(
                    "SELECT id from collection WHERE name = ?", (cname,)
                ).fetchone()
            ) is None:
                cur.execute(
                    "INSERT INTO collection (name) VALUES (?)",
                    (cname,),
                )
                gid = cur.lastrowid
            else:
                gid = fet[0]

            cur.execute(
                "INSERT INTO entry_collection (collection_id, entry_id) VALUES (?, ?)",
                (gid, id),
            )
            rid = cur.lastrowid

            num = col["number"]

            cur.execute(
                "INSERT INTO entry_ordinal (id, number) VALUES (?, ?)",
                (rid, num),
            )
    except KeyError:
        pass


if __name__ == "__main__":
    con = sqlite3.connect("stories.db")
    cur = con.cursor()

    with open("../stories2/data/schema.sql", "r") as f:
        schema = f.read()

    cur.executescript(schema)

    for medium, formats in MEDIA_FORMATS.items():
        cur.execute("INSERT INTO medium (name) VALUES (?)", (medium,))
        mid = cur.lastrowid

        for format in formats:
            cur.execute("INSERT INTO format (name) VALUES (?)", (format,))
            fid = cur.lastrowid

            cur.execute(
                "INSERT INTO format_medium (format_id, medium_id) VALUES (?, ?)",
                (fid, mid),
            )

    content = pathlib.Path("content")

    for entry in content.glob("collections/*.md"):
        process_entry(con, entry, "collection")

    for entry in content.glob("entries/*.md"):
        process_entry(con, entry, "entry")

    con.commit()
