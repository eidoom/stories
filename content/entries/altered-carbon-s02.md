---
titles:
- Altered Carbon S02
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - speculative fiction
  - science fiction
  - cyberpunk
  - mystery
  - action
  - adventure
  - dystopian
  - social divide
  years:
  - 2020
collections:
- name: Altered Carbon
  number: 1.2
- name: Altered Carbon (TV)
  number: 2
dates:
  published: 2020-12-05
  updated: 2022-08-28
  experienced:
  - started: 2020-02-27
    finished: 2020-02-29
links:
- https://en.wikipedia.org/wiki/Altered_Carbon_(TV_series)
---
