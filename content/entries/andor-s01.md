---
titles:
- Andor S01
taxonomies:
  media:
  - video
  formats:
  - television
  years:
  - 2022
collections:
- name: Andor
  number: 1
- name: Star Wars
  number: 3.8
dates:
  published: 2022-10-09
  updated: 2022-11-24
  experienced:
  - started: 2022-10-11
    finished: 2022-11-23
links:
- https://en.wikipedia.org/wiki/Andor_(TV_series)
---
