---
titles:
- 'Spider-Man: No Way Home'
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2021
collections:
- name: Spider-Man
  number: 12
- name: MCU Spider-Man
  number: 3
- name: Marvel Cinematic Universe
  number: 27
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-03-12
    finished: 2022-03-12
links:
- https://en.wikipedia.org/wiki/Spider-Man:_No_Way_Home
---

