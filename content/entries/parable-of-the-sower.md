---
titles:
- Parable of the Sower
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1993
  artists:
  - Octavia E. Butler
dates:
  published: 2022-12-10
  updated: 2023-04-17
  experienced:
  - started: 2023-04-17
links:
- https://en.wikipedia.org/wiki/Parable_of_the_Sower_(novel)
---
