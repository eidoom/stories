---
titles:
- Fargo S04
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - black comedy
  - crime
  - anthology
  - thriller
  - drama
  years:
  - 2020
collections:
- name: Fargo
  number: 4
dates:
  published: 2021-01-06
  updated: 2022-08-28
  experienced:
  - started: 2021-02-02
    finished: 2021-02-05
links:
- https://en.wikipedia.org/wiki/Fargo_(season_4)
---

In the 1950s, the newly arrived Cannons, a Black crime family, come to blows with the established Fadda Italian mafia.

A strong season of Fargo.
