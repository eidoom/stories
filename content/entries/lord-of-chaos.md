---
titles:
- Lord of Chaos
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1994
  artists:
  - Robert Jordan
collections:
- name: The Wheel of Time
  number: 6
- name: The Wheel of Time universe
  number: 6
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
