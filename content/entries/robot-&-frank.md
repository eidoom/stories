---
titles:
- Robot & Frank
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - science fiction
  - comedy
  - drama
  - robot
  - speculative fiction
  - fiction
  years:
  - 2012
dates:
  published: 2021-04-25
  updated: 2021-04-25
  experienced:
  - started: 2021-04-25
    finished: 2021-04-25
description: A bittersweet little story
links:
- https://en.wikipedia.org/wiki/Robot_%26_Frank
---

An elderly man struggles to accept his dementia.
He is forced to come to terms with his condition when he must wipe the memory of his healthcare robot—his “friend”—to destroy evidence implicating him in a crime.
