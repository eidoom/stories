---
titles:
- The Time of Contempt
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1995
  - 2013
  artists:
  - Andrzej Sapkowski
  - David French
collections:
- name: The Witcher Saga
  number: 2
- name: The Witcher
  number: 4
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Time_of_Contempt
---
