---
titles:
- Broken Angels
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2003
  artists:
  - Richard K. Morgan
collections:
- name: Takeshi Kovacs
  number: 2
- name: Altered Carbon
  number: 2
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Broken_Angels_(novel)
---
