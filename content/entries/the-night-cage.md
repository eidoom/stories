---
titles:
- The Night Cage
taxonomies:
  media:
  - game
  formats:
  - board game
  years:
  - 2021
dates:
  published: 2022-04-30
  updated: 2022-05-06
  experienced:
  - started: 2022-04-29
    finished: 2022-04-29
  - started: 2022-08-10
    finished: 2022-08-10
links:
- https://www.thenightcage.com/
- https://boardgamegeek.com/boardgame/306709/night-cage
---

I played with five players.
