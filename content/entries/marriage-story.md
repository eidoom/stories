---
titles:
- Marriage Story
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - drama
  years:
  - 2019
dates:
  published: 2021-09-03
  updated: 2021-09-03
  experienced:
  - started: 2021-09-03
    finished: 2021-09-03
description: Divorce
links:
- https://en.wikipedia.org/wiki/Marriage_Story
---

As we follow Nicole and Charlie through their divorce, we explore their failed relationship.
