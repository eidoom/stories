---
titles:
- Portal 2
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2011
collections:
- name: Portal
  number: 2
- name: Half-Life
  number: -1
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2011-04-28
completed: true
links:
- https://en.wikipedia.org/wiki/Portal_2
---

