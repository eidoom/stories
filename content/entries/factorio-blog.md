---
titles:
- Fun Friday Facts
taxonomies:
  media:
  - web
  formats:
  - blog
  years:
  - 2012
  artists:
  - Wube Software
dates:
  published: 2021-02-08
  updated: 2021-02-08
  experienced:
  - '?'
description: The Factorio blog
links:
- https://www.factorio.com/blog/
---

I enjoy following the [Factorio](factorio.html) development blog.
It’s fun to see the solutions they come up with for problems and hear their thoughts on designing the game.
