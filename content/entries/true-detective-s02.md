---
titles:
- True Detective S02
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - crime
  - drama
  - fiction
  - anthology
  - mystery
  - detective
  - neo-noir
  - nonchronological
  - nonlinear
  years:
  - 2015
collections:
- name: True Detective
  number: 2
dates:
  published: 2021-04-22
  updated: 2022-08-28
  experienced:
  - started: 2021-04-19
    finished: 2021-04-21
completed: true
links:
- https://en.wikipedia.org/wiki/True_Detective_(season_2)
---

A tale of the sordid corruption amongst leaders, exploitation of the poor, vigilante justice, an honourable gangster, and the soul-destroying burden of serving in the police.
We follow three cops: Velcoro, whose bosses are possibly less lawful than his unofficial boss, mobster Semyon; Bezzerides, damaged by a hippy commune upbringing and subsequent falling apart of her family; and Woodrugh, a young closeted veteran of a traumatic war.
It felt like we were only scraping the surface of the characters’ lives, giving the sense of a deep tapestry beneath the short story.
