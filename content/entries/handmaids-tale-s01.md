---
titles:
- The Handmaid’s Tale S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - speculative fiction
  - dystopian
  - tragedy
  - fiction
  years:
  - 2017
collections:
- name: The Handmaid’s Tale
  number: 1
dates:
  published: 2020-10-08
  updated: 2022-09-09
  experienced:
  - started: 2020-10-04
    finished: 2020-10-11
links:
- https://en.wikipedia.org/wiki/List_of_The_Handmaid%27s_Tale_episodes#Season_1_(2017)
---
