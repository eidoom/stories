---
titles:
- Misspent Youth
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2002
  artists:
  - Peter F. Hamilton
collections:
- name: Commonwealth universe
  number: 1
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
