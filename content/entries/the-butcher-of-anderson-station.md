---
titles:
- The Butcher of Anderson Station
taxonomies:
  media:
  - book
  formats:
  - novella
  years:
  - 2011
  artists:
  - James S. A. Corey
collections:
- name: The Expanse
  number: 1.5
- name: The Expanse (books)
  number: 1.5
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
