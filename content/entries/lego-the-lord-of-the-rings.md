---
titles:
- Lego The Lord of the Rings
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2013
collections:
- name: Middle-Earth
  number: 31.4
dates:
  published: 2022-04-21
  updated: 2022-04-21
  experienced:
  - started: 2022-04-20
links:
- https://en.wikipedia.org/wiki/Lego_The_Lord_of_the_Rings_(video_game)
---

