---
titles:
- 'Torment: Tides of Numenera'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2017
dates:
  published: 2022-05-13
  updated: 2022-05-13
links:
- https://en.wikipedia.org/wiki/Torment:_Tides_of_Numenera
---

