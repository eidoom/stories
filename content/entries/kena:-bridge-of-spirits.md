---
titles:
- 'Kena: Bridge of Spirits'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2021
  artists:
  - Ember Lab
dates:
  published: 2021-10-24
  updated: 2022-12-24
  experienced:
  - started: 2021-10-24
    finished: 2022-12-24
links:
- https://en.wikipedia.org/wiki/Kena:_Bridge_of_Spirits
---

