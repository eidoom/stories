---
titles:
- Assassin’s Creed
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2016
dates:
  published: 2021-05-09
  updated: 2021-05-09
  experienced:
  - started: 2021-05-09
    finished: 2021-05-09
description: Parkour+tin foil hats+pseudoscience
links:
- https://en.wikipedia.org/wiki/Assassin%27s_Creed_(film)
---

In a moment of weakness, I was taken by a bout of nostalgia for the games (I haven’t played any of them after 2010).
As one might imagine, it wasn’t the best film, although it did have fun parts.

Action sequences were exciting and there were pretty visuals galore.
Unfortunately, characters were zero dimensional with unhealthy stereotyping.

Eugenics and gene history were straight out of Frank Herbert’s fiction.
The background was Illuminati conspiracy theory.
