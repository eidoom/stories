---
titles:
- Blade Runner
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 1982
collections:
- name: Blade Runner
  number: 1
dates:
  published: 2021-10-25
  updated: 2021-10-25
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Blade_Runner
---

