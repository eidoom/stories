---
titles:
- The Hobbit
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2014
collections:
- name: Middle-Earth
  number: 30.1
dates:
  published: 2022-05-05
  updated: 2022-05-05
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Hobbit_(film_series)
---

