---
titles:
- Prince of the Spear
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  years:
  - 2018
  artists:
  - David Hair
collections:
- name: Sunsurge Quartet
  number: 2
- name: Mage’s Blood
  number: 6
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://davidhairauthor.com/Books/The-Sunsurge-Quartet/Prince
---
