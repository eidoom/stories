---
titles:
- The Legend of Vox Machina S03
taxonomies:
  media:
  - video
  formats:
  - television
  years:
  - unreleased
  genres:
  - fantasy
  - adventure
  - comedy drama
  - fantasy
  - adult animated
  provenances:
  - United States
  places:
  - Exandria
  - Tal'Dorei
  artists:
  - Ashley Johnson
  - Brandon Auman
  - Chris Prynoski
  - Critical Role Productions
  - Laura Bailey
  - Liam O'Brien
  - Marisha Ray
  - Matthew Mercer
  - Neal Acree
  - Sam Riegel
  - Taliesin Jaffe
  - Travis Willingham
  languages:
  - English
collections:
- name: The Legend of Vox Machina
  number: 3
- name: Critical Role
  number: 3
- name: Dungeons & Dragons
  number: 5.93
dates:
  published: 2023-03-29
  updated: 2023-03-29
#  experienced:
#  - started: 2023-02-13
#    finished: 2023-02-14
---

