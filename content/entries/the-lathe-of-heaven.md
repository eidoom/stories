---
titles:
- The Lathe of Heaven
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  - speculative fiction
  - fantasy
  - fiction
  years:
  - 1971
  artists:
  - Ursula K. Le Guin
dates:
  published: 2021-08-18
  updated: 2021-08-26
  experienced:
  - started: 2021-08-18
    finished: 2021-08-25
description: He falls down, wakes up, on the other side of the dream
links:
- https://en.wikipedia.org/wiki/The_Lathe_of_Heaven
---

What if dreams came true?
George Orr can have effective dreams, *iahklu’*, which change reality, and this terrifies him.
Only people there when he’s sleeping seem to be able to perceive the change, and even then their mind determinedly denies it.
He tries everything to not dream, for he feels it is not his place to alter the universe so.
After illegally trying drugs to suppress them, he is sentenced to appointments with a psychiatrist, Dr. Haber.

George accepts his place as part of the world, lives in his centre, in a Taoist way.
He is quietly strong.
Haber is arrogant and dominating, larger than life, an expression of more rationalist Western schools of thought.
However, it is through the balance and meeting of these two that George is finally “cured”—although Haber is ultimately broken by his hubris.

We psychologically and sociologically explore utilitarianism, gender, race, and the structure of power, in particular authoritarianism and the self-declared benevolent dictator.

Le Guin captures that perfect clarify and logic of dreams where one reality seamlessly segues into another, inconsistency a fable of the waking world, and distills it into this story.

The Alien have some [Odonian](the-dispossessed.html) philosophies.
They are also of the dream time like the [Athsheans](the-word-for-world-is-forest.html).

This book *must* have been an inspiration for [The Bridge](the-bridge.html).
Haber mistakenly calls his patient John Orr near the start, the Bridge name of The Bridge’s protagonist, and the Haber sequences remind me of John’s psychiatrist visits.
The whole dreamlike quality is another strong parallel.

I wonder if Haber’s Augmentor inspired the memory-manipulation technology of [Eternal Sunshine of the Spotless Mind](eternal-sunshine-of-the-spotless-mind.html).
The electroencephalogram stuff also stirred distant memories of Asimov’s work.
George Orr’s name is undoubtably given after George Orwell, whose influence is evident in dystopia and perhaps too in qualities of the character.

[^0]: “Il descend, réveillé, l’autre côté du rêve.” *Les contemplations*, Victor Hugo. Used as an epigraph to Chapter 10.
