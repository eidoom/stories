---
titles:
- Lyra’s Oxford
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2003
  artists:
  - Philip Pullman
collections:
- name: His Dark Materials
  number: 3.5
- name: Northern Lights
  number: 3.4
dates:
  published: 2019-12-27
  updated: 2021-07-30
  experienced:
  - started: 2019-12-27
    finished: 2019-12-28
links:
- https://en.wikipedia.org/wiki/Lyra%27s_Oxford
---
