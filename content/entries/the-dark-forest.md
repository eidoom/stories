---
titles:
- The Dark Forest
- 黑暗森林
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  - fiction
  - speculative fiction
  - hard sci-fi
  places:
  - China
  years:
  - 2015
  - 2008
  artists:
  - Joel Martinsen
  - Liu Cixin
collections:
- name: Remembrance of Earth’s Past
  number: 2
- name: Three-Body
  number: 2
dates:
  published: 2020-10-22
  updated: 2021-02-20
  experienced:
  - started: 2020-10-22
    finished: 2020-10-29
description: Cosmic game theory
links:
- https://en.wikipedia.org/wiki/The_Dark_Forest
---

In *The Dark Forest* (黑暗森林), the possible intra- and inter-relational behaviour of civilisations within the galaxy is explored.
We see how Earth reacts to the inevitability of the invasion of the technologically superior Trisolarans.
We see Trisolaris launch an immediate aggressive campaign to suppress human progress and conquer Earth.
Finally, “cosmic sociology” is distilled to [zero-sum](https://en.wikipedia.org/wiki/Game_theory#Zero-sum_/_non-zero-sum) [game theory](https://en.wikipedia.org/wiki/game_theory), where ever-expanding civilisations must suppress altruism to survive in a finite universe: the eponymous “dark forest” in metaphor.
This “chain of suspicion” is an alien echo of the [prisoner’s dilemma](https://en.wikipedia.org/wiki/prisoner’s_dilemma) and depressing solution to the [Fermi paradox](https://en.wikipedia.org/wiki/Fermi_paradox).

The dark forest solution of deterrence by mutually assured destruction between Earth and Trisolaris evokes thoughts of the Cold War.
 
The excellent non-linear timeline of the first book gives way to a chronological ordering in the second, although not without jumps in time as the characters “hibernate” into the future.
The story focusses on these characters, who show us in their own ways humanity’s reaction to first contact and subsequent impending doomsday: triumphalism and defeatism, hedonism and dedication, arrogance and humility.

Earth’s initial reaction is to establish the laughable yet oddly logical Wallfacer project. 
With humanity’s sole advantage against Trisolaris being our ability to deceive (before you feel any pride for our species), the Wallfacers must conceive of and execute a plan to win the Doomsday Battle without revealing it to anyone, therefore concealing it from the spying sophons of Trisolaris.
Fastforward a century and we see a wondrous utopian future through the eyes of the hibernators, with whose awe it is easy to empathise.
However, this future is not as bright as it seems and the easy confidence of modern society is exposed as a brittle hubris.
