---
titles:
- The Settlers IV
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2001
  artists:
  - Blue Byte
collections:
- name: The Settlers
  number: 4
dates:
  published: 2022-05-05
  updated: 2022-05-05
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Settlers_IV
---

