---
titles:
- Memories of Murder
- 살인의 추억
- 殺人의 追憶
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - crime
  - thriller
  - subtitled
  years:
  - 2003
  artists:
  - Bong Joon-ho
  places:
  - South Korea
  languages:
  - Korean
dates:
  published: 2022-09-12
  updated: 2022-09-12
  experienced:
  - started: 2022-09-12
    finished: 2022-09-12
links:
- https://en.wikipedia.org/wiki/Memories_of_Murder
---
