---
titles:
- Mr Robot S02
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - psychological thriller
  - drama
  - psychological
  - thriller
  - technology
  - corporate capitalism
  places:
  - United States
  - America
  years:
  - 2016
collections:
- name: Mr Robot
  number: 2
dates:
  published: 2021-02-24
  updated: 2022-08-28
  experienced:
  - started: 2021-02-26
    finished: 2021-03-01
links:
- https://en.wikipedia.org/wiki/List_of_Mr._Robot_episodes#Season_2_(2016)
---

Gideon’s gone.
Joanna Tyrell is still terrifying.
Leslie Romero’s killed by chance; Shama “Trenton” Biswas and Sunil “Mobley” Markesh are arrested but then gain amnesty, possibly via a terrified Mobley spilling the beans.
Darlene’s very valid little sister insecurities are superposed onto the position of leader of world-changing revolutionaries, then she’s caught by the FBI.
Cisco’s snuffed by the DA when the Feds make his description public.
Phillip Price continues to try to take over the world.

Dominique DiPierro’s an interesting one; she’s FBI, so she’s the enemy, but we’re rooting for her.
She has a great scene with clocks, dresses, and Whiterose in China before a submachine gun fuelled attack the next morning.
She talks to Alexa like Eliott talked to his fish, Qwerty.

Episodes start with a flashback scene to a pertinent previously unseen historical event.

Angela relentless hunts for revenge for her mother’s death.
Working at E Corp, “from within”, she seems to lose her humanity in this effort, even belittling her lovely father.

Whiterose’s alter ego is revealed as the Chinese Secretary of Security, Zhi Zhang, and she’s gearing up for Stage Two.
She has a project that has something to do with the Washington Township toxic waste scandal.
She radicalises Angela on this somehow, possibly promising the demise of E Corp?
As Qwerty’s caretaker, Angela does not appreciate the sacrifice of the fish in the water-clock.

Meanwhile, Elliot’s stay at his Mum’s is beautifully revealed as a prison stint.
Sweet and sour Ray’s actually the warden, the thugs are the guards, Leon’s a DA soldier, and the rest are regular inmates.
He makes peace with Mr Robot there, but it’s lost when they leave.
Tyrell turns up, unveils the plan—to blow up E Corp HQ with all the paper copies of the records inside—and when Elliot tries to stop it, since it’ll kill innocents, Mr Robot is one move ahead.
Tyrell shoots him with the popcorn gun.

What was the spent bullet from the popcorn gun in the Fun Society arcade about?
