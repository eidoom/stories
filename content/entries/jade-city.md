---
titles:
- Jade City
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  - urban fantasy
  - low fantasy
  - gangster
  - organised crime
  years:
  - 2017
  artists:
  - Fonda Lee
collections:
- name: The Green Bone Saga
  number: 1
dates:
  published: 2022-08-23
  updated: 2022-08-23
  experienced:
  - started: 2022-09-02
    finished: 2022-09-12
links:
- https://en.wikipedia.org/wiki/Jade_City_(novel)
---

