---
titles:
- Promising Young Woman
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - thriller
  - drama
  - rape
  - revenge
  years:
  - 2020
dates:
  published: 2021-10-04
  updated: 2021-10-04
  experienced:
  - started: 2021-10-03
    finished: 2021-10-03
links:
- https://en.wikipedia.org/wiki/Promising_Young_Woman
---

