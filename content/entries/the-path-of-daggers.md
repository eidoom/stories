---
titles:
- The Path of Daggers
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1998
  artists:
  - Robert Jordan
collections:
- name: The Wheel of Time
  number: 8
- name: The Wheel of Time universe
  number: 8
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
