---
titles:
- Shining Rock Software Devlog
taxonomies:
  media:
  - web
  formats:
  - blog
  years:
  - 2011
  artists:
  - Shining Rock Software
  - Luke Hodorowicz
dates:
  published: 2021-04-11
  updated: 2021-04-11
  experienced:
  - started: 2021-04-11
description: Banished blog (and beyond)
links:
- https://www.shiningrocksoftware.com/
---

Entertaining and insightful journey of a one-person computer game development team.
