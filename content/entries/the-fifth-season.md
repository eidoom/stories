---
titles:
- The Fifth Season
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2015
  artists:
  - N.K. Jemisin
collections:
- name: The Broken Earth
  number: 1
dates:
  published: 2020-07-08
  updated: 2021-07-30
  experienced:
  - started: 2020-07-15
    finished: 2020-07-18
links:
- https://en.wikipedia.org/wiki/The_Fifth_Season_(novel)
---
