---
titles:
- Harrow the Ninth 
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fantasy
  - speculative fiction
  - science fiction
  - fantasy
  - space
  - high fantasy
  - epic fantasy
  - dark fantasy
  - magic
  - necromancy
  - sword fighting
  years:
  - 2020
  artists:
  - Tamsyn Muir
  provenances:
  - New Zealand
  languages:
  - English
collections:
- name: Locked Tomb
  number: 2
dates:
  published: 2023-03-01
  updated: 2023-03-12
  experienced:
  - started: 2023-03-01
    finished: 2023-03-12
links:
- https://en.wikipedia.org/wiki/Harrow_the_Ninth
---
