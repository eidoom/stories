---
titles:
- The Book of Boba Fett
taxonomies:
  media:
  - video
  formats:
  - television
  years:
  - 2021
  artists:
  - Ming-Na Wen
  - Pedro Pascal
collections:
- name: Star Wars
  number: 6.6
- name: The Mandalorian +
  number: 3
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-01-12
    finished: 2022-02-09
links:
- https://en.wikipedia.org/wiki/The_Book_of_Boba_Fett
---

