---
titles:
- The Settlers II
- The Settlers II (10th Anniversary)
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 1996
  - 2006
  artists:
  - Blue Byte
collections:
- name: The Settlers
  number: 2
dates:
  published: 2022-05-05
  updated: 2022-05-05
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Settlers_II
- https://en.wikipedia.org/wiki/The_Settlers_II_(10th_Anniversary)
---

