---
titles:
- 'Star Wars: Episode III – Revenge of the Sith'
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2005
collections:
- name: Skywalker saga
  number: 3
- name: Star Wars prequel trilogy
  number: 3
- name: Star Wars
  number: 3
dates:
  published: 2022-05-21
  updated: 2022-05-21
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Star_Wars:_Episode_III_%E2%80%93_Revenge_of_the_Sith
---

