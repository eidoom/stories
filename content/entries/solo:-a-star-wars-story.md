---
titles:
- 'Solo: A Star Wars Story'
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - science fiction
  - fantasy
  - speculative fiction
  - space opera
  - space
  - fiction
  - backstory
  - origin
  years:
  - 2018
collections:
- name: Star Wars
  number: 3.6
dates:
  published: 2020-12-05
  updated: 2020-12-05
  experienced:
  - started: 2020-12-04
    finished: 2020-12-04
links:
- https://en.wikipedia.org/wiki/Solo:_A_Star_Wars_Story
---

