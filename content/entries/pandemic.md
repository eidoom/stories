---
titles:
- Pandemic
taxonomies:
  media:
  - game
  formats:
  - board game
  years:
  - 2008
dates:
  published: 2022-05-06
  updated: 2022-05-06
  experienced:
  - '?'
links:
- https://boardgamegeek.com/boardgame/30549/pandemic
---

