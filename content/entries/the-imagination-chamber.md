---
titles:
- The Imagination Chamber
taxonomies:
  media:
  - book
  years:
  - 2022
  artists:
  - Philip Pullman
  formats:
  - companion book
collections:
- name: His Dark Materials
  number: 1.1
- name: Northern Lights
  number: 1.1
dates:
  published: 2022-11-01
  updated: 2022-11-01
---
