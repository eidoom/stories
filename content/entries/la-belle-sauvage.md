---
titles:
- La Belle Sauvage
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  years:
  - 2017
  artists:
  - Philip Pullman
collections:
- name: The Book of Dust
  number: 1
- name: Northern Lights
  number: 0
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - started: 2019-11-25
    finished: 2019-12-06
links:
- https://en.wikipedia.org/wiki/La_Belle_Sauvage
---
