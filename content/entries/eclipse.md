---
titles:
- 'Eclipse: Second Dawn for the Galaxy'
taxonomies:
  media:
  - game
  formats:
  - board game
  genres:
  - 4x
  - space opera
  - space
  - aliens
  - science fiction
  - speculative fiction
  - fiction
  - far future
  - amerigame
  - wargame
  - space exploration
  - civilisation game
  years:
  - 2020
dates:
  published: 2021-06-15
  updated: 2022-05-06
  experienced:
  - started: 2021-06-15
    finished: 2021-06-15
  - started: 2021-06-18
    finished: 2021-06-18
description: Settlers of the GCDS
players:
  min: 1
  max: 6
links:
- https://www.boardgamegeek.com/boardgame/246900/eclipse-second-dawn-galaxy
- https://en.lautapelit.fi/product/24681/eclipse—2nd-dawn-for-the-galaxy-eng
---

This 2020 release is a revised version of the original 2011 edition, *Eclipse: New Dawn for the Galaxy*.

It’s like they made [Stellaris](stellaris.html) into a board game.
Well, except the classic version predates Stellaris so it’s really the other way around.

Very fun to play.
Suberp artwork and gameplay mechanics.

I played with four players.
