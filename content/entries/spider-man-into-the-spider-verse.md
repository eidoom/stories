---
titles:
- 'Spider-Man: Into the Spider-Verse'
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2018
collections:
- name: Spider-Man
  number: 10
- name: Animated Spider-Verse
  number: 1
dates:
  published: 2022-12-18
  updated: 2022-12-18
  experienced:
  - started: 2022-12-18
    finished: 2022-12-18
links:
- https://en.wikipedia.org/wiki/Spider-Man:_Into_the_Spider-Verse
---
