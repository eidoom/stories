---
titles:
- 'The Elder Scrolls V: Skyrim'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2011
  genres:
  - fantasy
  - speculative fiction
collections:
- name: The Elder Scrolls
  number: 5
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2011-11-11
completed: true
links:
- https://en.wikipedia.org/wiki/The_Elder_Scrolls_V:_Skyrim
---

