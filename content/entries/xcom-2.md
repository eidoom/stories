---
titles:
- XCOM 2
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2016
collections:
- name: XCOM
  number: 9
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2019-03-22
completed: true
links:
- https://en.wikipedia.org/wiki/XCOM_2
---

