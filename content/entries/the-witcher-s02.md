---
titles:
- The Witcher S02
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - dark
  - fantasy
  - drama
  years:
  - 2021
collections:
- name: The Witcher (TV)
  number: 2
- name: The Witcher
  number: 0.92
dates:
  published: 2021-09-05
  updated: 2022-04-19
  experienced:
  - started: 2022-01-05
    finished: 2022-01-07
links:
- https://en.wikipedia.org/wiki/The_Witcher_(TV_series)#Season_2_(2021)
---
