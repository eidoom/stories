---
titles:
- The Man from U.N.C.L.E.
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - spy
  years:
  - 2015
  artists:
  - Guy Ritchie
dates:
  published: 2022-09-18
  updated: 2022-09-18
  experienced:
  - started: 2022-09-18
    finished: 2022-09-18
links:
- https://en.wikipedia.org/wiki/The_Man_from_U.N.C.L.E._(film)
---
