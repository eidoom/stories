---
titles:
- The Hundred Thousand Kingdoms
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2010
  artists:
  - N. K. Jemisin
collections:
- name: The Inheritance Trilogy
  number: 1
- name: Inheritance
  number: 1
dates:
  published: 2020-08-02
  updated: 2021-07-30
  experienced:
  - started: 2020-07-25
    finished: 2020-08-04
links:
- https://en.wikipedia.org/wiki/The_Hundred_Thousand_Kingdoms
---
