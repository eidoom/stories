---
titles:
- Equilibrium
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2002
  artists:
  - Christian Bale
  - Sean Bean
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-04-10
    finished: 2022-04-10
links:
- https://en.wikipedia.org/wiki/Equilibrium_(film)
---

