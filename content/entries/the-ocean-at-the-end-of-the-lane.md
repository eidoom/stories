---
titles:
- The Ocean at the End of the Lane
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - legend
  - myth
  - fiction
  - fantasy
  - speculative fiction
  - magic
  - surrealism
  - magical realism
  - horror
  - dark fantasy
  places:
  - England
  - United Kingdom
  - Britain
  years:
  - 2013
  artists:
  - Neil Gaiman
dates:
  published: 2021-05-03
  updated: 2021-06-08
  experienced:
  - started: 2021-05-27
    finished: 2021-05-30
description: A dream of childhood
links:
- https://en.wikipedia.org/wiki/The_Ocean_at_the_End_of_the_Lane
---

A lovely read.
Gaiman takes us on a compelling journey with his trademark touch of the fantastical and the humorous.
No matter how magical the tale he weaves, it *just is*.

Our narrator is a young boy who finds himself involved with some deitic happenings.
