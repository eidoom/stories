---
titles:
- "Avatar: The Last Airbender – Katara and the Pirate's Silver"
taxonomies:
  media:
  - book
  formats:
  - comic
  - graphic novel
  years:
  - 2020
collections:
- name: 'Avatar: The Last Airbender'
  number: 29.5
dates:
  published: 2023-04-18
  updated: 2023-04-18
#  experienced:
#  - started: 2023-04-18
#    finished: 2023-04-18
---
