---
titles:
- 'Babel, or the Necessity of Violence: an Arcane History of the Oxford Translators’ Revolution'
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  - urban fantasy
  - low fantasy
  years:
  - 2022
  artists:
  - R. F. Kuang
  places:
  - Oxford
  - London
  - England
dates:
  published: 2022-10-15
  updated: 2022-10-15
  experienced:
  - started: 2022-10-18
    finished: 2022-10-31
---

I loved the magic system through all the etymological digressions, the beautiful and tragic relationships between the characters, the raw depiction of elitism and colonialism, the epic plunge that was the end of the book.
