---
titles:
- Consider Phlebas
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1987
  artists:
  - Iain M. Banks
collections:
- name: Culture
  number: 1
- name: The Culture
  number: 1
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Consider_Phlebas
---
