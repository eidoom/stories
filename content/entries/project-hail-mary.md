---
titles:
- Project Hail Mary
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fiction
  - speculative fiction
  - science fiction
  - hard sci-fi
  - near future
  - amnesia
  - space
  years:
  - 2021
  artists:
  - Andy Weir
dates:
  published: 2021-05-07
  updated: 2021-06-08
  experienced:
  - started: 2021-05-10
    finished: 2021-05-19
description: First contact
links:
- https://en.wikipedia.org/wiki/Project_Hail_Mary
---

First contact is an exo-bacterium that’s eating the sun so gluttonously as to spell extinction on Earth.
Second contact is an *earie* space-spider who comes to hold a special place of affection in our heart.
Ryland’s the unwitting nerd who has to save the world.
