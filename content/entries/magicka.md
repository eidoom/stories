---
titles:
- Magicka
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2011
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-03-06
completed: true
links:
- https://en.wikipedia.org/wiki/Magicka
---

