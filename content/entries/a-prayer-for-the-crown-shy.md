---
titles:
- A Prayer for the Crown-Shy
taxonomies:
  media:
  - book
  formats:
  - novella
  genres:
  - solarpunk
  - science fiction
  years:
  - 2022
  artists:
  - Becky Chambers
collections:
- name: Monk & Robot
  number: 2
dates:
  published: 2022-08-23
  updated: 2022-08-23
  experienced:
  - started: 2022-08-24
    finished: 2022-08-24
---

