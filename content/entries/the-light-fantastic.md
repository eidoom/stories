---
titles:
- The Light Fantastic
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  - fiction
  years:
  - 1983
  artists:
  - Terry Pratchett
collections:
- name: Rincewind
  number: 2
- name: Discworld
  number: 2
dates:
  published: 2021-09-23
  updated: 2021-09-23
  experienced:
  - started: 2022-05-29
    finished: 2022-05-31
links:
- https://en.wikipedia.org/wiki/The_Light_Fantastic
---

