---
titles:
- Doughnut Economics
taxonomies:
  media:
  - web
  formats:
  - website
  genres:
  - economics
  - society
  - ecology
  - environment
  - factual
  years:
  - 2020
dates:
  published: 2021-03-15
  updated: 2021-03-15
  experienced:
  - started: 2021-03-15
    finished: 2021-03-15
description: Principles for a sustainable future
links:
- https://doughnuteconomics.org/about-doughnut-economics
---

I like this framework.
It takes the two biggest problems of the modern world—social division and the ecological crisis, which have always felt like two faces of the same coin to me—and paints them as the floor and ceiling of a planetary economy.
