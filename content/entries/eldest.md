---
titles:
- Eldest
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2005
  artists:
  - Christopher Paolini
collections:
- name: The Inheritance Cycle
  number: 2
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - '?'
---
