---
titles:
- Dune Messiah
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1969
  artists:
  - Frank Herbert
collections:
- name: Dune Chronicles
  number: 2
- name: Dune
  number: 2
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Dune_Messiah
---
