---
titles:
- House of Chains
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2002
  artists:
  - Steven Erikson
collections:
- name: Malazan Book of the Fallen
  number: 4
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - '?'
---
