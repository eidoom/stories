---
titles:
- Equal Rites
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  - comedy
  - fiction
  - speculative fiction
  - magic
  years:
  - 1987
  artists:
  - Terry Pratchett
collections:
- name: Witches (Discworld)
  number: 1
- name: Discworld
  number: 3
dates:
  published: 2021-07-09
  updated: 2021-07-25
  experienced:
  - started: 2021-07-22
    finished: 2021-07-25
description: Uproariously witty
links:
- https://en.wikipedia.org/wiki/Equal_Rites
---

Esk is given a wizard’s staff as a baby after she is mistaken for a son.
Granny Weatherwax, witch, tries to help her get admitted to the Unseen University to become the Discworld’s first female wizard.
