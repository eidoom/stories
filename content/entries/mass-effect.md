---
titles:
- Mass Effect
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2007
collections:
- name: Mass Effect
  number: 1
dates:
  published: 2022-05-07
  updated: 2022-05-07
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Mass_Effect_(video_game)
---

