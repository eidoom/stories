---
titles:
- Shades in Shadow
taxonomies:
  media:
  - book
  formats:
  - collection
  - short story
  - triptych
  genres:
  - fantasy
  years:
  - 2015
  artists:
  - N. K. Jemisin
collections:
- name: Inheritance
  number: 0.5
dates:
  published: 2020-08-02
  updated: 2021-07-30
  experienced:
  - started: 2020-08-20
    finished: 2020-08-22
---
