---
titles:
- Slipways
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - space
  - strategy
  - turn-based
  - puzzle
  - sandbox
  - 3x
  - tbs
  years:
  - 2021
dates:
  published: 2021-06-16
  updated: 2021-06-16
  experienced:
  - started: 2021-06-15
completed: true
description: Arcade 4X
links:
- https://slipways.net/
---

Like a single-player board game of interstellar economy balancing.
Fast and fun.
