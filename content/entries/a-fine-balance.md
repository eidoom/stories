---
titles:
- A Fine Balance
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1995
  artists:
  - Rohinton Mistry
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
