---
titles:
- Outer Wilds
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - temporal loop
  - science fiction
  - fiction
  - speculative fiction
  - aliens
  - exploration
  - mystery
  - story
  years:
  - 2019
  artists:
  - Mobius Digital
collections:
- name: Outer Wilds
  number: 1
dates:
  published: 2021-01-06
  updated: 2021-01-23
  experienced:
  - started: 2021-01-10
    finished: 2021-01-22
description: A beautifully crafted story
links:
- https://en.wikipedia.org/wiki/Outer_Wilds
- https://www.mobiusdigitalgames.com/outer-wilds.html
- https://outerwilds.gamepedia.com/Outer_Wilds_Wiki
---

Wow, what a way to tell a story!
A whole solar system full of dynamic behaviour to marvel at, ancient mysteries to unravel, and wholesome friends to greet.
