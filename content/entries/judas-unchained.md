---
titles:
- Judas Unchained
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2005
  artists:
  - Peter F. Hamilton
collections:
- name: Commonwealth Saga
  number: 2
- name: Commonwealth universe
  number: 3
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
