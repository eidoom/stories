---
titles:
- 'Avatar: The Last Airbender – The Rift'
taxonomies:
  media:
  - book
  formats:
  - graphic novel
  years:
  - 2014
collections:
- name: 'Avatar: The Last Airbender'
  number: 64
dates:
  published: 2023-04-18
  updated: 2023-04-18
  experienced:
  - started: 2023-04-18
    finished: 2023-04-18
links:
- https://en.wikipedia.org/wiki/Avatar:_The_Last_Airbender_%E2%80%93_The_Rift
---
