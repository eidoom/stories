---
titles:
- A Fisherman of the Inland Sea
taxonomies:
  media:
  - book
  formats:
  - collection
  - short story
  genres:
  - speculative fiction
  - science fiction
  - fiction
  - comedy
  - anthropological
  - feminist
  years:
  - 1994
  artists:
  - Ursula K. Le Guin
dates:
  published: 2021-07-10
  updated: 2021-07-24
  experienced:
  - started: 2021-07-20
    finished: 2021-07-22
description: Jokes, fun in the Ekumen, feminism, and rich fictitious cultures
links:
- https://en.wikipedia.org/wiki/A_Fisherman_of_the_Inland_Sea
- https://www.ursulakleguin.com/a-fisherman-of-the-inland-sea
---

{{ short(title="Introduction", years=["1994"], started="2021-07-20") }}

Le Guin’s comments on these short stories, and her thoughts on “not reading science fiction”.

{{ short(title="The First Contact with the Gorgonids", link="https://en.wikipedia.org/wiki/A_Fisherman_of_the_Inland_Sea#The_First_Contact_with_the_Gorgonids", years=["1991"], started="2021-07-20") }}

A satire of the Western suited macho man and his wife.
The stereotype reminds me of Richard Morgan's *Market Forces*.

{{ short(title="Newton’s Sleep", link="https://en.wikipedia.org/wiki/A_Fisherman_of_the_Inland_Sea#Newton's_Sleep", years=["1991"], started="2021-07-20", finished="2021-07-21") }}

Somewhat a satire of the controlling American dad. Ike reminds me of Marty from the first season of *True Detective*.

With the Earth barely hospitable, not dissimilar to the world of Margaret Atwood’s *The Handmaid’s Tale*, a group escape to an artificial environment in orbit.
However, they start seeing ghosts of the planet they left in their pristine metal corridors.

{{ short(title="The Ascent of the North Face", link="https://en.wikipedia.org/wiki/A_Fisherman_of_the_Inland_Sea#The_Ascent_of_the_North_Face", years=["1983"], started="2021-07-21") }}

A mountaineering journal is revealed to be describing the scaling of a semi-derelict skyscraper.

{{ short(title="The Rock That Changed Things", years=["1992"], started="2021-07-21") }}

A slave caste read the colours of freedom in the stones they set in mosaics for their masters.

{{ short(title="The Kerastion", years=["1990"], started="2021-07-21") }}

A glimpse of a culture segregated into castes by trade, where all beauty belongs to the Goddess and destruction of sand sculptures by wind is a great honour to the artist.
When a Sculptor is found to be preserving his art, he commits suicide in shame and his Tanner sister attends his funeral, where a silent flute is played which she has made from the leather of their mother’s arm.

{{ short(title="The Shobies’ Story", link="https://en.wikipedia.org/wiki/The_Shobies%27_Story", uni="The Ekumen", years=["1990"], started="2021-07-21") }}

Le Guin plays with faster than light travel, called “transilience” or “skipping”, through “churten theory”, although it comes with a psychological twist: the interference with sentient thought.
The crew of the *Shoby* experience different realities on arrival of the first skip and must tell their stories together to cohere and avoid slipping into chaos.

{{ short(title="Dancing to Ganam", link="https://en.wikipedia.org/wiki/A_Fisherman_of_the_Inland_Sea#Dancing_to_Ganam", uni="The Ekumen", years=["1993"], started="2021-07-21") }}

A tale of self-deception.

Dalzul (later mentioned in [The Telling](the-telling.html)) is the stereotypical male hero, and so shapes his own Truth after skipping to Ganam with three crew including Shan of the Shobies.
The crew perceive that reality on Ganam is far more complex then Dalzul sees but are unable to stop him before he unwittingly is electrocuted to death by wielding a sacred scepter he thought would make him King.

Includes the hilarious mezklete bartender.

{{ short(title="Another Story", alt="A Fisherman of the Inland Sea",link="https://en.wikipedia.org/wiki/A_Fisherman_of_the_Inland_Sea#Another_Story", uni="The Ekumen", years=["1994"], started="2021-07-22") }}

A bittersweet story of the pains of leaving home and family in search of more, familiar to all who have flown the nest to study at university, I'm sure.
In an experiment with time travel, Le Guin affords the protagonist, Hideo, a second chance and subsequent happy ending.

Hideo is born to a sedoretu on O with a Terran mother who was an Ekumenical Mobile but went native after falling in love.
When he leaves home to study churten theory on Hain, he must pay the relativistic price of time dilation beyond homesickness.
