---
titles:
- The Books of Earthsea
- 'The Books of Earthsea: The Complete Illustrated Edition'
taxonomies:
  media:
  - book
  formats:
  - short story
  - collection
  years:
  - 2018
  artists:
  - Ursula K. Le Guin
collections:
- name: The Books of Earthsea
  number: 0
dates:
  published: 2022-05-02
  updated: 2022-05-05
  experienced:
  - started: 2021-11-20
    finished: 2022-05-05
---

{{ short(title="A Description of Earthsea", started="2022-05-02") }}

{{ short(title="The Daughter of Odren", started="2022-05-02") }}

{{ short(title="Firelight", started="2022-05-03") }}

{{ short(title="Earthsea Revisioned", alt="Children, Women, Men, and Dragons", started="2022-05-04", finished="2022-05-05") }}
