---
titles:
- House of Flying Daggers
- 十面埋伏
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - wuxia
  - romance
  - fiction
  - martial arts
  - fantasy
  languages:
  - Mandarin
  artists:
  - Zhang Yimou
  - Andy Lau
  - Zhang Ziyi
  - Takeshi Kaneshiro
  - Shigeru Umebayashi
  years:
  - 2004
  places:
  - China
dates:
  published: 2022-11-20
  updated: 2022-11-27
  experienced:
  - started: 2022-11-20
    finished: 2022-11-20
links:
- https://en.wikipedia.org/wiki/House_of_Flying_Daggers
description: So over the top
---
