---
titles:
- 'Avatar: The Last Airbender – The Promise'
taxonomies:
  media:
  - book
  formats:
  - graphic novel
  years:
  - 2012
collections:
- name: 'Avatar: The Last Airbender'
  number: 62
dates:
  published: 2023-04-17
  updated: 2023-04-17
  experienced:
  - started: 2023-04-16
    finished: 2023-04-17
links:
- https://en.wikipedia.org/wiki/Avatar:_The_Last_Airbender_%E2%80%93_The_Promise
---
