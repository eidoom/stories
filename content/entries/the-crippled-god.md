---
titles:
- The Crippled God
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2011
  artists:
  - Steven Erikson
collections:
- name: Malazan Book of the Fallen
  number: 10
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - '?'
---
