---
titles:
- 'Critical Role: Vox Machina Origins Volume 3'
taxonomies:
  media:
  - book
  formats:
  - comic
  years:
  - 2022
collections:
- name: 'Critical Role: Vox Machina Origins'
  number: 18.1
- name: Critical Role
  number: 12
dates:
  published: 2023-02-14
  updated: 2023-02-16
  experienced:
  - started: 2023-02-16
    finished: 2023-02-16
links:
- https://en.wikipedia.org/wiki/Critical_Role:_Vox_Machina_Origins
---
