---
titles:
- Zack Snyder’s Justice League
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - superhero
  - fiction
  - speculative fiction
  - fantasy
  places:
  - United States
  - America
  years:
  - 2021
collections:
- name: DC Extended Universe
  number: 10
dates:
  published: 2021-03-26
  updated: 2021-05-07
  experienced:
  - started: 2021-03-25
    finished: 2021-03-25
description: Comic books on the silver screen
links:
- https://en.wikipedia.org/wiki/Zack_Snyder%27s_Justice_League
---

Utterly ridiculous.
Not unenjoyable though.
