---
titles:
- A Wizard of Earthsea
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1968
  artists:
  - Ursula K. Le Guin
collections:
- name: The Books of Earthsea
  number: 1
- name: Earthsea
  number: 3
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2021-11-20
    finished: 2021-12-23
links:
- https://en.wikipedia.org/wiki/A_Wizard_of_Earthsea
---

