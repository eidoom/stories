---
titles:
- 'Critical Role: Vox Machina Origins Series I & II Library Edition'
taxonomies:
  media:
  - book
  formats:
  - comic
  years:
  - 2020
collections:
- name: 'Critical Role: Vox Machina Origins'
  number: 12.3
- name: Critical Role
  number: 11
dates:
  published: 2023-02-14
  updated: 2023-02-16
  experienced:
  - started: 2023-02-15
    finished: 2023-02-15
links:
- https://en.wikipedia.org/wiki/Critical_Role:_Vox_Machina_Origins
---
