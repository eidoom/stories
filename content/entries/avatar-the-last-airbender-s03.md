---
titles:
- 'Avatar: The Last Airbender S03'
- 'Book Three: Fire'
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - action
  - adventure
  - comedy drama
  - fantasy
  years:
  - 2007
  artists:
  - Benjamin Wynn
  - Bryan Konietzko
  - Dante Basco
  - Dee Bradley Baker
  - Greg Baldwin
  - Grey DeLisle
  - Jack DeSena
  - Jeremy Zuckerman
  - Jessie Flower
  - Mae Whitman
  - Mako
  - Mark Hamill
  - Michael Dante DiMartino
  - Zach Tyler Eisen
  provenances:
  - United States
collections:
- name: 'Avatar: The Last Airbender'
  number: 41
dates:
  published: 2023-03-23
  updated: 2023-04-15
  experienced:
  - started: 2023-04-12
    finished: 2023-04-15
links:
- https://en.wikipedia.org/wiki/Avatar:_The_Last_Airbender_(season_3)
---
