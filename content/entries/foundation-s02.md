---
titles:
- Foundation S02
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - science fiction
  years:
  - unreleased
collections:
- name: Foundation (TV)
  number: 2
dates:
  published: 2022-11-01
  updated: 2022-11-01
---

