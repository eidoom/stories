---
titles:
- The Birthday of the World and Other Stories
taxonomies:
  media:
  - book
  formats:
  - novella
  - novelette
  - collection
  - short story
  genres:
  - anthropological
  - ethnographical
  - speculative fiction
  - science fiction
  - fiction
  years:
  - 2002
  artists:
  - Ursula K. Le Guin
dates:
  published: 2021-07-10
  updated: 2021-10-14
  experienced:
  - started: 2021-07-10
    finished: 2021-07-20
description: Studies of alien societies
links:
- https://en.wikipedia.org/wiki/The_Birthday_of_the_World_and_Other_Stories
- https://www.ursulakleguin.com/the-birthday-of-the-world
---

{{ short(title="Foreword", years=["2001"], started="2021-07-10") }}

Le Guin’s comments on discovering “the Ekumen” and these short stories.

{{ short(title="Coming of Age in Karhide", years=["1995","2002"], started="2021-07-11", link="https://en.wikipedia.org/wiki/Coming_of_Age_in_Karhide", uni="The Ekumen") }}

A short bildungsroman, Gethen-style.

{{ short(title="The Matter of Seggri", link="https://en.wikipedia.org/wiki/The_Matter_of_Seggri", uni="The Ekumen", years=["1994"], started="2021-07-13", finished="2021-07-14") }}

A study of gender role.

Seggri is a planet where there are many more women than men.
Women do all the work while men spend their lives playing in castles, yet freedom and power lie with the women while the men are but reproductive trophies.

{{ short(title="Unchosen Love", uni="The Ekumen", years=["1994"], started="2021-07-16", finished="2021-07-17") }}

Making a marriage work; forming a sedoretu.

A marriage arrangement on O is called a sedoretu.
It involves four people, two genders (male and female), and two moieties (Morning and Evening).
There are four sexual pairings, two each of homosexual and heterosexual: Morning, Morning woman and Evening man; Evening, Evening woman and Morning man; Day, Morning woman and Evening woman; and Night, Morning man and Evening man.
There are also two remaining non-sexual relationships between opposite moieties: Morning woman and Morning man; and Evening woman and Evening man.
Pairings within a moiety are considered incestuous.

{{ short(title="Mountain Ways", uni="The Ekumen", years=["1996"], started="2021-07-17") }}

Back on O, a tale of unconventional love within their society.

{{ short(title="Solitude", uni="The Ekumen", years=["1994"], started="2021-07-18") }}

Eleven-Soro was once the planet of Hainish seeding with highest urbanisation, an ecumenopolis.
That society collapsed, leaving low population, low technology villages spread over land reclaimed by nature.
Their culture bears the warning of their past follies so strongly that social interactions and relationships between individuals, let alone the idea of society and governance, are superstitiously called magic, feared, and avoided.

An Ekumenical Mobile takes her children there in an ethnographic attempt to understand the Sorovian culture, but loses her daughter (the narrator) to solitude.

On “primitive” societies,
> “... people who live in artificially complicated situations called such a life “simple”. I never knew anybody, anywhere I have been, who found life simple.”

{{ short(title="Old Music and the Slave Women", uni="The Ekumen") }}

Read in previous collection: [Five Ways to Forgiveness](../../entries/five-ways-to-forgiveness#old-music-and-the-slave-women).

{{ short(title="The Birthday of the World", years=["2000"], started="2021-07-18", finished="2021-07-19") }}

Visitors from across the heavens disrupt a society who worship their monarchs as deities.

{{ short(title="Paradises Lost", link="https://en.wikipedia.org/wiki/Paradises_Lost", years=["2002"], started="2021-07-19") }}

On a generation ship whose mission is to investigate a distant Earth-like planet, a religious cult emerges which threatens to silence reason and doom the descendants to an entropic death aboard the ship.

Parallels to climate denial.

{{ short(title="Postscript", started="2021-07-20") }}

Includes an essay *On Despising Genres* which attacks the elitist dismissal of genre fiction, an interview *Answers to a Questionnaire*, and *A Few Words to a Young Writer* from Le Guin.
