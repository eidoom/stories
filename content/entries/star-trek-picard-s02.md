---
titles:
- 'Star Trek: Picard S02'
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - speculative fiction
  - science fiction
  - action
  - adventure
  - drama
  - space
  - future
  - aliens
  - artificial intelligence
  - android
  years:
  - 2022
collections:
- name: Star Trek
  number: 9.2
- name: 'Star Trek: Picard'
  number: 2
dates:
  published: 2020-12-05
  updated: 2022-08-28
  experienced:
  - started: 2022-03-13
    finished: 2022-05-05
links:
- https://en.wikipedia.org/wiki/Star_Trek:_Picard_(season_2)
---

Now it's Q's turn to face his demise and he's taking Picard along for the ride.
We go via the Terran Empire to a pivotal moment in Earth's history that seeds the Confederation, and birth a new Borg.
Picard must come to terms with a [cornerstone](westworld-s01.html) childhood trauma to satisfy Q.

I noticed with much amusement that the spaceship rumble plays almost entirely throughout the season, in particular when we're not on a spaceship.
