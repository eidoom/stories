---
titles:
- A Memory of Light
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2013
  artists:
  - Robert Jordan
  - Brandon Sanderson
collections:
- name: The Wheel of Time
  number: 14
- name: The Wheel of Time universe
  number: 14
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
