---
titles:
- Pillars of Eternity Guidebook Volume One
taxonomies:
  media:
  - book
  years:
  - 2015
  formats:
  - sourcebook
  genres:
  - lore
  - worldbuilding
collections:
- name: Pillars of Eternity
  number: 1.1
dates:
  published: 2023-02-04
  updated: 2023-02-04
  experienced:
  - started: 2023-02-02
    finished: 2023-02-04
links:
- https://www.darkhorse.com/Books/28-219/Pillars-of-Eternity-Guidebook-Volume-One-HC#prettyPhoto
- https://pillarsofeternity.fandom.com/wiki/Pillars_of_Eternity_Guidebook
- https://www.goodreads.com/book/show/23278666-pillars-of-eternity-guidebook-volume-one
---
