---
titles:
- Frozen
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2013
  artists:
  - Walt Disney Animation Studios
collections:
- name: Frozen
  number: 1
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-01-22
    finished: 2022-01-22
links:
- https://en.wikipedia.org/wiki/Frozen_(2013_film)
---

