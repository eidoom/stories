---
titles:
- Leviathan Wakes
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2011
  artists:
  - James S. A. Corey
collections:
- name: The Expanse
  number: 1
- name: The Expanse (books)
  number: 1
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Leviathan_Wakes
---
