---
titles:
- Desperate Hours
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2017
  artists:
  - David Mack
collections:
- name: Star Trek
  number: 6.5
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
