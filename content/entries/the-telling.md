---
titles:
- The Telling
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - speculative fiction
  - science fiction
  - fiction
  - far future
  years:
  - 2000
  artists:
  - Ursula K. Le Guin
collections:
- name: Hainish Cycle
  number: 8
- name: The Ekumen
  number: 8
dates:
  published: 2021-07-10
  updated: 2021-07-29
  experienced:
  - started: 2021-07-25
    finished: 2021-07-29
description: A story about stories
links:
- https://en.wikipedia.org/wiki/The_Telling
---

> “...belief is the wound that knowledge heals.”

Sutty, who lived on Terra through the violent religious oppression of the Unists and lost her partner Poe to that brutality, becomes an Ekumenical Observer on the planet Aka.
There, the ancient traditions of storytelling which formed the soul of a global society have been suppressed by a new corporate state born of the suffering created by a minority group of knowledge-usurers and exposure of the isolated civilisation to the Ekumen.

The depiction of the political climate of Aka reminds me of Liu Cixin’s description of the Cultural Revolution in China in [*The Three-Body Problem*](the-three-body-problem.html).
Le Guin states in the preface that the parallels to this part of Chinese history are deliberate.
