---
titles:
- Artemis
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - speculative fiction
  - science fiction
  - hard sci-fi
  - near future
  years:
  - 2017
  artists:
  - Andy Weir
dates:
  published: 2021-05-03
  updated: 2021-05-07
  experienced:
  - started: 2021-05-03
    finished: 2021-05-07
description: Lunar capers
links:
- https://en.wikipedia.org/wiki/Artemis_(novel)
---

A quick read; fun hard sci-fi.
