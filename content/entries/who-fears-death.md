---
titles:
- Who Fears Death
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2010
  artists:
  - Nnedi Okorafor
  genres:
  - science fiction
  - fantasy
  - africanfuturism
  - science fantasy
  - speculative fiction
  - fiction
  - post-apocalypse
  provenances:
  - Nigeria
  - United States
  places:
  - Sudan
collections:
- name: Who Fears Death
  number: 1
dates:
  published: 2022-11-01
  updated: 2022-12-09
  experienced:
  - started: 2022-11-22
    finished: 2022-12-09
links:
- https://en.wikipedia.org/wiki/Who_Fears_Death
---
