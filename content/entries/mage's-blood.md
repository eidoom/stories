---
titles:
- Mage’s Blood
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  years:
  - 2012
  artists:
  - David Hair
collections:
- name: Moontide Quartet
  number: 1
- name: Mage’s Blood
  number: 1
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
