---
titles:
- 'Extinction: The Facts'
taxonomies:
  media:
  - video
  formats:
  - television
  - documentary
  genres:
  - environment
  - life
  - nature
  - nonfiction
  - ecology
  - factual
  years:
  - 2020
  artists:
  - David Attenborough
dates:
  published: 2020-10-01
  updated: 2020-10-01
  experienced:
  - started: 2020-09-28
    finished: 2020-09-28
description: Ecological collapse is today’s problem
links:
- https://en.wikipedia.org/wiki/Extinction:_The_Facts
---

I watched David Attenborough’s [Extinction: The Facts](https://www.thetvdb.com/movies/extinction-the-facts).
This simply should be mandatory viewing for Britons.
