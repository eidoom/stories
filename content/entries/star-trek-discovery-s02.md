---
titles:
- 'Star Trek: Discovery S02'
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - speculative fiction
  - science fiction
  - action
  - adventure
  - drama
  - space
  - future
  - aliens
  years:
  - 2019
collections:
- name: 'Star Trek: Discovery'
  number: 2
- name: Star Trek
  number: 7.2
dates:
  published: 2020-12-05
  updated: 2022-08-28
  experienced:
  - '?'
completed: true
links:
- https://en.wikipedia.org/wiki/Star_Trek:_Discovery_(season_2)
---
