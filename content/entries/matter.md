---
titles:
- Matter
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  years:
  - 2008
  artists:
  - Iain M. Banks
collections:
- name: Culture
  number: 8
- name: The Culture
  number: 6
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - started: 2019-06-29
    finished: 2019-07-22
links:
- https://en.wikipedia.org/wiki/Matter_(novel)
---
