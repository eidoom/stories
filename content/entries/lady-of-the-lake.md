---
titles:
- The Lady of the Lake
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1996
  - 2017
  artists:
  - Andrzej Sapkowski
  - David French
collections:
- name: The Witcher Saga
  number: 5
- name: The Witcher
  number: 7
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Lady_of_the_Lake_(Sapkowski_novel)
---
