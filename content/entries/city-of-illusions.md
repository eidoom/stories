---
titles:
- City of Illusions
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - speculative fiction
  - science fiction
  - fiction
  - far future
  - fantasy
  - amnesia
  years:
  - 1967
  artists:
  - Ursula K. Le Guin
collections:
- name: Hainish Cycle
  number: 3
- name: The Ekumen
  number: 5
dates:
  published: 2021-06-05
  updated: 2021-06-27
  experienced:
  - started: 2021-06-05
    finished: 2021-06-08
description: I am lying
links:
- https://en.wikipedia.org/wiki/City_of_Illusions
---

A longer novel this time, although still ending suddenly.
A bitter fate for the future of an alternative Earth.

We share our protagonist’s struggle with self-identity and truth as each new encounter casts the last in doubt.
The varied societies across the planet’s face bear different aspects of the human experience.

In the cycle, we see the aftermath of the success of the Enemy (Shing) and the start of their overthrowal by the Alterrans (Werelians), leading to the establishment of the Ecumen.
