---
titles:
- Surface Detail
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  years:
  - 2010
  artists:
  - Iain M. Banks
collections:
- name: Culture
  number: 9
- name: The Culture
  number: 9
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - started: 2019-07-22
    finished: 2019-08-21
links:
- https://en.wikipedia.org/wiki/Surface_Detail
---
