---
titles:
- Volo's Guide to Monsters
taxonomies:
  media:
  - book
  formats:
  - rulebook
  - sourcebook
  genres:
  - supplement
  - bestiary
  years:
  - 2016
  artists:
  - Wizards of the Coast
collections:
- name: Dungeons & Dragons
  number: 5.21
- name: Dungeons & Dragons 5th Edition
  number: 11
dates:
  published: 2022-11-13
  updated: 2022-11-13
  experienced:
  - started: 2022-11-13
    finished: 2022-11-13
links:
- https://en.wikipedia.org/wiki/Volo%27s_Guide_to_Monsters
---

Replaced by [Mordenkainen](mordenkainen-presents-monsters-of-the-multiverse).
