---
titles:
- The Last Ringbearer
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1999
  artists:
  - Kirill Yeskov
  - Yisroel Markov
collections:
- name: Middle-Earth
  number: 31.1
dates:
  published: 2019-06-15
  updated: 2022-04-24
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Last_Ringbearer
---
