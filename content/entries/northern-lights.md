---
titles:
- Northern Lights
- The Golden Compass
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1995
  artists:
  - Philip Pullman
collections:
- name: His Dark Materials
  number: 1
- name: Northern Lights
  number: 1
dates:
  published: 2019-11-25
  updated: 2022-11-01
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Northern_Lights_(Pullman_novel)
---
