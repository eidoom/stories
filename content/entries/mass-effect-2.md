---
titles:
- Mass Effect 2
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2010
collections:
- name: Mass Effect
  number: 2
dates:
  published: 2022-05-07
  updated: 2022-05-07
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Mass_Effect_2
---

