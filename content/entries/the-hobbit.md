---
titles:
- The Hobbit
- The Hobbit, or There and Back Again
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1937
collections:
- name: Middle-Earth
  number: 30
- name: Tolkien’s legendarium
  number: 3
dates:
  published: 2022-05-05
  updated: 2022-05-05
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Hobbit
---

