---
titles:
- Westworld S04
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - android
  - artificial intelligence
  - speculative fiction
  - fiction
  - science fiction
  - drama
  - western
  - dystopian
  - mystery
  - action
  - adventure
  - nonlinear
  years:
  - 2022
collections:
- name: Westworld
  number: 4
dates:
  published: 2020-12-05
  updated: 2022-08-28
  experienced:
  - started: 2022-07-10
    finished: 2022-08-25
links:
- https://en.wikipedia.org/wiki/Westworld_(season_4)
---
