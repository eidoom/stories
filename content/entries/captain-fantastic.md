---
titles:
- Captain Fantastic
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - drama
  - comedy
  - fiction
  places:
  - United States
  - America
  years:
  - 2016
  artists:
  - Matt Ross
  - Viggo Mortensen
dates:
  published: 2020-10-26
  updated: 2020-10-26
  experienced:
  - started: 2020-10-25
    finished: 2020-10-25
description: Mortensen makes for a terrifying and awesome Dad
---

A bittersweet and joyous story of an unconventional father trying to care for his children in the aftermath of losing Mum.
The family’s hippy lifestyle is based on very worthy ideals, but we see Ben, the father, pushing things to the extreme.

The film opens in silence with the camera flying over beautiful, untouched forests.
The sounds of nature slowly creep in as we cut under the canopy and come across a deer.
It’s unusual—I initially suspected my speakers were off—but very peaceful and sets the eccentric tone of the film, particularly when the scene becomes a rite of passage into manhood for the eldest son, Bodevan (a made-up name for a unique soul), who hunts the deer barehanded and eats one of its internal organs raw.

I was in awe of the “paradise” this family had created in the wilderness, the intelligence and sheer ability of the children, and their social philosophies. I was also deeply unnerved by the length to which these ideas were taken. It is clear that there is something wrong and we soon learn that the mother had bipolar disorder and committed suicide while in hospital care. They chose to live outside society in an effort to help her, but to no avail. The details of her time with the family are a mystery, but echoes from this time suggest it was not without significant turbulence.

There are incidents which make us question Ben’s wisdom in the loving but brutal way he rears his children, which occur with increasing frequency as the story progresses and approaches its climax.
When the children injure themselves on a “mission”, they’re told to soldier on; they receive and have an unhealthy enthusiasm for weapons; the family run con acts to deceive or steal what they cannot buy.
Initially we forgive these as the family’s enchanting charisma leads us on and the delightful humour of the film keeps a big grin on our faces.
Eventually, however, they become unavoidable as the family leaves home to attend their mother’s funeral and must confront the outside world.
Ben’s training is woefully inadequate to prepare his children for this.

Some observations, since I’ve run out of time:
* Despite the worries, I’m still on Ben’s side.
* To travel, they road trip in an old bus which has been retrofitted as a camper.
* They celebrate Noam Chomsky’s birthday, but not Christmas.
* Personally hunting animals for dinner is a strong tenet of the family.
When one of the daughters stalks domestic sheep, she throws down her bow in disgust: “They just stand there.”
* Ben’s sister’s family (I think) is presented as a foil: the authentic American family.
The children’s faces hang slack in horror, painted by the bright primary colours from their cousins’ video games.
* Bo, who is high educated from Ben’s homeschooling, wishes to attend college since he feels that he doesn’t know anything: “I know nothing! ... Unless it comes out of a fucking book, I don’t know anything...” He seeks experience. Ben, in his rejection of the system, struggles to come to terms with this.
* Ben’s father-in-law is initially presented as highly antagonistic but we later see that he genuinely cares for his grandchildren and he raises some valid points against Ben.
* Near the end, Ben loses faith in himself as a father. 
This time, it is his children who come to his rescue.
* The film ends on a high note as the family settles into a life true to their beliefs, but without the dangerous extremism.
