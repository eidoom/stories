---
titles:
- The Peripheral S02
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - speculative fiction
  - science fiction
  - post-apocalypse
  - time travel
  - virtual reality
  - 2160p
  - 16:9
  - drama
  - thriller
  years:
  - unreleased
  places:
  - London
  - North Carolina
  provenances:
  - United States
dates:
  published: 2023-03-29
  updated: 2023-03-29
#  experienced:
#  - started: 2022-11-21
#    finished: 2022-12-02
#links:
#- https://en.wikipedia.org/wiki/The_Peripheral_(TV_series)
---
