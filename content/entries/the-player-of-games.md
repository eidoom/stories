---
titles:
- The Player of Games
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1988
  artists:
  - Iain M. Banks
collections:
- name: Culture
  number: 2
- name: The Culture
  number: 4
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Player_of_Games
---
