---
titles:
- 'Subnautica: Below Zero'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - exploration
  - story
  - crafting
  - survival
  - open world
  - science fiction
  - speculative fiction
  - fiction
  - far future
  years:
  - 2019
  artists:
  - Unknown Worlds Entertainment
collections:
- name: Subnautica
  number: 2
dates:
  published: 2020-11-06
  updated: 2021-06-08
  experienced:
  - started: 2020-07-21
description: More Subnautica—excellent!
links:
- https://en.wikipedia.org/wiki/Subnautica:_Below_Zero
- https://unknownworlds.com/subnautica/
---

### Through final beta and release

2021-04-18 -- ?
