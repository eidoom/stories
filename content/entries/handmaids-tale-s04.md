---
titles:
- The Handmaid’s Tale S04
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - speculative fiction
  - dystopian
  - tragedy
  - fiction
  years:
  - 2021
collections:
- name: The Handmaid’s Tale
  number: 4
dates:
  published: 2023-02-17
  updated: 2023-02-25
  experienced:
  - started: 2023-02-16
    finished: 2023-02-25
links:
- https://en.wikipedia.org/wiki/List_of_The_Handmaid%27s_Tale_episodes#Season_4_(2021)
---
