---
titles:
- The Peripheral
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  years:
  - 2014
  artists:
  - Willian Gibson
dates:
  published: 2022-12-02
  updated: 2022-12-02
links:
- https://en.wikipedia.org/wiki/The_Peripheral
---