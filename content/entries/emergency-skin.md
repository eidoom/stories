---
titles:
- Emergency Skin
taxonomies:
  media:
  - book
  formats:
  - short story
  - novelette
  genres:
  - science fiction
  - fiction
  - speculative fiction
  years:
  - 2019
  artists:
  - N. K. Jemisin
collections:
- name: Forward
  number: 5
dates:
  published: 2020-10-12
  updated: 2020-10-12
  experienced:
  - started: 2020-09-26
    finished: 2020-09-26
description: Earth cannot be saved
links:
- https://en.wikipedia.org/wiki/N._K._Jemisin#Short_stories
---

The world’s richest men, only a tiny sliver of the entire population, band together to start an exoplanet colony, escaping the dying Earth and ensuring humanity’s survival.

It was easy for the rest of us to fix our planet after they left.
