---
titles:
- Dead of Winter
- 'Dead of Winter: A Crossroads Game'
taxonomies:
  media:
  - game
  formats:
  - board game
  years:
  - 2014
collections:
- name: Crossroads
  number: 1
dates:
  published: 2022-05-06
  updated: 2022-05-06
  experienced:
  - '?'
completed: true
links:
- https://boardgamegeek.com/boardgame/150376/dead-winter-crossroads-game
---

