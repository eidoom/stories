---
titles:
- Children of Dune
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1976
  artists:
  - Frank Herbert
collections:
- name: Dune Chronicles
  number: 3
- name: Dune
  number: 3
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Children_of_Dune
---
