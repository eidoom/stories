---
titles:
- American Psycho
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 1991
  artists:
  - Christian Bale
  - Willem Dafoe
dates:
  published: 2022-09-09
  updated: 2022-09-09
  experienced:
  - '?'
completed: true
links:
- https://en.wikipedia.org/wiki/American_Psycho_(film)
---
