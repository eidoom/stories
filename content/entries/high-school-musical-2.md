---
titles:
- High School Musical 2
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - musical
  years:
  - 2007
dates:
  published: 2022-06-28
  updated: 2022-06-28
  experienced:
  - started: 2022-06-26
    finished: 2022-06-26
links:
- https://en.wikipedia.org/wiki/High_School_Musical_2
---

