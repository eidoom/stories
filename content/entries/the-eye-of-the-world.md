---
titles:
- The Eye of the World
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1990
  artists:
  - Robert Jordan
collections:
- name: The Wheel of Time
  number: 1
- name: The Wheel of Time universe
  number: 1
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
