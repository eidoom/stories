---
titles:
- Abzû
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - arthouse
  - adventure
  years:
  - 2016
  artists:
  - Giant Squid Studios
  - Austin Wintory
dates:
  published: 2021-01-10
  updated: 2021-01-19
  experienced:
  - started: 2021-01-10
    finished: 2021-01-12
description: The cosmic ocean
links:
- https://en.wikipedia.org/wiki/Abz%C3%BB
- https://abzugame.com/
---

While I played *Abzû* first, it feels like a spiritual successor to [*Journey*](journey.html).

Wow.
This is not so much a “game” as an *experience*.
Specifically, a transcendental experience of an interactive work of art.

*Abzû* has a strong stylistic aesthetic, with simple geometry and textures blended with more photorealistic lighting effects.
It is set underwater, where the lighting creates enthralling scenes, and imbued with a beautiful, fluid kineticism.
Being one with this living ocean full of curious and reactive marine animals is a joy.

The journey is accompanied by enchanting classical music that evokes a sense of religious awe.
It dynamically and seamlessly sets the mood of locations and events.

We encounter mythical ruins of an ancient civilisation, the submerged walls adorned with Egyptian-style art with a watery twist.
Long extinct animals inhabit these depths.

The antagonist of the narrative is a network of mysterious machines, harsh metal polyhedral structures in stark contrast to the organicity of the surrounding flora and fauna.
We must defeat this dark presence to restore the full vibrancy of life to the ocean.
