---
titles:
- V for Vendetta
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - speculative fiction
  - political
  - action
  - dystopian
  places:
  - Britain
  - London
  - England
  - United Kingdom
  years:
  - 2005
dates:
  published: 2021-05-24
  updated: 2021-06-08
  experienced:
  - started: 2021-05-24
    finished: 2021-05-24
description: Orwellian dystopia behind a Guy Fawkes mask
links:
- https://en.wikipedia.org/wiki/V_for_Vendetta_(film)
---

This film was a bit all over the place, recognisably Wachowski.
Quite fun though, with some very enjoyable scenes.
