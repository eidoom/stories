---
titles:
- A Second Chance at Eden
taxonomies:
  media:
  - book
  formats:
  - short story
  - collection
  genres:
  - science fiction
  years:
  - 1998
  artists:
  - Peter F. Hamilton
collections:
- name: Confederation universe
  number: 0
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/A_Second_Chance_at_Eden
---
