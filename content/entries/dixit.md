---
titles:
- Dixit
taxonomies:
  media:
  - game
  formats:
  - card game
  years:
  - 2008
dates:
  published: 2022-08-23
  updated: 2022-08-23
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Dixit_(card_game)
- https://www.libellud.com/en/our-games/dixit/
- https://boardgamegeek.com/boardgame/39856/dixit
---

