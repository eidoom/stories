---
titles:
- Black Widow
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - superhero
  - action
  years:
  - 2021
collections:
- name: Marvel Cinematic Universe
  number: 24
dates:
  published: 2021-07-09
  updated: 2021-07-09
  experienced:
  - started: 2021-07-09
    finished: 2021-07-09
description: Fun
links:
- https://en.wikipedia.org/wiki/Black_Widow_(2021_film)
---

