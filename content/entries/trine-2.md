---
titles:
- Trine 2
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2011
collections:
- name: Trine
  number: 2
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-04-17
links:
- https://en.wikipedia.org/wiki/Trine_2
---

