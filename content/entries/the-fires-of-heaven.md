---
titles:
- The Fires of Heaven
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1993
  artists:
  - Robert Jordan
collections:
- name: The Wheel of Time
  number: 5
- name: The Wheel of Time universe
  number: 5
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
