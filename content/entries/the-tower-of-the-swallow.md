---
titles:
- The Tower of the Swallow
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1997
  - 2016
  artists:
  - Andrzej Sapkowski
  - David French
collections:
- name: The Witcher Saga
  number: 4
- name: The Witcher
  number: 6
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Tower_of_the_Swallow
---
