---
titles:
- Unfinished Tales
- Unfinished Tales of Númenor and Middle-Earth
taxonomies:
  media:
  - book
  formats:
  - collection
  genres:
  - fantasy
  years:
  - 1980
  artists:
  - J. R. R. Tolkien
  - Christopher Tolkien
collections:
- name: Tolkien’s legendarium
  number: 2
- name: Middle-Earth
  number: 21
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Unfinished_Tales
---
