---
titles:
- 'A Plague Tale: Innocence'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - stealth
  - story
  - horror
  - historical
  - survival
  - action
  - adventure
  years:
  - 2019
  artists:
  - Asobo Studio
dates:
  published: 2020-11-25
  updated: 2020-11-25
  experienced:
  - started: 2020-11-25
description: Flee the rats, and worse
links:
- https://en.wikipedia.org/wiki/A_Plague_Tale:_Innocence
- https://aplaguetale.com/
---

This game is **heavy**.
