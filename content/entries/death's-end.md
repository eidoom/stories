---
titles:
- Death’s End
- 死神永生
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  - fiction
  - hard sci-fi
  - speculative fiction
  places:
  - China
  years:
  - 2010
  - 2016
  artists:
  - Liu Cixin
  - Ken Liu
collections:
- name: Remembrance of Earth’s Past
  number: 3
- name: Three-Body
  number: 3
dates:
  published: 2020-10-30
  updated: 2021-02-20
  experienced:
  - started: 2020-10-29
    finished: 2020-11-09
description: The fate of the universe
links:
- https://en.wikipedia.org/wiki/Death%27s_End
---

With *Death’s End* (死神永生), we come to the finale of the [*Remembrance of Earth’s Past*](../../series/remembrance-of-earth-s-past) (地球往事) trilogy.
The scope of this novel is truly grand, spanning the Common, Crisis, Deterrence, Broadcast, Bunker, and Galactic Eras—and beyond.
After an apparent non sequitur, we start by skipping back from the time of the end of *The Dark Forest* to the beginning of *The Three-Body Problem*, giving us a new perspective on the events of these books and some seemingly insignificant efforts of these times which had great consequence in the future.
We follow Cheng Xin through it all, with the narrative interspersed with educational excerpts from her memoirs, *A Past Outside of Time*.

The story contains many dichotomies, often under oscillation.
The Swordholders, Luo Ji and Cheng Xin, embody vigilant, ruthless deterrence against the enemy, and forgiving, motherly love for all life.
Thomas Wade is also a foil for Cheng Xin with his merciless determination against her merciful determination: the duty of survival against that of morality.
Humanity vacillates between single-minded focus on defence and blind hedonism founded on hubris.
Solar system humans retain compassion, empathy, and altruism, while galactic humans are forged into paranoid predators by the bleak darkness of the endless void and the unescapable threat of the dark forest.

A set of three fairy tales are woven by Yun Tianming, captive human on the First Trisolaran Fleet, to convey in secret messages to Earth.
Like the computer game *Three Body*, this presents an intriguing story within the story with hidden meaning to untangle.

The far-flung future presents many fun technologies.
Strung degenerate matter is used to create gravitational wave transmitters.
A circumsolar particle collider is constructed to probe higher energy scales and used to create a microscopic black hole which is then contained in a laboratory the size of a city for study.
Strong-interaction material is artificial matter held together not by electromagnetism, but the strong interaction.
Curvature-propulsion drives implement some kind of [Alcubierre drive](https://en.wikipedia.org/wiki/Alcubierre_drive) with the scary consequence that they leave behind wakes of spacetime which have permanently reduced lightspeed, called black domains. 
Aliens have even weaponised dimensionality: objects like the two-vector foil can cause three-dimensional space to collapse into two macroscopic dimensions and one microscopic dimension like in [Kaluza-Klein theory](https://en.wikipedia.org/wiki/Kaluza-Klein_theory).
The terrifying conclusion of these final two revelations is that the universe started as a higher-dimensional spacetime with an infinitely higher speed of light, but the ravages of war poisoned the very laws of physics to create the universe we observe today.

Parallels can be drawn between the situation of Earth against the technologically-superior and oppressive Trisolaris, and the current international political climate between China and the West.
The same can be said of the competing philosophies and political ideologies explored in this series.[^politic]

[^politic]: [This article](https://www.newyorker.com/magazine/2019/06/24/liu-cixins-war-of-the-worlds) makes for an interesting read on these points.
