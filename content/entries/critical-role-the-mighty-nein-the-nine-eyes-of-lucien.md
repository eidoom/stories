---
titles:
- 'Critical Role: The Mighty Nein – The Nine Eyes of Lucien'
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  - literature
  years:
  - 2022
  artists:
  - Madeleine Roux
  places:
  - Exandria
  provenances:
  - United States
  languages:
  - English
collections:
- name: Critical Role novels
  number: 2
- name: Critical Role
  number: 22
dates:
  published: 2023-02-16
  updated: 2023-04-12
  experienced:
  - started: 2023-03-02
    finished: 2023-03-08
links:
- https://en.wikipedia.org/wiki/Critical_Role:_The_Mighty_Nein_%E2%80%93_The_Nine_Eyes_of_Lucien
---
