---
titles:
- 'Divinity: Original Sin II'
- 'Divinity: Original Sin II — Definitive Edition'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - rpg
  - fantasy
  - single-player
  - multiplayer
  - role-playing game
  - ficiton
  - speculative fiction
  - comedy
  - role-playing
  - crpg
  - high fantasy
  artists:
  - Larian Studios
  years:
  - 2017
  places:
  - Rivellon
collections:
- name: 'Divinity: Original Sin'
  number: 2
- name: Divinity
  number: 5
dates:
  published: 2021-07-15
  updated: 2023-03-19
  experienced:
  - started: 2021-07-15
    finished: 2023-03-19
links:
- https://en.wikipedia.org/wiki/Divinity:_Original_Sin_II
---

