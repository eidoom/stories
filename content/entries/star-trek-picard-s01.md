---
titles:
- 'Star Trek: Picard S01'
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - speculative fiction
  - science fiction
  - action
  - adventure
  - drama
  - space
  - future
  - aliens
  - artificial intelligence
  - android
  years:
  - 2020
collections:
- name: Star Trek
  number: 9.1
- name: 'Star Trek: Picard'
  number: 1
dates:
  published: 2020-12-05
  updated: 2022-08-28
  experienced:
  - started: 2020-01-23
    finished: 2020-03-26
links:
- https://en.wikipedia.org/wiki/Star_Trek:_Picard_(season_1)
---

With his own days numbered, Jean-Luc is desperate to ensure a future for android life, in particular one who may be Data’s daughter.
Having burnt his bridges with Starfleet in protest of their behaviour, he has to appeal to old friends for help.
