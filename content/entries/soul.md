---
titles:
- Soul
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - animated
  - fiction
  - fantasy
  - speculative fiction
  - family friendly
  years:
  - 2020
  artists:
  - Pixar Animation Studios
dates:
  published: 2021-01-10
  updated: 2021-01-10
  experienced:
  - started: 2021-01-10
    finished: 2021-01-10
description: Such a lovely and hilarious film
links:
- https://en.wikipedia.org/wiki/Soul_(2020_film)
---
