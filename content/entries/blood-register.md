---
titles:
- Blood Register
taxonomies:
  media:
  - book
  formats:
  - short story
  years:
  - 2015
  artists:
  - Paul Kirsch
collections:
- name: Pillars of Eternity
  number: 1.22
dates:
  published: 2023-02-12
  updated: 2023-02-14
  experienced:
  - started: 2023-02-13
    finished: 2023-02-13
---
