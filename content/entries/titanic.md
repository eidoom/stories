---
titles:
- Titanic
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 1997
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-04-15
    finished: 2022-04-15
links:
- https://en.wikipedia.org/wiki/Titanic_(1997_film)
---

Watched on the 110th anniversary of its sinking.
