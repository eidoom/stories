---
titles:
- The Great Gatsby
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1925
  artists:
  - F. Scott Fitzgerald
dates:
  published: 2019-11-25
  updated: 2021-07-30
---
