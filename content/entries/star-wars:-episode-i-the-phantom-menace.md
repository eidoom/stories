---
titles:
- 'Star Wars: Episode I – The Phantom Menace'
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 1999
collections:
- name: Skywalker saga
  number: 1
- name: Star Wars prequel trilogy
  number: 1
- name: Star Wars
  number: 1
dates:
  published: 2022-05-21
  updated: 2022-05-21
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Star_Wars:_Episode_I_%E2%80%93_The_Phantom_Menace
---

