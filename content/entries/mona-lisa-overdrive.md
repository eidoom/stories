---
titles:
- Mona Lisa Overdrive
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - cyberpunk
  years:
  - 1988
  artists:
  - William Gibson
collections:
- name: The Sprawl
  number: 3
dates:
  published: 2022-12-31
  updated: 2023-01-16
  experienced:
  - started: 2023-01-16
    finished: 2023-01-27
links:
- https://en.wikipedia.org/wiki/Mona_Lisa_Overdrive
---
