---
titles:
- '300: Rise of an Empire'
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - sequel
  - sword and sandal
  - epic
  - fantasy
  - historical
  - action
  - fiction
  - heroic
  years:
  - 2014
collections:
- name: '300'
  number: 2
dates:
  published: 2021-10-02
  updated: 2021-10-02
  experienced:
  - started: 2021-10-01
    finished: 2021-10-01
links:
- https://en.wikipedia.org/wiki/300%3A_Rise_of_an_Empire
---

