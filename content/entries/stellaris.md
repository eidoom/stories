---
titles:
- Stellaris
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - rts
  - 4x
  - grand strategy
  - space
  - science fiction
  - fiction
  - speculative fiction
  - strategy
  - real-time
  - rpg
  years:
  - 2016
  artists:
  - Paradox Development Studio
dates:
  published: 2020-10-21
  updated: 2022-04-19
  experienced:
  - started: 2016-10-20
completed: true
description: War crimes in space
links:
- https://en.wikipedia.org/wiki/Stellaris_(video_game)
- https://www.stellaris.com/en/pc
---

Including:
* Story packs:
    * Leviathans
    * Utopia
    * Synthetic Dawn
    * Apocalypse
    * Distant Stars
    * Megacorp
    * Ancient Relics
    * Federations
* Species packs:
    * Plantoids
    * Humanoids
    * Lithoids

## Story time

There was once an authoritarian, divine empire of avians known as the Kingdom of Yondarim.
Beyond their beliefs in the superiority of the noble class and that the lower caste deserved to be under chattel slavery, their religion was venerated above all else.
This fanaticism came as no surprise, seeing as how their deities lived among them as their High Kings and Queens.

Once spacecraft transcended their wings as the means of flight, they quickly spread to the stars from their home planet of B’Yond, following a harsh expansionist regime under the iron fist of the High Queen of that era.
The discovery of other intelligent life in the galaxy was a disappointment: they were quickly found to be ungodly beasts.
In fact, many unworthy neighbours were encountered, scum without the true guiding principle of spirituality who chased false idols.
The High Queen turned a cold shoulder to these lesser beings, vowing to bide her time until the new colonies were settled and the military could be bolstered to start a holy crusade to cleanse the galaxy of this filth.

However, this glorious war was not meant to be, at least not in Her time.
The rate of expansion was a great strain on society, which was only held together by the produce of the slaves.
Truly, the overseers, under Her direction, were blessed as the saviours of the Kingdom.
The High Queen sought to increase moral by increasing the living standards of all Yondarim from basic subsistence to decent conditions.
Placing this trust in the masses would prove to be a grave error, although the results would not be seen in Her reign.

Time passed, and the colonies flourished into powerhouses of industry and worship.
Rule passed to the royal divine daughter.
The economy was strong, and so the war fleet inflated.
However, the improved living conditions of the workers had sown the seed of dissension.
Factions promoting egalitarian ideals formed, even finding sympathisers amongst the aristocracy.
The unity of the Kingdom was eroded and its very survival hung from a perilous precipice.
Disaster struck in the newest colony with the first slave rebellion.
The traitorous rebels were quenched and executed, but industry took a heavy blow.
Even worse, the story of these martyrs spread like cancer, leading the hearts of further Yondarim astray.

Thankfully, this incident shook the elite from their stupor.
The High Queen took decisive action to bring Her Kingdom to heel.
Worship was forcibly intensified and spiritual festivals were held to bring everyone back to their religious roots and remind them of their faith in their holy High Queen.
Society was restructured as a stratified economy, with the luxuries previous extended to commoners revoked and laid solely in the laps of the ruling classes.
With proper order restored and wealth distributed only to those who had the right to it, the treasonous cries became whispers.
To put an end once and for all to the faithless, all citizens who were members of heretical factions were marched to the agricultural and mineral labour camps to replace the expired slaves of the rebellions.
With their citizenship annulled, these degenerates lost political voice in the Kingdom including the right to be a faction member, thus dissolving the last controversial elements of the Yondarim.
They were thankful over the short remainder of their lives to atone for their sins by labouring for the Kingdom.

The Yondarim lost their taste for holy war after this close brush with civil war and pursued more isolationist agendas following this dark period of their history.
Although slow to learn, the academic elite demonstrated innate aptitude for physics and made many breakthroughs in scientific research.
The influence of religious propaganda and the natural conformist tendencies of the Yondarim further galvanised the people as devout zealots.
This faith was rewarded as latent psionic abilities began to manifest in blessed individuals as society spiritually matured.
Psionics were used to create new jump drives that hurled space flight into a new golden age.
Progress culminated in the transcendence of the entire species to psionically active beings, with leaders exhibiting the most powerful abilities, securing the ascendancy of the Yondarim on the galactic stage.

This awakening allowed the Yondarim to discover another realm of existence known as the Shroud.
After the first tentative communions with this otherly plane, the High Queen entered the Shroud.
Feeling the menace emanating from beyond, Yondarim held its breath in Her absence from reality.
She returned as the Chosen One, the divine sovereign become immortal, fulfilling the destiny of Her royal line in becoming the first and last God-Empress.
The Kingdom would for ever more reside under the mercy of her terrible, avian stare; the people rejoiced at this proof of their belief.

Over the next century, the war fleets of Yondarim spread their crimson wings through the galaxy.
A great ship was constructed, the Colossus, and fitted with an experimental weapon of mass mind control, the Divine Enforcer.
The mislead miscreant species neighbouring the Kingdom were brought under Her Word with the direction of the Colossus and, if judged worthy, spared as dominion subjects.
The more resistant were not so fortunate in their fate, along with the unholy non-organics.

As the galaxy was gathered under Her magnanimous wings, further wonders were uncovered.
The greatest such discovery was that of the L-Gate network.
These dormant Gates eluded activation for many years until the great Yondarim scientists succeeded in opening one.
This turned out to be the opening of a Pandora’s box, as the first scientist to pass through an L-Gate was lost in an onslaught of self-replicating nano-robots called the Grey Tempest.
These nanite fleets burst forth from L-Gates around in the galaxy in an explosive invasion.
It became apparent that a past civilisation had sacrificed itself to seal off the Grey Tempest in a satellite cluster of the galaxy called the L-Cluster, disabling the L-Gate network behind them.
The Kingdom fought a long and bloody war to subdue these soulless machines, eventually eradicating them and claiming the resource-rich L-Cluster.

Entrenched as the dominant superpower of the galaxy only a few centuries after leaving B’Yond, the God-Empress was as close to feeling happy as was possible for her post-Yondar immortal mind.
It was then that a massive power surge was detected beyond the borders of the divine Kingdom and reports started flooding in of systems around the surge going dark.
People everywhere claimed to hear the background noise of a rising wind, even in spacecraft.
The Yondarim’s forays into the Shroud and use of the Psi Jump Drives had left ripples in spacetime that had led the Unbidden to the galaxy.
So began a fight for survival that continues to this day.
