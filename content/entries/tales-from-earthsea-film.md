---
titles:
- Tales from Earthsea
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2006
  artists:
  - Atsushi Okui
  - Gorō Miyazaki
  - Takeshi Seyama
  - Tamiya Terashima
  - Toshio Suzuki
  provenances:
  - Japan
dates:
  published: 2023-04-17
  updated: 2023-04-17
  experienced:
  - started: 2023-04-17
    finished: 2023-04-17
links:
- https://en.wikipedia.org/wiki/Tales_from_Earthsea_(film)
---
