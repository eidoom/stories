---
titles:
- The Lies of Locke Lamora
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2006
  artists:
  - Scott Lynch
collections:
- name: Gentleman Bastard
  number: 1
- name: Gollancz 50
  number: 6
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Lies_of_Locke_Lamora
---
