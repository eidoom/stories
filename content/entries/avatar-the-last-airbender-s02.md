---
titles:
- 'Avatar: The Last Airbender S02'
- 'Book Two: Earth'
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - action
  - adventure
  - comedy drama
  - fantasy
  years:
  - 2006
  artists:
  - Benjamin Wynn
  - Bryan Konietzko
  - Dante Basco
  - Dee Bradley Baker
  - Greg Baldwin
  - Grey DeLisle
  - Jack DeSena
  - Jeremy Zuckerman
  - Jessie Flower
  - Mae Whitman
  - Mako
  - Mark Hamill
  - Michael Dante DiMartino
  - Zach Tyler Eisen
  provenances:
  - United States
collections:
- name: 'Avatar: The Last Airbender'
  number: 21
dates:
  published: 2023-03-23
  updated: 2023-03-29
  experienced:
  - started: 2023-03-24
    finished: 2023-03-28
links:
- https://en.wikipedia.org/wiki/Avatar:_The_Last_Airbender_(season_2)
---
