---
titles:
- Death on the Nile
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2022
collections:
- name: Kenneth Branagh’s Poirot
  number: 2
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-03-28
    finished: 2022-03-28
links:
- https://en.wikipedia.org/wiki/Death_on_the_Nile_(2022_film)
---

