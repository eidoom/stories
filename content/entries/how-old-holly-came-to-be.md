---
titles:
- How Old Holly Came to Be
taxonomies:
  media:
  - book
  formats:
  - short story
  years:
  - 2013
  artists:
  - Patrick Rothfuss
collections:
- name: The Kingkiller Chronicle
  number: 0.1
- name: Unfettered
  number: 2
dates:
  published: 2022-08-14
  updated: 2022-08-14
  experienced:
  - started: 2022-08-14
    finished: 2022-08-14
---

