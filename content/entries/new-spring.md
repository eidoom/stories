---
titles:
- New Spring
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2004
  artists:
  - Robert Jordan
collections:
- name: The Wheel of Time
  number: 0
- name: The Wheel of Time universe
  number: 0
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/New_Spring
---
