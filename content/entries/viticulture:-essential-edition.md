---
titles:
- 'Viticulture: Essential Edition'
taxonomies:
  media:
  - game
  formats:
  - board game
  genres:
  - strategy
  - economic
  - farming
  - wine
  years:
  - 2015
dates:
  published: 2021-09-22
  updated: 2022-05-06
  experienced:
  - started: 2021-09-21
    finished: 2021-09-21
  - started: 2022-01-01
    finished: 2022-01-01
players:
  min: 1
  max: 6
links:
- https://boardgamegeek.com/boardgame/183394/viticulture-essential-edition
---

Reimplementation of 2013 *Viticulture*.

I played with three players.
