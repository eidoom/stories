---
titles:
- Elantris
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2005
  artists:
  - Brandon Sanderson
collections:
- name: Elantris
  number: 1
- name: Cosmere
  number: 0
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Elantris
---
