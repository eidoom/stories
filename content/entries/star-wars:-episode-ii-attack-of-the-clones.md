---
titles:
- 'Star Wars: Episode II – Attack of the Clones'
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2002
collections:
- name: Skywalker saga
  number: 2
- name: Star Wars prequel trilogy
  number: 2
- name: Star Wars
  number: 2
dates:
  published: 2022-05-21
  updated: 2022-05-21
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Star_Wars:_Episode_II_%E2%80%93_Attack_of_the_Clones
---

