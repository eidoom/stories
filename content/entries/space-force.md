---
titles:
- Space Force S01
taxonomies:
  media:
  - video
  formats:
  - television
  places:
  - United States
  - America
  genres:
  - comedy
  - drama
  - fiction
  years:
  - 2021
collections:
- name: Space Force
  number: 1
dates:
  published: 2020-12-05
  updated: 2020-12-05
  experienced:
  - started: 2020-05-29
completed: true
links:
- https://en.wikipedia.org/wiki/Space_Force_(TV_series)
---

