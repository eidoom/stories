---
titles:
- Mindstar Rising
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1993
  artists:
  - Peter F. Hamilton
collections:
- name: Greg Mandel
  number: 1
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
