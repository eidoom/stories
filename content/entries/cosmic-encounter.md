---
titles:
- Cosmic Encounter
taxonomies:
  media:
  - game
  formats:
  - board game
  years:
  - 2008
dates:
  published: 2022-08-23
  updated: 2022-08-23
  experienced:
  - '?'
completed: true
links:
- https://en.wikipedia.org/wiki/Cosmic_Encounter
- https://boardgamegeek.com/boardgame/39463/cosmic-encounter
---

