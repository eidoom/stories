---
titles:
- 'Avatar: The Way of Water'
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2022
  genres:
  - science fiction
  - speculative fiction
  - epic
  artists:
  - Amanda Silver
  - David Brenner
  - James Cameron
  - John Refoua
  - Jon Landau
  - Josh Friedman
  - Kate Winslet
  - Rick Jaffa
  - Russell Carpenter
  - Sam Worthington
  - Shane Salerno
  - Sigourney Weaver
  - Simon Franglen
  - Stephen Lang
  - Stephen Rivkin
  - Zoe Saldaña
  places:
  - Pandora
  provenances:
  - United States
collections:
- name: Avatar
  number: 2
dates:
  published: 2023-03-29
  updated: 2023-03-29
  experienced:
  - started: 2023-03-28
    finished: 2023-03-28
links:
- https://en.wikipedia.org/wiki/Avatar:_The_Way_of_Water
---
