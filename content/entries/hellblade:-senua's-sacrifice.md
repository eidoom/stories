---
titles:
- 'Hellblade: Senua’s Sacrifice'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - dark
  - fantasy
  - speculative fiction
  - dark fantasy
  - fiction
  - celtic
  - scotland
  - norse
  years:
  - 2017
  artists:
  - Ninja Theory
dates:
  published: 2021-01-06
  updated: 2021-01-06
  experienced:
  - started: 2021-01-10
description: Disturbing
links:
- https://en.wikipedia.org/wiki/Hellblade:_Senua%27s_Sacrifice
- https://www.hellblade.com/
---

