---
titles:
- Mordenkainen's Tome of Foes
taxonomies:
  media:
  - book
  formats:
  - rulebook
  - sourcebook
  genres:
  - supplement
  - bestiary
  years:
  - 2018
  artists:
  - Wizards of the Coast
collections:
- name: Dungeons & Dragons
  number: 5.23
- name: Dungeons & Dragons 5th Edition
  number: 13
dates:
  published: 2022-11-13
  updated: 2022-11-13
  experienced:
  - started: 2022-11-13
    finished: 2022-11-13
links:
- https://en.wikipedia.org/wiki/Mordenkainen%27s_Tome_of_Foes
---

Replaced by [Mordenkainen](mordenkainen-presents-monsters-of-the-multiverse).
