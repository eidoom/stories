---
titles:
- The Princess and the Frog
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2009
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-02-05
    finished: 2022-02-05
links:
- https://en.wikipedia.org/wiki/The_Princess_and_the_Frog
---

