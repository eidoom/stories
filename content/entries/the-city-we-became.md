---
titles:
- The City We Became
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  - speculative fiction
  - urban
  - fiction
  places:
  - United States
  - America
  years:
  - 2020
  artists:
  - N. K. Jemisin
collections:
- name: Great Cities Trilogy
  number: 1
dates:
  published: 2020-10-08
  updated: 2020-10-12
  experienced:
  - started: 2020-09-29
    finished: 2020-10-06
description: The city lives
links:
- https://en.wikipedia.org/wiki/The_City_We_Became
---

The fantastical story of the birth of New York.
This book creates similes of everything and slams them into the real world.
It doesn’t shy away from punching bigotry in the nose either, a Jemisin attitude staple.
Having never visited the city, I feel rather familiar with it after my reading.
It is quite the hoot; completely surreal, but so much fun.

We meet Manhattan, an amnesiac immigrant with a scary criminal past. 
Brooklyn was a famous rapper in her youth, now a respectable city councillor.
The Bronx is an artist, although now later in her career, she spends most of her time managing a local art gallery.
Queens has come to New York to study at university, hoping to do her family proud.
I almost forgot Staten Island, the pitiful but viscous racist woman still living at home in fear of her father.
The boroughs are diverse: Black, Native American, Indian, White, and of mixed heritage and forgotten family trees; live-and-die-here local and newcomer; gay and straight.
Jersey joins in too.
The primary avatar of New York is some homeless kid.
