---
titles:
- Space Haven
taxonomies:
  media:
  - game
  formats:
  - computer game
  genres:
  - colony
  - simulator
  - spaceship
  - indie
  - base builder
  years:
  - 2020
  artists:
  - Bugbyte
dates:
  published: 2023-01-08
  updated: 2023-01-08
  experienced:
  - started: 2023-01-06
links:
- https://bugbyte.fi/spacehaven/
---
