---
titles:
- 'Star Trek: Strange New Worlds S02'
taxonomies:
  media:
  - video
  formats:
  - television
  years:
  - unreleased
  genres:
  - science fiction
collections:
- name: 'Star Trek: Strange New Worlds'
  number: 2
- name: Star Trek
  number: 11.2
dates:
  published: 2022-05-07
  updated: 2022-05-07
links:
- https://en.wikipedia.org/wiki/Star_Trek:_Strange_New_Worlds_(season_2)
---
