---
titles:
- Control
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - action
  - adventure
  - paranormal
  years:
  - 2019
  artists:
  - Remedy Entertainment
dates:
  published: 2021-01-06
  updated: 2022-02-24
  experienced:
  - started: 2020-12-14
links:
- https://en.wikipedia.org/wiki/Control_(video_game)
- https://www.remedygames.com/games/control/
active: true
---

