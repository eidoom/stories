---
titles:
- Lagoon
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2014
  artists:
  - Nnedi Okorafor
dates:
  published: 2022-12-10
  updated: 2022-12-10
links:
- https://en.wikipedia.org/wiki/Lagoon_(novel)
---