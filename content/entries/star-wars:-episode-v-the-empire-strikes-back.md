---
titles:
- 'Star Wars: Episode V – The Empire Strikes Back'
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 1980
collections:
- name: Skywalker saga
  number: 5
- name: Star Wars original trilogy
  number: 2
- name: Star Wars
  number: 5
dates:
  published: 2022-05-21
  updated: 2022-05-21
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Empire_Strikes_Back
---

