---
titles:
- Social Cooling
taxonomies:
  media:
  - web
  formats:
  - website
  genres:
  - society
  - internet
  - nonfiction
  - factual
  - technology
  - privacy
  - data mining
  - modern
  years:
  - 2017
  artists:
  - Tijmen Schep
dates:
  published: 2020-10-01
  updated: 2021-02-12
  experienced:
  - started: 2020-09-29
    finished: 2020-09-29
description: Misuse of people’s data
links:
- https://www.socialcooling.com/
---

A couple of days ago, I [came across](https://news.ycombinator.com/item?id=24627363) an information [website](https://www.socialcooling.com/) about the negative social effects of big data on people, [coined](https://www.tijmenschep.com/socialcooling/) “social cooling”.
Today, I [found](https://news.ycombinator.com/item?id=24637064) another [site](https://www.hownormalami.eu/) demonstrating the role of facial recognition in social cooling in a poignant manner.
It turns out both resources were created by [Tijmen Schep](https://www.tijmenschep.com/), who seems to be thinking the right stuff.
