---
titles:
- Eternal Sunshine of the Spotless Mind
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - speculative fiction
  - drama
  - comedy
  - science fiction
  - romance
  - fiction
  years:
  - 2004
dates:
  published: 2021-05-21
  updated: 2021-06-08
  experienced:
  - started: 2021-05-20
    finished: 2021-05-20
description: To forget
links:
- https://en.wikipedia.org/wiki/Eternal_Sunshine_of_the_Spotless_Mind
---

Quirky, charming, funny.
