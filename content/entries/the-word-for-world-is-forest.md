---
titles:
- The Word for World Is Forest
taxonomies:
  media:
  - book
  formats:
  - novella
  genres:
  - speculative fiction
  - science fiction
  - fiction
  - far future
  - fantasy
  years:
  - 1972
  - 1976
  artists:
  - Ursula K. Le Guin
collections:
- name: Hainish Cycle
  number: 4
- name: The Ekumen
  number: 2
dates:
  published: 2021-06-19
  updated: 2021-08-07
  experienced:
  - started: 2021-06-19
    finished: 2021-06-22
description: Slavery of a new land and people, leading to their uprising
links:
- https://en.wikipedia.org/wiki/The_Word_for_World_Is_Forest
---

> “...slavery never worked. It was uneconomical.”

A powerful read.

A prequel to the preceding Cycle stories, coinciding with the admittance of Earth (Terra) to the League of All Worlds and subsequent receiving of ansibles.

> “Right, but this isn’t slavery...”
