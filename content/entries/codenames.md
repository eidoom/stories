---
titles:
- Codenames
taxonomies:
  media:
  - game
  formats:
  - board game
  years:
  - 2015
dates:
  published: 2022-05-06
  updated: 2022-05-06
  experienced:
  - '?'
completed: true
links:
- https://boardgamegeek.com/boardgame/178900/codenames
---

