---
titles:
- The Ratcatcher
taxonomies:
  media:
  - book
  formats:
  - short story
  years:
  - 2015
  artists:
  - Carrie Patel
collections:
- name: Pillars of Eternity
  number: 1.21
dates:
  published: 2023-02-12
  updated: 2023-02-12
  experienced:
  - started: 2023-02-12
    finished: 2023-02-12
---
