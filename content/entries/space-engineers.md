---
titles:
- Space Engineers
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - voxel
  - space
  - sandbox
  - indie
  - early access
  years:
  - 2013
  - 2019
  artists:
  - Keen Software House
dates:
  published: 2021-06-06
  updated: 2021-06-06
  experienced:
  - started: 2021-05-22
links:
- https://en.wikipedia.org/wiki/Space_Engineers
- https://www.spaceengineersgame.com/
- https://spaceengineerswiki.com/
---

