---
titles:
- 'Star Trek: Lower Decks S01'
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - science fiction
  - space
  - animated
  - cartoon
  - comedy
  years:
  - 2020
collections:
- name: Star Trek
  number: 6.1
- name: 'Star Trek: Lower Decks'
  number: 1
dates:
  published: 2021-05-14
  updated: 2022-08-28
  experienced:
  - started: 2021-05-14
    finished: 2021-05-20
links:
- https://en.wikipedia.org/wiki/Star_Trek:_Lower_Decks_(season_1)
---

This was very fun and funny, although I do wish it would pause to let me catch my breath.
