---
titles:
- Tiamat’s Wrath
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2019
  artists:
  - James S. A. Corey
collections:
- name: The Expanse
  number: 8
- name: The Expanse (books)
  number: 8
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Tiamat%27s_Wrath
---
