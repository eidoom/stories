---
titles:
- 'Star Trek: Picard S03'
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - speculative fiction
  - science fiction
  - action
  - adventure
  - drama
  - space
  - future
  - aliens
  - artificial intelligence
  - android
  years:
  - 2023
collections:
- name: Star Trek
  number: 9.3
- name: 'Star Trek: Picard'
  number: 3
dates:
  published: 2022-11-01
  updated: 2023-02-17
  experienced:
  - started: 2023-02-16
    finished: 2023-04-20
links:
- https://en.wikipedia.org/wiki/Star_Trek:_Picard_(season_3)
active: true
---
