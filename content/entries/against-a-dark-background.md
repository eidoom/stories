---
titles:
- Against a Dark Background
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  years:
  - 1993
  artists:
  - Iain M. Banks
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - started: 2019-10-03
    finished: 2019-11-14
links:
- https://en.wikipedia.org/wiki/Against_a_Dark_Background
---
