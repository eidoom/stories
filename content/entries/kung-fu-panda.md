---
titles:
- Kung Fu Panda
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2008
  artists:
  - DreamWorks Animation
collections:
- name: Kung Fu Panda
  number: 1
dates:
  published: 2022-06-03
  updated: 2022-06-03
  experienced:
  - started: 2022-06-03
    finished: 2022-06-03
links:
- https://en.wikipedia.org/wiki/Kung_Fu_Panda_(film)
---

