---
titles:
- The Resistance
taxonomies:
  media:
  - game
  formats:
  - board game
  years:
  - 2009
dates:
  published: 2022-05-06
  updated: 2022-05-06
  experienced:
  - '?'
links:
- https://boardgamegeek.com/boardgame/41114/resistance
---

