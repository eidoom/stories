---
titles:
- Half-Life 2
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2004
collections:
- name: Half-Life
  number: 2
- name: Half-Life universe
  number: 2
dates:
  published: 2022-04-21
  updated: 2022-04-21
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Half-Life_2
---
Including:
* Half-Life 2: Lost Coast
* Half-Life 2: Episode One
* Half-Life 2: Episode Two
