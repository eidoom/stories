---
titles:
- Baptism of Fire
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1996
  - 2014
  artists:
  - Andrzej Sapkowski
  - David French
collections:
- name: The Witcher Saga
  number: 3
- name: The Witcher
  number: 5
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Baptism_of_Fire_(novel)
---
