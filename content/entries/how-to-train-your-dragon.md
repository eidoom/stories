---
titles:
- How to Train Your Dragon
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - fantasy
  - speculative fiction
  - dragon
  - viking
  - animated
  - family friendly
  years:
  - 2010
  artists:
  - DreamWorks Animation
dates:
  published: 2021-01-06
  updated: 2021-01-06
  experienced:
  - started: 2020-12-30
    finished: 2020-12-30
description: Delightful
links:
- https://en.wikipedia.org/wiki/How_to_Train_Your_Dragon_(film)
---

This film is a joy to watch.
