---
titles:
- The Expanse S05
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - science fiction
  - hard sci-fi
  - drama
  - space opera
  - mystery
  - thriller
  - horror
  - speculative fiction
  years:
  - 2020
collections:
- name: The Expanse (TV)
  number: 5
- name: The Expanse
  number: 105
dates:
  published: 2021-01-06
  updated: 2022-08-28
  experienced:
  - started: 2021-01-04
    finished: 2021-02-06
links:
- https://en.wikipedia.org/wiki/List_of_The_Expanse_episodes#Season_5_(2020%E2%80%9321)
---

Wow, a great season.

Enjoyed the [aftershow](https://www.youtube.com/playlist?list=PLWz2DO39R-NU5FW-aFfilRvyeg9oXMTgp) from [Ty and Wes](https://tyandthatguy.com/) too.
