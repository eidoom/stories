---
titles:
- Something Ends, Something Begins
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1988
  artists:
  - Andrzej Sapkowski
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
