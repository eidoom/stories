---
titles:
- Age of Empires III
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2005
collections:
- name: Age of Empires
  number: 3
dates:
  published: 2022-05-05
  updated: 2022-05-05
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Age_of_Empires_III
---

