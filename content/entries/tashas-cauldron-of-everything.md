---
titles:
- Tasha's Cauldron of Everything
taxonomies:
  media:
  - book
  formats:
  - rulebook
  - sourcebook
  genres:
  - supplement
  years:
  - 2020
  artists:
  - Wizards of the Coast
collections:
- name: Dungeons & Dragons
  number: 5.24
- name: Dungeons & Dragons 5th Edition
  number: 14
dates:
  published: 2022-11-13
  updated: 2022-11-13
  experienced:
  - started: 2022-11-13
    finished: 2022-11-13
links:
- https://en.wikipedia.org/wiki/Tasha%27s_Cauldron_of_Everything
---
