---
titles:
- Into The Breach
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2018
  artists:
  - Subset Games
dates:
  published: 2022-05-10
  updated: 2022-05-10
  experienced:
  - started: 2022-05-10
links:
- https://en.wikipedia.org/wiki/Into_the_Breach
---

