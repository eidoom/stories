---
titles:
- Hocus Pocus
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 1993
  artists:
  - Walt Disney Pictures
dates:
  published: 2022-12-18
  updated: 2022-12-18
  experienced:
  - started: 2022-12-18
    finished: 2022-12-18
links:
- https://en.wikipedia.org/wiki/Hocus_Pocus_(1993_film)
---
