---
titles:
- Stranger Things S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - horror
  - drama
  - science fiction
  - speculative fiction
  years:
  - 2016
collections:
- name: Stranger Things
  number: 1
dates:
  published: 2022-07-19
  updated: 2022-08-28
  experienced:
  - started: 2022-07-16
    finished: 2022-07-19
links:
- https://en.wikipedia.org/wiki/Stranger_Things_(season_1)
---
