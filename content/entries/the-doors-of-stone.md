---
titles:
- The Doors of Stone
- 'The Kingkiller Chronicle: Day Three'
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - unreleased
  artists:
  - Patrick Rothfuss
collections:
- name: The Kingkiller Chronicle
  number: 3
dates:
  published: 2022-08-01
  updated: 2022-08-01
---

