---
titles:
- Dyson Sphere Program
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - simulation
  - factory
  years:
  - 2021
dates:
  published: 2021-02-02
  updated: 2021-02-07
  experienced:
  - started: 2021-02-01
description: Another 3d Factorio
links:
- https://en.wikipedia.org/wiki/Dyson_Sphere_Program
---

I love the premise, but I’ve too much Factorio fatigue to commit to it just now.
