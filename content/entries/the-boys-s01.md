---
titles:
- The Boys S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - action
  - black comedy
  - drama
  - satire
  - superhero
  years:
  - 2019
  artists:
  - Antony Starr
  - Chace Crawford
  - Christopher Lennertz
  - Dan Stoloff
  - Dominique McElligott
  - Dylan Macleod
  - Elisabeth Shue
  - Eric Kripke
  - Erin Moriarty
  - Evans Brown
  - Jack Quaid
  - Jeff Cutter
  - Jeremy Benning
  - Jessie T. Usher
  - Karen Fukuhara
  - Karl Urban
  - Laz Alonso
  - Matt Bowen
  - Mirosław Baszak
  - Nathan Mitchell
  - Tomer Capone
  provenances:
  - United States
collections:
- name: The Boys
  number: 1
dates:
  published: 2023-03-19
  updated: 2023-03-20
  experienced:
  - started: 2023-03-17
    finished: 2023-03-19
links:
- https://en.wikipedia.org/wiki/The_Boys_(season_1)
---
