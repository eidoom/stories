---
titles:
- 'Fantastic Beasts: The Crimes of Grindelwald'
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2018
collections:
- name: Fantastic Beasts
  number: 2
- name: Wizarding World
  number: 10
dates:
  published: 2022-06-02
  updated: 2022-06-02
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Fantastic_Beasts:_The_Crimes_of_Grindelwald
---

