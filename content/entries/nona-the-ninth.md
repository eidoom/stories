---
titles:
- Nona the Ninth 
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fantasy
  - speculative fiction
  - science fiction
  - fantasy
  - space
  - high fantasy
  - epic fantasy
  - dark fantasy
  - magic
  - necromancy
  - sword fighting
  years:
  - 2022
  artists:
  - Tamsyn Muir
  provenances:
  - New Zealand
  languages:
  - English
collections:
- name: Locked Tomb
  number: 3
dates:
  published: 2023-03-01
  updated: 2023-03-22
  experienced:
  - started: 2023-03-12
    finished: 2023-03-22
links:
- https://en.wikipedia.org/wiki/Nona_the_Ninth
---
