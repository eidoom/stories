---
titles:
- 'The Resistance: Avalon'
taxonomies:
  media:
  - game
  formats:
  - board game
  years:
  - 2012
dates:
  published: 2022-05-06
  updated: 2022-05-06
  experienced:
  - '?'
links:
- https://boardgamegeek.com/boardgame/128882/resistance-avalon
---

