---
titles:
- The Handmaid’s Tale S03
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - speculative fiction
  - dystopian
  - tragedy
  - fiction
  years:
  - 2019
collections:
- name: The Handmaid’s Tale
  number: 3
dates:
  published: 2020-10-08
  updated: 2022-09-09
  experienced:
  - '?'
completed: true
links:
- https://en.wikipedia.org/wiki/List_of_The_Handmaid%27s_Tale_episodes#Season_3_(2019)
---
