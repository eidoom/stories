---
titles:
- The Nano Flower
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1995
  artists:
  - Peter F. Hamilton
collections:
- name: Greg Mandel
  number: 3
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
