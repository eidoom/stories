---
titles:
- The Handmaid’s Tale S02
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - speculative fiction
  - dystopian
  - tragedy
  - fiction
  years:
  - 2018
collections:
- name: The Handmaid’s Tale
  number: 2
dates:
  published: 2020-10-08
  updated: 2022-09-09
  experienced:
  - '?'
completed: true
links:
- https://en.wikipedia.org/wiki/List_of_The_Handmaid%27s_Tale_episodes#Season_2_(2018)
---
