---
titles:
- The Handmaid’s Tale S05
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - speculative fiction
  - dystopian
  - tragedy
  - fiction
  years:
  - 2022
collections:
- name: The Handmaid’s Tale
  number: 5
dates:
  published: 2023-02-17
  updated: 2023-03-01
  experienced:
  - started: 2023-02-25
    finished: 2023-03-01
links:
- https://en.wikipedia.org/wiki/List_of_The_Handmaid%27s_Tale_episodes#Season_5_(2022)
---
