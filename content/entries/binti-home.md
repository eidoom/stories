---
titles:
- 'Binti: Home'
taxonomies:
  media:
  - book
  formats:
  - novella
  years:
  - 2017
  artists:
  - Nnedi Okorafor
dates:
  published: 2022-12-10
  updated: 2022-12-10
links:
- https://en.wikipedia.org/wiki/Binti:_Home
---