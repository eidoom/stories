---
titles:
- 'Cities: Skylines'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2015
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2018-11-22
completed: true
links:
- https://en.wikipedia.org/wiki/Cities:_Skylines
---

