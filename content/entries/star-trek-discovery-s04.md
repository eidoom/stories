---
titles:
- 'Star Trek: Discovery S04'
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - speculative fiction
  - science fiction
  - action
  - adventure
  - drama
  - space
  - future
  - aliens
  years:
  - 2021
collections:
- name: 'Star Trek: Discovery'
  number: 4
- name: Star Trek
  number: 7.4
dates:
  published: 2020-12-05
  updated: 2022-08-28
  experienced:
  - started: 2021-11-18
    finished: 2022-03-19
links:
- https://en.wikipedia.org/wiki/Star_Trek:_Discovery_(season_4)
---

A great take of the far far future: a dystopian power struggle in the wake of the collapse of Starfleet.
Plenty of delightful silliness too.
