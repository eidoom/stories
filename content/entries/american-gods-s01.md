---
titles:
- American Gods S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - drama
  - fantasy
  - speculative fiction
  - comedy
  - fiction
  places:
  - United States
  - America
  years:
  - 2017
collections:
- name: American Gods
  number: 1.1
- name: American Gods (TV)
  number: 1
dates:
  published: 2021-01-10
  updated: 2022-08-28
  experienced:
  - started: 2018-10-01
completed: true
links:
- https://en.wikipedia.org/wiki/American_Gods_(season_1)
---
