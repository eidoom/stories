---
titles:
- 'Top Gun: Maverick'
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2022
dates:
  published: 2022-09-05
  updated: 2022-09-05
  experienced:
  - started: 2022-09-04
    finished: 2022-09-04
links:
- https://en.wikipedia.org/wiki/Top_Gun:_Maverick
---
