---
titles:
- Factorio
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - open world
  - construction and management sim
  - indie
  - early access
  - real-time strategy
  - strategy
  - base builder
  - aliens
  - science fiction
  - 2d
  - speculative fiction
  - rts
  - real-time
  years:
  - 2016
  - 2020
  artists:
  - Wube Software
dates:
  published: 2020-11-22
  updated: 2020-11-25
  experienced:
  - started: 2018-06-12
description: Automate automation
completed: true
links:
- https://en.wikipedia.org/wiki/Factorio
- https://www.factorio.com/
---

The nirvana of satisfaction.
