---
titles:
- 'Codenames: Pictures'
taxonomies:
  media:
  - game
  formats:
  - board game
  years:
  - 2016
dates:
  published: 2022-05-06
  updated: 2022-05-06
  experienced:
  - '?'
completed: true
links:
- https://boardgamegeek.com/boardgame/198773/codenames-pictures
---

