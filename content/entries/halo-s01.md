---
titles:
- Halo S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - action-adventure
  - drama
  - military science fiction
  years:
  - 2022
  artists:
  - 'Bentley Kalu'
  - 'Bokeem Woodbine'
  - 'Charlie Murphy'
  - 'Danny Sapani'
  - 'Jen Taylor'
  - 'Kate Kennedy'
  - 'Kyle Killen'
  - 'Natascha McElhone'
  - 'Natasha Culzac'
  - 'Olive Gray'
  - 'Pablo Schreiber'
  - 'Shabana Azmi'
  - 'Steven Kane'
  - 'Yerin Ha'
dates:
  published: 2023-01-28
  updated: 2023-01-28
  experienced:
  - started: 2023-01-26
    finished: 2023-01-28
links:
- https://en.wikipedia.org/wiki/Halo_(TV_series)
---
