---
titles:
- 'The Elder Scrolls IV: Oblivion'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2006
  genres:
  - fantasy
  - speculative fiction
collections:
- name: The Elder Scrolls
  number: 4
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Elder_Scrolls_IV:_Oblivion
---

