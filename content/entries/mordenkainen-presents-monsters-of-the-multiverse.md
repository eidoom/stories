---
titles:
- 'Mordenkainen Presents: Monsters of the Multiverse'
taxonomies:
  media:
  - book
  formats:
  - rulebook
  - sourcebook
  genres:
  - supplement
  - bestiary
  years:
  - 2022
  artists:
  - Wizards of the Coast
collections:
- name: Dungeons & Dragons
  number: 5.26
- name: Dungeons & Dragons 5th Edition
  number: 16
dates:
  published: 2022-11-13
  updated: 2022-11-13
  experienced:
  - started: 2022-11-13
    finished: 2022-11-13
links:
- https://en.wikipedia.org/wiki/Mordenkainen_Presents:_Monsters_of_the_Multiverse
---

Replaces [Volo's](volos-guide-to-monsters.html) and [Mordenkainen's](mordenkainens-tome-of-foes.html).
