---
titles:
- 'Magic: The Gathering'
taxonomies:
  media:
  - game
  formats:
  - tabletop
  - card game
  genres:
  - fantasy
  years:
  - 1993
dates:
  published: 2022-08-16
  updated: 2022-08-16
  experienced:
  - started: 2022-08-15
    finished: 2022-08-15
links:
- https://en.wikipedia.org/wiki/Magic:_The_Gathering
---

Played with 3 players.
