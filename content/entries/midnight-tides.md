---
titles:
- Midnight Tides
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2004
  artists:
  - Steven Erikson
collections:
- name: Malazan Book of the Fallen
  number: 5
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - '?'
---
