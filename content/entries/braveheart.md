---
titles:
- Braveheart
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - epic
  - heroic
  - historic
  - war
  - scotland
  - fiction
  places:
  - Britain
  - England
  years:
  - 1995
dates:
  published: 2021-07-07
  updated: 2021-07-07
  experienced:
  - started: 2021-07-07
    finished: 2021-07-07
description: Aye
links:
- https://en.wikipedia.org/wiki/Braveheart
---

