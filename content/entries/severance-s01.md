---
titles:
- Severance S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - psychological thriller
  - thriller
  - psychological
  - drama
  - dystopian
  - mystery
  - speculative fiction
  - science fiction
  years:
  - 2022
  artists:
  - 'Adam Scott'
  - 'Aoife McArdle'
  - 'Ben Stiller'
  - 'Britt Lower'
  - 'Christopher Walken'
  - 'Dichen Lachman'
  - 'Jen Tullock'
  - 'John Turturro'
  - 'Michael Chernus'
  - 'Patricia Arquette'
  - 'Tramell Tillman'
  - 'Zach Cherry'
  provenances:
  - United States
collections:
- name: Severance
  number: 1
dates:
  published: 2023-01-07
  updated: 2023-01-07
  experienced:
  - started: 2023-01-06
    finished: 2021-01-07
links:
- https://en.wikipedia.org/wiki/Severance_(TV_series)
---
