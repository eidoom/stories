---
titles:
- Until He Started Screaming
taxonomies:
  media:
  - book
  formats:
  - short story
  years:
  - 2015
  artists:
  - Carrie Patel
collections:
- name: Pillars of Eternity
  number: 1.24
dates:
  published: 2023-02-12
  updated: 2023-02-16
  experienced:
  - started: 2023-02-15
    finished: 2023-02-16
---
