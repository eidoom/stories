---
titles:
- The State of the Art
taxonomies:
  media:
  - book
  formats:
  - collection
  - short story
  - novella
  genres:
  - science fiction
  years:
  - 1991
  artists:
  - Iain M. Banks
collections:
- name: Culture
  number: 4
- name: The Culture
  number: 3
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
