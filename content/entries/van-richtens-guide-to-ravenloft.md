---
titles:
- Van Richten's Guide to Ravenloft
taxonomies:
  media:
  - book
  years:
  - 2021
  formats:
  - sourcebook
  artists:
  - Wizards of the Coast
  genres:
  - campaign
collections:
- name: Dungeons & Dragons
  number: 5.37
- name: Dungeons & Dragons 5th Edition
  number: 27
dates:
  published: 2022-11-13
  updated: 2022-11-13
  experienced:
  - started: 2022-11-13
    finished: 2022-11-13
links:
- https://en.m.wikipedia.org/wiki/Van_Richten%27s_Guide_to_Ravenloft
---
