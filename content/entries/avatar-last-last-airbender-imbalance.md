---
titles:
- 'Avatar: The Last Airbender – Imbalance'
taxonomies:
  media:
  - book
  formats:
  - graphic novel
  years:
  - 2018
collections:
- name: 'Avatar: The Last Airbender'
  number: 67
dates:
  published: 2023-04-18
  updated: 2023-04-18
#  experienced:
#  - started: 2023-04-18
#    finished: 2023-04-18
---
