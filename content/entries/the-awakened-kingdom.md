---
titles:
- The Awakened Kingdom
taxonomies:
  media:
  - book
  formats:
  - novella
  years:
  - 2014
  artists:
  - N. K. Jemisin
collections:
- name: Inheritance
  number: 3.5
dates:
  published: 2020-08-02
  updated: 2021-07-30
  experienced:
  - started: 2020-08-19
    finished: 2020-08-20
---
