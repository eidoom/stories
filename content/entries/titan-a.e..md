---
titles:
- Titan A.E.
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - animated
  - science fiction
  - post-apocalypse
  - adventure
  years:
  - 2000
dates:
  published: 2022-06-26
  updated: 2022-06-26
  experienced:
  - started: 2022-06-21
    finished: 2022-06-21
links:
- https://en.wikipedia.org/wiki/Titan_A.E.
---

