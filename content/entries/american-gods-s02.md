---
titles:
- American Gods S02
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - drama
  - fantasy
  - speculative fiction
  - comedy
  - fiction
  places:
  - United States
  - America
  years:
  - 2019
collections:
- name: American Gods
  number: 1.2
- name: American Gods (TV)
  number: 2
dates:
  published: 2021-01-10
  updated: 2022-08-28
  experienced:
  - "?"
completed: true
links:
- https://en.wikipedia.org/wiki/American_Gods_(season_2)
---
