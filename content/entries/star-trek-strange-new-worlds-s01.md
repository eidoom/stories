---
titles:
- 'Star Trek: Strange New Worlds S01'
taxonomies:
  media:
  - video
  formats:
  - television
  years:
  - 2022
  genres:
  - science fiction
collections:
- name: 'Star Trek: Strange New Worlds'
  number: 1
- name: Star Trek
  number: 11.1
dates:
  published: 2022-05-07
  updated: 2022-05-07
  experienced:
  - started: 2022-05-06
    finished: 2022-07-07
links:
- https://en.wikipedia.org/wiki/Star_Trek:_Strange_New_Worlds_(season_1)
---
