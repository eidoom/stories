---
titles:
- The Temporal Void
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2008
  artists:
  - Peter F. Hamilton
collections:
- name: The Void Trilogy
  number: 2
- name: Commonwealth universe
  number: 4
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
