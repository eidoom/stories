---
titles:
- Tolkien Gateway
taxonomies:
  media:
  - web
  years:
  - 2005
  formats:
  - digital encyclopædia
  - wiki
dates:
  published: 2022-10-31
  updated: 2022-10-31
  experienced:
  - started: 2022-10-31
collections:
- name: Middle-Earth
  number: 0
links:
- https://tolkiengateway.net/wiki/Main_Page
---
