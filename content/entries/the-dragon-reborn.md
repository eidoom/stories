---
titles:
- The Dragon Reborn
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1991
  artists:
  - Robert Jordan
collections:
- name: The Wheel of Time
  number: 3
- name: The Wheel of Time universe
  number: 3
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
