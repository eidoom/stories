---
titles:
- Mother of Daemons
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  years:
  - 2020
  artists:
  - David Hair
collections:
- name: Sunsurge Quartet
  number: 4
- name: Mage’s Blood
  number: 8
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - started: 2020-06-03
    finished: 2020-06-09
links:
- https://davidhairauthor.com/Blog/Post/2639/MOTHER-OF-ENDINGS-FINAL-BOOKS-AND-NEW-BEGINNINGS
---
