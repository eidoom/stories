---
titles:
- RimWorld
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - simulator
  - construction and management sim
  - base builder
  - colony
  - speculative fiction
  - science fiction
  - drama
  - story
  - survival
  - strategy
  - indie
  years:
  - 2013
  - 2018
  artists:
  - Ludeon Studios
dates:
  published: 2020-11-22
  updated: 2020-11-25
  experienced:
  - started: 2015-10-07
completed: true
description: Firefly-esque colony sim
links:
- https://en.wikipedia.org/wiki/RimWorld
- https://rimworldgame.com/
---

A rich story generator.
Often produces hilarious circumstances.
Punishingly merciless at delivering setbacks.
Very fun, just don’t get too attached to anyone or you’re going to rage quit at some point.
