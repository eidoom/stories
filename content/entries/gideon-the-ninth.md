---
titles:
- Gideon the Ninth 
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fantasy
  - speculative fiction
  - science fiction
  - fantasy
  - space
  - high fantasy
  - epic fantasy
  - dark fantasy
  - magic
  - necromancy
  - sword fighting
  - horror
  - comedy
  - comedy horror
  years:
  - 2019
  artists:
  - Tamsyn Muir
  provenances:
  - New Zealand
  languages:
  - English
collections:
- name: Locked Tomb
  number: 1
dates:
  published: 2023-02-11
  updated: 2023-03-02
  experienced:
  - started: 2023-02-11
    finished: 2023-03-01
links:
- https://en.wikipedia.org/wiki/Gideon_the_Ninth
---
