---
titles:
- Hero
- 英雄
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - wuxia
  - historical
  - fantasy
  - fiction
  - speculative fiction
  - martial arts
  - Chinese
  years:
  - 2002
  languages:
  - Mandarin
  places:
  - China
  artists:
  - Zhang Yimou
  - Jet Li
  - Tony Leung
  - Maggie Cheung
  - Zhang Ziyi
  - Chen Daoming
  - Donnie Yen
  - Tan Dun
dates:
  published: 2022-11-27
  updated: 2022-11-27
  experienced:
  - started: 2022-11-27
    finished: 2022-11-27
links:
- https://en.wikipedia.org/wiki/Hero_(2002_film)
---
