---
titles:
- The Mandalorian S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - science fiction
  - fantasy
  - speculative fiction
  - space opera
  - space
  - fiction
  years:
  - 2019
collections:
- name: Star Wars
  number: 6.51
- name: The Mandalorian
  number: 1
- name: The Mandalorian +
  number: 1
dates:
  published: 2020-12-05
  updated: 2021-01-10
  experienced:
  - started: 2019-11-12
    finished: 2019-12-27
links:
- https://en.wikipedia.org/wiki/The_Mandalorian_(season_1)
---

