---
titles:
- The Expanse S06
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - science fiction
  - hard sci-fi
  - drama
  - space opera
  - mystery
  - thriller
  - horror
  - speculative fiction
  years:
  - 2021
collections:
- name: The Expanse (TV)
  number: 6
- name: The Expanse
  number: 106
dates:
  published: 2021-01-06
  updated: 2022-08-28
  experienced:
  - started: 2022-01-16
    finished: 2022-01-16
links:
- https://en.wikipedia.org/wiki/List_of_The_Expanse_episodes#Season_6_(2021%E2%80%9322)
---

We did a group watching of the season on my projector.
