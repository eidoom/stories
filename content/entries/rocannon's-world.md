---
titles:
- Rocannon’s World
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - speculative fiction
  - science fiction
  - fiction
  - far future
  - fantasy
  - epic
  - heroic
  - romantic
  years:
  - 1966
  artists:
  - Ursula K. Le Guin
collections:
- name: Hainish Cycle
  number: 1
- name: The Ekumen
  number: 3
dates:
  published: 2021-06-02
  updated: 2021-07-01
  experienced:
  - started: 2021-05-30
    finished: 2021-06-02
description: Tolkienesque but also Einsteinian
links:
- https://en.wikipedia.org/wiki/Rocannon%27s_World
---

A tale woven by a masterful storyteller with indulgently romantic prose.
The fantastical setting within the science fiction context is a fun one.

In the Hainish Cycle, we meet the League of All Worlds and see the inception of telepathy within it.
Interesting how psychic abilities were treated as possibility in late science fiction (Asimov’s Foundation series come to mind) unlike today.
We also are introduced to the War To Come and the idea that will become the Cultural Embargo.

I enjoy how while others cry FTL (Faster Than Light), Le Guin is more concerned with the social effects of NAFAL (Nearly As Fast As Light), as she will [later](the-left-hand-of-darkness.html) coin it.
The extreme conditions of FTL mean it is possible for machines but people are stuck with NAFAL.
