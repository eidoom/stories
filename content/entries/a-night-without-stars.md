---
titles:
- A Night Without Stars
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2016
  artists:
  - Peter F. Hamilton
collections:
- name: The Chronicle of the Fallers
  number: 2
- name: Commonwealth universe
  number: 8
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
