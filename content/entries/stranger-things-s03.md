---
titles:
- Stranger Things S03
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - horror
  - drama
  - science fiction
  - speculative fiction
  years:
  - 2019
collections:
- name: Stranger Things
  number: 3
dates:
  published: 2022-07-19
  updated: 2022-08-28
  experienced:
  - started: 2022-08-26
    finished: 2022-08-27
links:
- https://en.wikipedia.org/wiki/Stranger_Things_(season_3)
---
