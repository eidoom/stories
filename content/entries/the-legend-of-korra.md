---
titles:
- The Legend of Korra
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - action
  - adventure
  - comedy drama
  - science fantasy
  - steampunk
  years:
  - 2012
  artists:
  - Bryan Konietzko
  - David Faustino
  - Dee Bradley Baker
  - J. K. Simmons
  - Janet Varney
  - Jeremy Zuckerman
  - Kiernan Shipka
  - Michael Dante DiMartino
  - Mindy Sterling
  - P. J. Byrne
  - Seychelle Gabriel
  provenances:
  - United States
dates:
  published: 2023-03-30
  updated: 2023-03-30
#  experienced:
#  - started: 2023-03-30
#    finished: 2023-03-30
links:
- https://en.wikipedia.org/wiki/The_Legend_of_Korra
---