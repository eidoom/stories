---
titles:
- 'Star Trek: Discovery S03'
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - speculative fiction
  - science fiction
  - action
  - adventure
  - drama
  - space
  - future
  - aliens
  years:
  - 2020
collections:
- name: 'Star Trek: Discovery'
  number: 3
- name: Star Trek
  number: 7.3
dates:
  published: 2020-12-05
  updated: 2022-08-28
  experienced:
  - started: 2020-10-15
    finished: 2020-01-07
links:
- https://en.wikipedia.org/wiki/Star_Trek:_Discovery_(season_3)
---

A great take of the far far future: a dystopian power struggle in the wake of the collapse of Starfleet.
Plenty of delightful silliness too.
