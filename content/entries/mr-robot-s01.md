---
titles:
- Mr Robot S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - psychological thriller
  - drama
  - psychological
  - thriller
  - technology
  - corporate capitalism
  places:
  - United States
  - America
  years:
  - 2015
collections:
- name: Mr Robot
  number: 1
dates:
  published: 2021-02-24
  updated: 2022-08-28
  experienced:
  - started: 2021-02-23
    finished: 2021-02-25
links:
- https://en.wikipedia.org/wiki/List_of_Mr._Robot_episodes#Season_1_(2015)
---

Let’s get this out for starters: what they did to Bill Harper was unforgivable.
Neither was Shayla’s fate.
Partially balanced by the existence of Gideon and Harry Goddard.

This is a story about Elliot Alderson.
He is a brilliant hacker, severely introverted, paranoid, and angry at the corporate controlled world around him.

He is clearly mentally unstable.
He even states it to us directly on many occasions.
At the start, he explains how he hears E Corp (think [Big Tech](https://en.wikipedia.org/wiki/Big_Tech) all rolled up in one) as Evil Corp.
We proceed to hear it as Evil Corp from everyone: we’re seeing from his perspective, and he is not a reliable narrator.
This is reinforced by drug-induced sequences.

The eponymous Mr Robot is a perfectly believable character, if dangerously mad.
As time goes on, small curiosities crop up though.
They becomes increasingly insistent, like towards the end when Mr Robot turns up at Allsafe and nobody else bats an eyelid.
Then he’s Elliot’s father, and Darlene’s his sister, and he’d forgotten.
In the culmination of Elliot’s descent into madness, they pull a Fight Club on us: “You’re going to make me say it, aren’t you? I’m Mr Robot”.
I didn’t even see it coming.

Other things are obvious foreshadow in retrospect, like Darlene responding “Why shouldn’t I know where you live?” when she first picks him up from his flat, or rewatching scenes with Mr Robot and Elliot to see how the other characters behave towards “them”.
In particular, every time Elliot said or thought Mr Robot was crazy, it was just another confession.

Oh, and they pull off the hack to cripple Evil Corp.
(Go fsociety.)
Somehow...
Something’s going on with the other mad guy, Tyrell Wellick.
And the post-credit scene show that Whiterose, trans leader of the Dark Army (DA), is also some kind of Chinese business giant?
Roll season two, please.
