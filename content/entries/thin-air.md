---
titles:
- Thin Air
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2018
  artists:
  - Richard K. Morgan
collections:
- name: Genetically modified
  number: 2
dates:
  published: 2020-03-01
  updated: 2021-07-30
  experienced:
  - started: 2020-04-13
    finished: 2020-04-24
links:
- https://en.wikipedia.org/wiki/Thin_Air_(Morgan_novel)
---
