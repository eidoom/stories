---
titles:
- Frostpunk
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2018
  artists:
  - 11 bit studios
dates:
  published: 2022-05-10
  updated: 2022-11-24
  experienced:
  - started: 2022-05-09
links:
- https://en.wikipedia.org/wiki/Frostpunk
---

Scenarios:

* A New Home 2022-11-05 -- 2022-11-06
* The Last Autumn 2022-11-08 -- 2022-11-11
