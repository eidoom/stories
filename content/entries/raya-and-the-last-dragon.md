---
titles:
- Raya and the Last Dragon
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - animated
  - fiction
  - fantasy
  - speculative fiction
  - family friendly
  - dragon
  years:
  - 2021
  artists:
  - Walt Disney Animation Studios
dates:
  published: 2021-03-12
  updated: 2021-03-12
  experienced:
  - started: 2021-03-12
    finished: 2021-03-12
description: An Asian fantasy
links:
- https://en.wikipedia.org/wiki/Raya_and_the_Last_Dragon
---

Damn, that’s a good render.
