---
titles:
- Pillars of Eternity
- "Pillars of Eternity: The White March – Part I"
- "Pillars of Eternity: The White March – Part II"
- "Pillars of Eternity: Definitive Edition"
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2015
  artists:
  - Obsidian Entertainment
  genres:
  - crpg
  - isometric
collections:
- name: Pillars of Eternity
  number: 1
dates:
  published: 2022-09-03
  updated: 2023-03-05
  experienced:
  - started: 2023-01-14
    finished: 2023-03-04
links:
- https://en.wikipedia.org/wiki/Pillars_of_Eternity
- https://en.wikipedia.org/wiki/Pillars_of_Eternity:_The_White_March
- https://eternity.obsidian.net/eternity/
---

