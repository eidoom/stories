---
titles:
- The Boys S04
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - action
  - black comedy
  - drama
  - satire
  - superhero
  years:
  - unreleased
  artists:
  - Antony Starr
  - Chace Crawford
  - Christopher Lennertz
  - Claudia Doumit
  - Colby Minifie
  - Dan Stoloff
  - Dominique McElligott
  - Dylan Macleod
  - Elisabeth Shue
  - Eric Kripke
  - Erin Moriarty
  - Evans Brown
  - Jack Quaid
  - Jeff Cutter
  - Jensen Ackles
  - Jeremy Benning
  - Jessie T. Usher
  - Karen Fukuhara
  - Karl Urban
  - Laz Alonso
  - Matt Bowen
  - Mirosław Baszak
  - Nathan Mitchell
  - Tomer Capone
  provenances:
  - United States
collections:
- name: The Boys
  number: 4
dates:
  published: 2023-03-29
  updated: 2023-03-29
#  experienced:
#  - started: 2023-03-25
#    finished: 2023-03-29
links:
- https://en.wikipedia.org/wiki/The_Boys_(season_4)
---
