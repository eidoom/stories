---
titles:
- The Naked God
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1999
  artists:
  - Peter F. Hamilton
collections:
- name: The Night’s Dawn Trilogy
  number: 3
- name: Confederation universe
  number: 3
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
