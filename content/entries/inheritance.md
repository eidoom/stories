---
titles:
- Inheritance
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2011
  artists:
  - Christopher Paolini
collections:
- name: The Inheritance Cycle
  number: 4
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - '?'
---
