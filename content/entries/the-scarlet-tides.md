---
titles:
- The Scarlet Tides
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  years:
  - 2013
  artists:
  - David Hair
collections:
- name: Moontide Quartet
  number: 2
- name: Mage’s Blood
  number: 2
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
