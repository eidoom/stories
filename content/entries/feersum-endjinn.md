---
titles:
- Feersum Endjinn
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  years:
  - 1994
  artists:
  - Iain M. Banks
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - started: 2020-01-23
    finished: 2020-02-18
links:
- https://en.wikipedia.org/wiki/Feersum_Endjinn
---
