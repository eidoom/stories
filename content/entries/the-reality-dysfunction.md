---
titles:
- The Reality Dysfunction
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1996
  artists:
  - Peter F. Hamilton
collections:
- name: The Night’s Dawn Trilogy
  number: 1
- name: Confederation universe
  number: 1
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
