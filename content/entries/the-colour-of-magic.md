---
titles:
- The Colour of Magic
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  - fiction
  years:
  - 1983
  artists:
  - Terry Pratchett
collections:
- name: Rincewind
  number: 1
- name: Discworld
  number: 1
dates:
  published: 2021-09-23
  updated: 2021-09-23
  experienced:
  - started: 2022-05-27
    finished: 2022-05-28
links:
- https://en.wikipedia.org/wiki/The_Colour_of_Magic
---

