---
titles:
- 'The Lord of the Rings: The Battle for Middle-earth'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2004
collections:
- name: Middle-Earth
  number: 31.31
dates:
  published: 2022-05-05
  updated: 2022-05-05
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Lord_of_the_Rings:_The_Battle_for_Middle-earth
---

