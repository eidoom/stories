---
titles:
- Player's Handbook 5th Edition
taxonomies:
  media:
  - book
  formats:
  - rulebook
  - sourcebook
  genres:
  - core
  years:
  - 2014
  artists:
  - Wizards of the Coast
collections:
- name: Dungeons & Dragons
  number: 5.11
- name: Dungeons & Dragons 5th Edition
  number: 1
dates:
  published: 2022-11-13
  updated: 2022-11-13
  experienced:
  - started: 2022-01-23
    finished: 2022-01-23
links:
- https://en.wikipedia.org/wiki/Player%27s_Handbook#Dungeons_&_Dragons_5th_edition
---
