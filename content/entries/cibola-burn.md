---
titles:
- Cibola Burn
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2014
  artists:
  - James S. A. Corey
collections:
- name: The Expanse
  number: 4
- name: The Expanse (books)
  number: 4
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Cibola_Burn
---
