---
titles:
- The Boys S02
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - action
  - black comedy
  - drama
  - satire
  - superhero
  years:
  - 2020
  artists:
  - Antony Starr
  - Aya Cash
  - Chace Crawford
  - Christopher Lennertz
  - Colby Minifie
  - Dan Stoloff
  - Dominique McElligott
  - Dylan Macleod
  - Elisabeth Shue
  - Eric Kripke
  - Erin Moriarty
  - Evans Brown
  - Jack Quaid
  - Jeff Cutter
  - Jeremy Benning
  - Jessie T. Usher
  - Karen Fukuhara
  - Karl Urban
  - Laz Alonso
  - Matt Bowen
  - Mirosław Baszak
  - Nathan Mitchell
  - Tomer Capone
  provenances:
  - United States
collections:
- name: The Boys
  number: 2
dates:
  published: 2023-03-20
  updated: 2023-03-21
  experienced:
  - started: 2023-03-20
    finished: 2023-03-24
links:
- https://en.wikipedia.org/wiki/The_Boys_(season_2)
---
