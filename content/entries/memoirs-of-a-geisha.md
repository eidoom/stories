---
titles:
- Memoirs of a Geisha
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2005
  artists:
  - Rob Marshall
  - Zhang Ziyi
  - Ken Watanabe
  - Michelle Yeoh
  - Kōji Yakusho
  - Youki Kudoh
  - Kaori Momoi
  - Gong Li
  - John Williams
  genres:
  - period
  - drama
  - historical
  - world war two
  places:
  - Japan
  languages:
  - English
  provenances:
  - United States
dates:
  published: 2022-12-02
  updated: 2022-12-04
  experienced:
  - started: 2022-12-04
    finished: 2022-12-04
links:
- https://en.wikipedia.org/wiki/Memoirs_of_a_Geisha_(film)
---
