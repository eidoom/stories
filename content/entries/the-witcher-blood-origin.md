---
titles:
- 'The Witcher: Blood Origin'
taxonomies:
  media:
  - video
  formats:
  - television
  - miniseries
  years:
  - 2022
  genres:
  - prequel
  - 'action'
  - 'adventure'
  - 'drama'
  - 'fantasy'
  artists:
  - 'Amy Murray'
  - 'Declan de Barra'
  - 'Dylan Moran'
  - 'Francesca Mills'
  - 'Huw Novelli'
  - 'Jacob Collins Levy'
  - 'Joey Batey'
  - 'Lauren Schmidt Hissrich'
  - "Laurence O'Fuarain"
  - 'Lenny Henry'
  - 'Lizzie Annis'
  - 'Michelle Yeoh'
  - 'Minnie Driver'
  - 'Mirren Mack'
  - 'Sophia Brown'
  - 'Zach Wyatt'
collections:
- name: The Witcher (TV)
  number: -1
- name: The Witcher
  number: -1
dates:
  published: 2023-01-07
  updated: 2023-01-07
  experienced:
  - started: 2023-01-05
    finished: 2023-01-06
links:
- https://en.wikipedia.org/wiki/The_Witcher:_Blood_Origin
---
