---
titles:
- Knives Out
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - mystery
  years:
  - 2019
  artists:
  - 'Ana de Armas'
  - 'Chris Evans'
  - 'Christopher Plummer'
  - 'Daniel Craig'
  - 'Don Johnson'
  - 'Jaeden Martell'
  - 'Jamie Lee Curtis'
  - 'Katherine Langford'
  - 'LaKeith Stanfield'
  - 'Michael Shannon'
  - 'Ram Bergman'
  - 'Rian Johnson'
  - 'Toni Collette'
collections:
- name: Knives Out
  number: 1
dates:
  published: 2023-02-05
  updated: 2023-02-05
  experienced:
  - started: 2023-02-04
    finished: 2023-02-04
links:
- https://en.wikipedia.org/wiki/Knives_Out
---
