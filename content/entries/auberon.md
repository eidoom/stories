---
titles:
- Auberon
taxonomies:
  media:
  - book
  formats:
  - novella
  years:
  - 2019
  artists:
  - James S. A. Corey
collections:
- name: The Expanse
  number: 8.5
- name: The Expanse (books)
  number: 8.5
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Persepolis_Rising#%22Auberon%22
---
