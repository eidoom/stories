---
titles:
- The Proposal
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - comedy
  - drama
  - romance
  places:
  - United States
  - America
  years:
  - 2009
dates:
  published: 2021-01-24
  updated: 2021-02-07
  experienced:
  - started: 2021-01-24
    finished: 2021-01-24
description: Funny, if dated at times
links:
- https://en.wikipedia.org/wiki/The_Proposal_(2009_film)
---

Outside my usual repertoire, I know.
Some jokes didn’t age well, but a laugh overall.
