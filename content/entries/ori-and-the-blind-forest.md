---
titles:
- Ori and the Blind Forest
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - platformer
  - adventure
  - metroidvania
  - 2.5d
  - fantasy
  - speculative fiction
  - fiction
  years:
  - 2015
  artists:
  - Moon Studios
collections:
- name: Ori
  number: 1
dates:
  published: 2020-11-06
  updated: 2023-02-24
  experienced:
  - started: 2015-11-03
    #finished: 2023-
description: A masterpiece of art
links:
- https://en.wikipedia.org/wiki/Ori_and_the_Blind_Forest
- https://www.orithegame.com/blind-forest/
active: true
---

I’m not much of a player of platformers, but this game deserved my attention.
The visual artwork, story, and music come together to create a truly beautiful game.
