---
titles:
- The Wind’s Twelve Quarters
taxonomies:
  media:
  - book
  formats:
  - collection
  - short story
  genres:
  - speculative fiction
  - science fiction
  - fiction
  - far future
  years:
  - 1975
  artists:
  - Ursula K. Le Guin
dates:
  published: 2021-07-10
  updated: 2021-08-12
  experienced:
  - started: 2021-07-01
    finished: 2021-07-12
description: A collection of Le Guin’s earliest short stories
links:
- https://en.wikipedia.org/wiki/The_Wind%27s_Twelve_Quarters
- https://www.ursulakleguin.com/the-winds-twelve-quarters
---

This collection of short stories includes a foreword and a preface to each story.

{{ short(title="Semley’s Necklace", link="https://en.wikipedia.org/wiki/The_Dowry_of_Angyar", years=[”1964“,”1966“,”1975"], uni="The Ekumen", alt="The Dowry of Angyar") }}

Semley embarks on an odyssey to find her family’s lost necklace, taking her astronomically far from home.
On her return, she finds she has paid the price in time dilation.

Read as prologue to [Rocannon’s World](rocannons-world.html).

{{ short(title="April in Paris“, years=[”1962“,”1975"],started="2021-08-02") }}

A lonely alchemist uses dark arts to teleport people from past and future to him as companions.

{{ short(title="The Masters“, years=[”1963“,”1975"],started="2021-08-03") }}

The world is hinted to be a post-apocalyptic future, with sky almost always obscured by cloud.
A newly-initiated Master is introduced to the forbidden knowledge of arithmetic by his friend and subsequently flees the city in fear of his life after the friend is declared a heretic for practicing the dark art of classical mechanics and burnt alive.

{{ short(title="Darkness Box“, years=[”1963“,”1975"],started="2021-08-04") }}

In a static world of light with no sun and no shadow, a witch’s son finds a box of darkness.
He gives it to a prince who is stuck in a temporal cycle of confronting his exiled brother on the battleground.
The opening of the box provides the yin to the prince’s yang.

{{ short(title="The Word of Unbinding", link="https://en.wikipedia.org/wiki/The_Word_of_Unbinding", years=[”1964“,”1975"],started="2021-08-05",uni="Earthsea") }}

Festin, wizard of an island of the Outer Reach, is imprisoned by Voll the Fell.
He seeks escape by Transforming into mist, air, animals, even the “magic of going home”, but the unseen Voll catches him every time.
Finally, he takes the only path left to him and speaks the eponymous Word.
In death, he finds the ruinous Voll undead and commands him to rest.

{{ short(title="The Rule of Names", link="https://en.wikipedia.org/wiki/The_Rule_of_Names", years=[”1964“,”1975"],uni="Earthsea",started="2021-08-06",finished="2021-08-07") }}

Mr. Underhill is a pathetic little man to be the wizard of Sattins Island of the Outer Reach of the Archipelago, but there you go.
Blackbeard, descendant of the Lords of Pendor, comes to reclaim his family’s stolen treasure, Inalkil the Greenstone, bearing the theif’s true name through black magic.
Calling “Yevaud”, Mr. Underhill’s true form is revealed to be a dragon.

{{ short(title="Winter’s King“, years=[”1969“,”1975"], started="2021-07-11", link="https://en.wikipedia.org/wiki/Winter%27s_King", uni="The Ekumen") }}

A first visit to Karhide on Gethen, set after [*The Left Hand of Darkness*](the-left-hand-of-darkness.html) judging by the king’s regnal number.

The king, Argaven Harge, is subject to mind alteration, so abdicates in favour of their child, Emran, and seeks ayslum on the Ekumenical planet of Ollul, studying there.
Relativity take its toll on the NAFAL travel, so when they return after ansible reports of unrest, Argaven finds their child an old tyrant.
Argaven is reinstated as king.

On Argaven XVII: the names of the kings of Karhide always cycle between Arvagen and Emran, which shouldn’t come as a surprise when it is always Year One.
The mad king of *The Left Hand of Darkness* is Argaven XV.

{{ short(title="The Good Trip“, years=[”1970“,”1975"],started="2021-08-08",finished="2021-08-09") }}

A man hallucinates (sober) that he hallucinates (psychedelically) about meeting his wife, who has been admitted to a psychiatric hospital, on a mountain he can see from his window.
Through this experience, he awakens from despair in his loss to enjoying a cherished memory of their time together.

{{ short(title="Nine Lives", link="https://en.wikipedia.org/wiki/Nine_Lives_(novelette)", years=[”1969“,”1975"],started="2021-08-09",uni="The Ekumen") }}

This story seems to be from a similar period of Earth’s future history as [The Word for World is Forest](the-word-for-world-is-forest.html); although the links are tenuous, I’ll say they share the Ekumen universe.
It’s not about a cat.

It opens with two prospectors alone on a desolate Far Out planet, workers just earning their keep out in the bleakness of space, in the vein of the crew of the *Nostromo* (this was surely an inspiration for [Alien](alien.html)).
They are joined by a mining crew of clones, ten people sharing identical genes (called John Chow, each with a middle name derived from a letter of the Semitic abjads).
Le Guin uses this to probe the human experience: the prospectors each intimately alone against the virtuous circle of self felt by the clone group; then when all but one of the clones are killed in a quake, the survivor must learn how to connect with a stranger.

{{ short(title="Things", alt="The End“, years=[”1970“,”1975"],started="2021-08-09") }}

On an island, it is The End.
Ragers go around destroying everything because it is the end.
A bricklayer dreams of other islands, where he could have a future.
He attempts to lay a causeway in secret under the sea to reach these new islands, helped by a friend.
On walking the unfinished bridge in desperation, they are rescued by sailors from the other islands.

{{ short(title="A Trip to the Head“, years=[”1970“,”1975"],started="2021-08-10") }}

A person emerges from a forest without knowledge of who they are.
They think they are this person or that person, recalling memories, but reject those personas.
They talk with another person who reveals some details of the future Earth.
They remember their name but then deny it by returning to the forest where they forget again.

{{ short(title="Vaster than Empires and More Slow", link="https://en.wikipedia.org/wiki/Vaster_than_Empires_and_More_Slow", uni="The Ekumen", started="2021-07-11", finished="2021-07-13“, years=[”1970“,”1975"]) }}

A group of neurotic explorers on an empathic Gaia world.

{{ short(title="The Stars Below“, years=[”1973“,”1975"],started="2021-08-10") }}

An astronomer, declared a heretic, his observatory razed, hides in an old mine.
He befriends some old miners there, who find nothing of value but have no other trade.
The astronomer builds a new telescope and starts seeing stars beneath the earth.
He tells the miners to look at a particular spot and disappears into the depths the next day.
The miners find a new seam of silver at the marked spot.

{{ short(title="The Field of Vision“, years=[”1973“,”1975"],started="2021-08-11") }}

The Psyche XIV returns from its expedition to Mars, where a Room of a City was explored.
Of the crew, one is blind, one is deaf, and one is dead.
The survivors have been imprinted with the perception of God, the Room being a conversion site left by ancient alien missionaries.
The blind one suicides as the world adopts this new religion.

{{ short(title="Direction of the Road“, years=[”1974“,”1975"],started="2021-08-11",finished="2021-08-12") }}

The visually perceived size of an object is relative to its distance, so this is the story of an oak tree who has dutifully—“[n]oblesse oblige”—spent its life growing larger and smaller as people enter and leave its field of view.
In that time, people have gone from walking and horse riding, admiring the tree while passing, to driving past in droves without a glance.

{{ short(title="The Ones Who Walk Away from Omelas", link="https://en.wikipedia.org/wiki/The_Ones_Who_Walk_Away_from_Omelas", years=[”1973“,”1975“,”2014"],started="2021-08-12") }}

Omelas is a utopian paradise build upon the promise of the unrelenting suffering of one child.

The prose is so striking, so stylised, I immediately recognised in it Jemisin’s response, [*The Ones Who Stay and Fight*](how-long-til-black-future-month.html).

{{ short(title="The Day Before the Revolution", uni="The Ekumen", started="2021-07-01“, years = [”1974“,”1975“], links = ["https://en.wikipedia.org/wiki/The_Day_Before_the_Revolution"]) }}

Laia Odo’s last day.

A prologue for [The Dispossessed](the-dispossessed.html).
