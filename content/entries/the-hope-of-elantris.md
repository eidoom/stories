---
titles:
- The Hope of Elantris
taxonomies:
  media:
  - book
  formats:
  - short story
  years:
  - 2006
  artists:
  - Brandon Sanderson
collections:
- name: Elantris
  number: 1.5
- name: Cosmere
  number: 0
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Elantris#The_Hope_of_Elantris
---
