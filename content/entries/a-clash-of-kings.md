---
titles:
- A Clash of Kings
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1998
  artists:
  - George R. R. Martin
collections:
- name: A Song of Ice and Fire
  number: 2
- name: Game of Thrones
  number: 2
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
