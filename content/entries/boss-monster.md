---
titles:
- 'Boss Monster: The Dungeon Building Card Game'
- 'Boss Monster 2: The Next Level'
taxonomies:
  media:
  - game
  formats:
  - tabletop
  - card game
  genres:
  - fantasy
  years:
  - 2013
dates:
  published: 2022-08-16
  updated: 2022-08-16
  experienced:
  - started: 2022-08-15
    finished: 2022-08-15
links:
- https://boardgamegeek.com/boardgame/131835/boss-monster-dungeon-building-card-game
---

Played with 4 players.
