---
titles:
- The Wandering Earth
taxonomies:
  media:
  - book
  formats:
  - collection
  - short story
  - novella
  genres:
  - speculative fiction
  - science fiction
  - hard sci-fi
  - space
  - fiction
  places:
  - China
  years:
  - 2017
  artists:
  - Liu Cixin
  - Holger Nahm
dates:
  published: 2020-11-22
  updated: 2021-08-05
  experienced:
  - started: 2020-11-10
    finished: 2020-11-22
description: Hard sci-fi shorts
links:
- https://en.wikipedia.org/wiki/Liu_Cixin#Works_of_short_fiction
---

A collection of short stories and novellas by Liu Cixin, translated from Chinese to English by Holger Nahm.

{{ short(title="The Wandering Earth", alt="流浪地球", year=2000) }}

Great engines are constructed on the Earth’s face to drive it to another solar system in a centuries-long odyssey to escape our Sun, which threatens to metamorphose into a deadly red giant.
We see the swinging tides of humanity’s societal reaction to this, and the epic transformation of the Earth under such destructive manipulation.

{{ short(title="Mountain", alt="山", year=2006) }}

Some aliens pop by to visit the Earth on their way past.
They talk to a man who is a mountain climbing enthusiast, but has punitively exiled himself to the ocean after a fatal climb for his team.
The aliens describe their ancient history up to space flight; they are silicon-based lifeforms who evolved in the weightless, vacuum core of a hollow rock planet.

{{ short(title="Of Ants and Dinosaurs", alt="白垩纪往事", year=2004, uni="Dinosaur Universe", num=1, rest=[2, "./#devourer", 3, "../hold-up-the-sky#cloud-of-poems"]) }}

In the [Cretaceous Period](https://en.wikipedia.org/wiki/Cretaceous), ants and dinosaurs formed a symbiotic high-tech society with ants providing exquisite engineering and dinosaurs the ideas and scientific progress.
This civilisation falls apart when the ants realise their industries are irrevocably damaging the Earth’s ecosystem and the dinosaurs refuse to curtail them.
The ants defeat the dinosaurs with extreme guerilla warfare, but discover that the two dinosaur nations were in a state of ultimate [deterrence](https://en.wikipedia.org/wiki/Deterrence_theory) with the use of antimatter bombs under “anti-timers”.
Unable to stop the countdown, the [Cretaceous-Paleogene extinction event](https://en.wikipedia.org/wiki/Cretaceous-Paleogene_extinction_event) commences.

{{ short(title="Sun of China", alt="中国太阳", year=2002) }}

A man from rural china migrates to the city in search of a better life.
He ends up on the cleaning crew of a space [megastructure](https://en.wikipedia.org/wiki/megastructure), a mirror used to redirect sunlight upon the Earth’s surface to crudely control the weather and improve the climate of China’s agricultural regions.
On its decommissioning, he and his crew repurpose the “China Sun” as a [solar sail](https://en.wikipedia.org/wiki/solar_sail) ship to leave the solar system.

{{ short(title="The Wages of Humanity", alt="赡养人类", year=2005, uni="Fourth Earth Universe", num= 2, rest=[1,"#taking-care-of-gods"]) }}

In the foreground, a hitman dutifully fulfills his contracts.

In the background, Earth is the Fourth of six such planets seeded with their own genetic material by an advanced galactic civilisation.
Capitalism and the inviolable right of private property lead to a widening of the social divide on the First until one man owns everything and exiles the two billion other inhabitants of the planet.
These technologically superior refugees come to Earth and lay claim to all but Australia, providing for everyone the subsistence granted the lowest member of current Earth society.
The world’s plutocrats attempt to redistribute their wealth among the poverty-stricken in order to raise everyone’s living standard in the reserve, but they are too late.

{{ short(title="Curse 5.0", alt="太原之恋", year=2010) }}

A humorous account of the computer virus which led to the end of the human race.
It includes a hilarious depiction of the author’s own unfortunate future.

{{ short(title="The Micro-Age", alt="微纪元", year=2001) }}

The last human alive, who was sent out to find a new home for humanity under the threat of the Earth’s destruction by an imminent massive solar “flash”, but failed, returns to the solar system.
He finds humanity has survived by miniaturising themselves to 10 μm tall (short?) people.
They enjoy such abundance of resources that they live in a carefree golden age.
The macro-human realises their only threat is macro-humanity, so destroys the human embryos he carries in his ship’s seed bank.

{{ short(title="Devourer", alt="吞食者", year=2002, uni="Dinosaur Universe", num=2, rest=[1,"#of-ants-and-dinosaurs", 3, "../hold-up-the-sky#cloud-of-poems"]) }}

Terrible lizards (the same *deinos sauros* who evolved on Earth, the survivors of the Ant-Dinosaur War) come to the solar system in a ginormous tire-shaped ship to straddle the Earth and consume its resources.
Humanity attempts to defend itself by ramming the Moon into the Tire, but the Tire narrowly outmaneuvers the Moon, sustaining structural damage but surviving.
The Earth is left devastated and a human population is sustained on the Tire as a luxury food source.

The lizard envoy and leader of the human resistance discuss the philosophy of [cosmic sociology](the-dark-forest.html) through the years of these events, wondering how an altruistic civilisation could ever emerge in the cut-throat galaxy.
The final members of the human resistance ultimately sacrifice themselves to be food for a small ant colony left on the barren Earth by the lizard envoy, ensuring live continues on Earth.

{{ short(title="Taking Care of Gods", alt="赡养上帝", year=2005, uni="Fourth Earth Universe", num=1, rest=[2, "#the-wages-of-humanity"]) }}

The creator of the Earths, the Gods, are revealed to be a senescent people who have forgotten how to maintain their deteriorated space ships.
They spend some time on Earth, adopted by families as a grandparent member of the household, to enjoy humanity’s vitality.
Humans become spiteful of the drain on society of these pensioners, and the Gods ultimately leave as it pains them for their great history to end with the pity of primitive humans.
Despite the abuse of some people, the Gods are thankful for their time on the Fourth Earth, as they were treated far worse by the other Earths.

This tale reflects on our duty to our parents and grandparents.

{{ short(title="With Her Eyes", alt="带上她的眼睛", year=1999, uni="Earth’s Core Universe", num=1, rest=[2,"#the-longest-fall"]) }}

A man goes on holiday. 
In this society, people are heavily worked and luxuries are expensive.
To receive travel subsidies, people volunteer to share their experiences abroad with space-workers via “remote-sensing glasses”.
The man is astounded by how his virtual companion is moved by the simple sights of the outdoors.
He learns she is aboard an earth-boring ship that has become stranded in the Earth’s core.

{{ short(title="The Longest Fall", alt="地球大炮", year=2003, uni="Earth’s Core Universe", num=2, rest=[1,"#with-her-eyes"]) }}

With Antarctica left as the last untouched source of natural resources, a tunnel is bored through the Earth from China to the South Pole to allow quick, zero-energy[^tunnel] zero-pollution [transit](https://en.wikipedia.org/wiki/Gravity_train).
The reaping of Antarctica quickly depletes its fruits and worsens the ecosystem and climate of the Earth, leading to the re-enactment of the [Antarctic Treaty System](https://en.wikipedia.org/wiki/Antarctic_Treaty_System).
Along with several disasters resulting in many deaths, this leaves the “Antarctic Doorstep” as an economic disaster for China and the recipient of everyone’s dissatisfaction.

Further in the future, it is repurposed as the “Earth Cannon”: a humongous [Verne gun](https://en.wikipedia.org/wiki/Space_gun) using gravity in the first section and retro-engineered as a planetary [railgun](https://en.wikipedia.org/wiki/railgun) in the second to accelerate objects into space.
Moving heavy industry and energy generation into orbit saves the planet from ecological disaster.

[^tunnel]: Jumping into the vacuum tunnel, a person would perform a single [harmonic oscillation](https://en.wikipedia.org/wiki/Harmonic_oscillator) under the Earth’s gravity, ending their journey stationary at the same radius from the Earth’s centre of mass on the other side.
