---
titles:
- Season of Storms
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2013
  - 2018
  artists:
  - Andrzej Sapkowski
  - David French
collections:
- name: The Witcher
  number: 1.5
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Season_of_Storms
---
