---
titles:
- The Three-Body Problem
- 三体
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  - fiction
  - speculative fiction
  - hard sci-fi
  places:
  - China
  years:
  - 2014
  - 2008
  - 2006
  artists:
  - Liu Cixin
  - Ken Liu
collections:
- name: Remembrance of Earth’s Past
  number: 1
- name: Three-Body
  number: 1
dates:
  published: 2020-10-22
  updated: 2021-02-20
  experienced:
  - started: 2020-10-13
    finished: 2020-10-21
description: An explanation for the difficulty of modern particle physics
links:
- https://en.wikipedia.org/wiki/The_Three-Body_Problem_(novel)
---

This is my first reading of any novel originally written in Chinese. 
Liu Cixin’s 三体 (2008) was translated to English as *The Three-Body Problem* by Ken Liu and released in 2014.

The story begins against the red backdrop of the [Cultural Revolution](https://en.wikipedia.org/wiki/Cultural_Revolution).
This makes for tragic reading, but I very much appreciate the insight[^firsthand] into a time of history in China that I know little about.
The translator includes footnotes to elaborate on cultural references and contexts that I would have missed otherwise.
Part of the story concerns characters losing faith in humanity, either to the belief that only an external power could help us (Redemptionists), or to unredeemable, capital condemnation (Adventists).
Following Ye Wenjie’s painful life in these times makes her and others’ arrival at such extreme convictions all too believable.

In the approximate modern day, we follow researcher Wang Miao as he becomes embroiled in a secret international war effort to counter a global conspiracy group, the Earth-Trisolaris Organisation (ETO).
Science is turned on its head as he witnesses the impossible, and the dark intrigue thickens as murders and suicides occur.
We are kept afloat by comedic encounters with the initially aggravating but big-hearted police detective, Da Shi, and Wang’s exploration of the mysterious computer game, *Three Body*.
In this virtual reality, the Trisolaran people struggle to survive in a harsh alien world.
Wang encounters other players who have assumed the avatars of famous historical figures, awe-inspiring and terrible sights such as a computer formed of soldiers acting as transistors, and seemingly chaotic phenomena with satisfying explanations.

Everything comes together with the revelation that Alpha Centauri harbours intelligent life, the civilisation depicted in *Three Body*.
Furthermore, the ETO is assisting the Trisolarans in subtly attacking the Earth with some fanciful advanced technologies in preparation for an invasion fleet making its centuries-long voyage between the two star systems.
The Trisolaran’s war on reason, to halt technological and societal progress on Earth, is an amusing explanation for the wall we seem to have hit in fundamental particle physics, at least compared to the discoveries of the last century.
It is also sobering in the current post-truth political climate, when fact is dictated by marketing rather than rationalism, and frighteningly familiar after reading of China in the 1960s and 70s. 

[^firsthand]: The author reveals in the postscript that he experienced this atrocious period of history firsthand as child.
