---
titles:
- Insignificant Little Vermin
taxonomies:
  media:
  - game
  formats:
  - interactive fiction
  genres:
  - fantasy
  - sword and sorcery
  - high fantasy
  - text-based
  - rpg
  - procedural
  - adventure
  - fiction
  - speculative fiction
  years:
  - 2017
dates:
  published: 2021-07-08
  updated: 2021-07-08
  experienced:
  - started: 2021-07-08
    finished: 2021-07-08
description: Fun prototype
links:
- https://egamebook.com/vermin/v/latest/
- https://github.com/filiph/egamebook
---

