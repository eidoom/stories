---
titles:
- The Machinist
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - psychological thriller
  - thriller
  - psychological
  - fiction
  years:
  - 2004
  artists:
  - Brad Anderson
  - Scott Kosar
  - Christian Bale
dates:
  published: 2020-11-08
  updated: 2020-11-08
  experienced:
  - started: 2020-11-08
    finished: 2020-11-08
description: A little bit of guilt goes a long way
---

Trevor Reznik is a delusional insomniac.
We learn this is due to an event a year prior to the current time, in which he hits a pedestrian child through negligent driving.
He flew the scene as an uncaught hit-and-run criminal.
His guilt consumes him and transforms him into the man we meet: paranoid and emaciated.
He has forgotten what he has done, but is tormented by hallucinations which point towards the truth.

The colour palette of the film is so washed out, it’s halfway to being a black-and-white.
This is an expression of Reznik’s view of the world.

Recurring motifs appear throughout the film: an innocuously repeated phrase, an object that catches Reznik’s attention, the framing of a location.
He notices clocks stuck at 1:30, the seconds blinking between 01 and 02.
The identity of the mother of the run-over child, his own mother, and a café barista become mixed up.
He draws a game of hangman for himself on sticky notes but on finding them cannot remember that he made them, stoking his paranoia as his subconscious self-destructively attacks him.
Perhaps the hit-and-run occurred on the return from a fishing trip; references to fishing are littered through the narrative, including the fish themselves which gruesomely leak blood from the freezer when Reznik’s utilities are shut off due to late payment.

Reznik often looks shocked when he sees refections of his face in the mirror.
His transformative weight loss is an attempt to distance himself from the man who ran over the child.
The grotesque figure of Ivan, always driving a red sports car, is revealed as a delusion: he’s Reznik’s view of himself.
