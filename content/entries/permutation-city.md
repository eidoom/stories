---
titles:
- Permutation City
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  - hard sci-fi
  years:
  - 1994
  artists:
  - Greg Egan
collections:
- name: Subjective Cosmology
  number: 2
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - started: 2020-02-18
    finished: 2020-03-04
links:
- https://en.wikipedia.org/wiki/Permutation_City
---
