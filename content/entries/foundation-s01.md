---
titles:
- Foundation S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - science fiction
  years:
  - 2021
collections:
- name: Foundation (TV)
  number: 1
dates:
  published: 2021-10-11
  updated: 2021-10-11
  experienced:
  - started: 2021-10-10
    finished: 2021-11-19
links:
- https://en.wikipedia.org/wiki/Foundation_(TV_series)
---

