---
titles:
- The Farthest Shore
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1972
  artists:
  - Ursula K. Le Guin
collections:
- name: The Books of Earthsea
  number: 3
- name: Earthsea
  number: 5
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-02-01
    finished: 2022-02-17
links:
- https://en.wikipedia.org/wiki/The_Farthest_Shore
---

