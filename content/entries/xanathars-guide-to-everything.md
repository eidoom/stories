---
titles:
- Xanathar's Guide to Everything
taxonomies:
  media:
  - book
  formats:
  - rulebook
  - sourcebook
  genres:
  - supplement
  years:
  - 2017
  artists:
  - Wizards of the Coast
collections:
- name: Dungeons & Dragons
  number: 5.22
- name: Dungeons & Dragons 5th Edition
  number: 12
dates:
  published: 2022-11-13
  updated: 2022-11-13
  experienced:
  - started: 2022-11-13
    finished: 2022-11-13
links:
- https://en.wikipedia.org/wiki/Xanathar%27s_Guide_to_Everything
---
