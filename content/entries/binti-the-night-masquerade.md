---
titles:
- 'Binti: The Night Masquerade'
taxonomies:
  media:
  - book
  formats:
  - novella
  years:
  - 2018
  artists:
  - Nnedi Okorafor
dates:
  published: 2022-12-10
  updated: 2022-12-10
links:
- https://en.wikipedia.org/wiki/Binti:_The_Night_Masquerade
---