---
titles:
- American Gods S03
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - drama
  - fantasy
  - speculative fiction
  - comedy
  - fiction
  places:
  - United States
  - America
  years:
  - 2021
collections:
- name: American Gods
  number: 1.3
- name: American Gods (TV)
  number: 3
dates:
  published: 2021-01-10
  updated: 2022-08-28
  experienced:
  - started: 2021-02-01
    finished: 2021-03-22
description: Specifically, the New versus the Old
links:
- https://en.wikipedia.org/wiki/American_Gods_(season_3)
---
