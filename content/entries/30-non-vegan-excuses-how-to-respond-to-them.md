---
titles:
- 30 Non-Vegan Excuses & How to Respond to Them
taxonomies:
  media:
  - book
  artists:
  - Ed Winters
  years:
  - 2018
  genres:
  - veganism
  - factual
  - nonfiction
  formats:
  - handbook
dates:
  published: 2023-01-18
  updated: 2023-01-19
  experienced:
  - started: 2023-01-18
    finished: 2021-01-19
links:
- https://earthlinged.org/ebook
---
