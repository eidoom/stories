---
titles:
- Stranger Things S02
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - horror
  - drama
  - science fiction
  - speculative fiction
  years:
  - 2017
collections:
- name: Stranger Things
  number: 2
dates:
  published: 2022-07-19
  updated: 2022-08-28
  experienced:
  - started: 2022-07-24
    finished: 2022-07-29
links:
- https://en.wikipedia.org/wiki/Stranger_Things_(season_2)
---
