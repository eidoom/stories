---
titles:
- The Legend of Vox Machina S01
taxonomies:
  media:
  - video
  formats:
  - television
  years:
  - 2022
  genres:
  - fantasy
  - adventure
  - comedy drama
  - fantasy
  - adult animated
  provenances:
  - United States
  places:
  - Exandria
  - Tal'Dorei
  artists:
  - Ashley Johnson
  - Brandon Auman
  - Chris Prynoski
  - Critical Role Productions
  - Laura Bailey
  - Liam O'Brien
  - Marisha Ray
  - Matthew Mercer
  - Neal Acree
  - Sam Riegel
  - Taliesin Jaffe
  - Travis Willingham
  languages:
  - English
collections:
- name: The Legend of Vox Machina
  number: 1
- name: Critical Role
  number: 1
- name: Dungeons & Dragons
  number: 5.91
dates:
  published: 2022-04-19
  updated: 2023-02-14
  experienced:
  - started: 2022-02-21
    finished: 2022-02-26
links:
- https://en.wikipedia.org/wiki/The_Legend_of_Vox_Machina#Season_1_(2022)
---

