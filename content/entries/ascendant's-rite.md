---
titles:
- Ascendant’s Rite
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  years:
  - 2015
  artists:
  - David Hair
collections:
- name: Moontide Quartet
  number: 4
- name: Mage’s Blood
  number: 4
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
