---
titles:
- The Lord of the Rings
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2003
collections:
- name: Middle-Earth
  number: 31.2
dates:
  published: 2022-05-05
  updated: 2022-05-05
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Lord_of_the_Rings_(film_series)
---

