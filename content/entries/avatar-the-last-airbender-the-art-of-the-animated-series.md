---
titles:
- 'Avatar: The Last Airbender – The Art of the Animated Series'
taxonomies:
  media:
  - book
  formats:
  - artbook
  - sourcebook
  genres:
  - concept art
  years:
  - 2010
collections:
- name: 'Avatar: The Last Airbender'
  number: 0
dates:
  published: 2023-04-15
  updated: 2023-04-16
  experienced:
  - started: 2023-04-15
    finished: 2023-04-16
---
