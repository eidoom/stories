---
titles:
- The Book of Phoenix
taxonomies:
  media:
  - book
  formats:
  - novel
  artists:
  - Nnedi Okorafor
  years:
  - 2015
  genres:
  - prequel
collections:
- name: Who Fears Death
  number: 0
dates:
  published: 2022-12-09
  updated: 2022-12-09
  experienced:
  - started: 2022-12-09
    finished: 2022-12-10
links:
- https://en.wikipedia.org/wiki/The_Book_of_Phoenix
---
