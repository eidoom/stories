---
titles:
- 'Star Wars: Episode VI – Return of the Jedi'
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 1983
collections:
- name: Skywalker saga
  number: 6
- name: Star Wars original trilogy
  number: 3
- name: Star Wars
  number: 6
dates:
  published: 2022-05-21
  updated: 2022-05-21
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Return_of_the_Jedi
---

