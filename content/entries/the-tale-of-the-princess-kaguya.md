---
titles:
- The Tale of the Princess Kaguya
- かぐや姫の物語
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - historical
  - anime
  - animated
  - fantasy
  - japanese
  years:
  - 2013
  artists:
  - Studio Ghibli
dates:
  published: 2021-11-09
  updated: 2021-11-09
  experienced:
  - started: 2021-11-09
    finished: 2021-11-09
description: A work of art
links:
- https://en.wikipedia.org/wiki/The_Tale_of_the_Princess_Kaguya_(film)
---

