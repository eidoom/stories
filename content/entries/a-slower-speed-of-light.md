---
titles:
- A Slower Speed of Light
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - libre
  - open source
  - free
  - special relativity
  - educational
  - science
  - physics
  - relativity
  years:
  - 2012
dates:
  published: 2021-02-27
  updated: 2021-02-27
  experienced:
  - started: 2021-02-27
    finished: 2021-02-27
description: Special relativity illuminated
completed: true
links:
- https://en.wikipedia.org/wiki/A_Slower_Speed_of_Light
- http://gamelab.mit.edu/games/a-slower-speed-of-light/
---

This is a free game with an educationally illustrative focus.
It is short, at most ten minutes (is that world time or player time?).
It has a touching little story and accompanying music for flavour.
By slowing the speed of light, the player is able to observe special relativistic effects of [Doppler shifting](https://en.wikipedia.org/wiki/Relativistic_Doppler_effect) in the electromagnetic spectrum, [relativistic aberration](https://en.wikipedia.org/wiki/relativistic_aberration), [length contraction](https://en.wikipedia.org/wiki/length_contraction), and [time dilation](https://en.wikipedia.org/wiki/time_dilation).
It’s great!
