---
titles:
- Stranger Things S04
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - horror
  - drama
  - science fiction
  - speculative fiction
  years:
  - 2022
collections:
- name: Stranger Things
  number: 4
dates:
  published: 2022-07-19
  updated: 2022-08-28
  experienced:
  - started: 2022-08-28
    finished: 2022-09-01
active: false
links:
- https://en.wikipedia.org/wiki/Stranger_Things_(season_4)
---
