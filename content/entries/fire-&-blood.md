---
titles:
- Fire & Blood
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  years:
  - 2018
  artists:
  - George R. R. Martin
collections:
- name: A Targaryen History
  number: 1
- name: Game of Thrones
  number: 0
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - started: 2020-06-14
    finished: 2020-07-15
links:
- https://en.wikipedia.org/wiki/Fire_%26_Blood_(novel)
---
