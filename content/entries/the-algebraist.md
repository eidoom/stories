---
titles:
- The Algebraist
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  years:
  - 2004
  artists:
  - Iain M. Banks
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - started: 2019-12-31
    finished: 2020-01-23
links:
- https://en.wikipedia.org/wiki/The_Algebraist
---
