---
titles:
- The Long Way to a Small, Angry Planet
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2014
  artists:
  - Becky Chambers
collections:
- name: Wayfarers
  number: 1
- name: Galactic Commons
  number: 1
dates:
  published: 2022-04-30
  updated: 2022-05-11
  experienced:
  - started: 2022-05-05
    finished: 2022-05-11
links:
- https://en.wikipedia.org/wiki/The_Long_Way_to_a_Small,_Angry_Planet
---

A drama of the crew of the *Wayfarer*, the newest member Rosemary fitting in, and their biggest job yet.
