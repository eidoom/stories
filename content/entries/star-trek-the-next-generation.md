---
titles:
- 'Star Trek: The Next Generation'
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - speculative fiction
  - science fiction
  - space opera
  years:
  - 1987
collections:
- name: Star Trek
  number: 8
dates:
  published: 2021-10-10
  updated: 2021-10-10
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Star_Trek:_The_Next_Generation
---

There is *a lot* to watch: 178 episodes, each at 45 minutes!
I saw many of them as a kid on the TV, leaving only a nostalgic sense of the setting and characters.
Now I’ll pick and choose some as recommended by friends, the [Let’s Watch Star Trek](https://www.letswatchstartrek.com/tng-episode-guide/) blog, or randomly.

{{ episode(season=1,episodes=[1,2],title="Encounter at Farpoint",links=["https://en.wikipedia.org/wiki/Encounter_at_Farpoint"]) }}

{{ episode(season=1,episode=25,title="Conspiracy",seen="2021-10-10",links=["https://en.wikipedia.org/wiki/Conspiracy_(Star_Trek:_The_Next_Generation)"]) }}

{{ episode(season=1,episode=24,title="We'll Always Have Paris",seen="2023-04-03",links=["https://en.wikipedia.org/wiki/We%27ll_Always_Have_Paris_(Star_Trek:_The_Next_Generation)"]) }}

{{ episode(season=2,episode=16,title="Q Who",seen="2021-10-08") }}

{{ episode(season=3,episode=4,title="Who Watches The Watchers",seen="2021-10-07") }}
