---
titles:
- Salvation Lost
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  years:
  - 2019
  artists:
  - Peter F. Hamilton
collections:
- name: Salvation Sequence
  number: 2
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - started: 2019-11-14
    finished: 2019-11-25
---
