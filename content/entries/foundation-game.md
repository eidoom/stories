---
titles:
- Foundation
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - city builder
  - medieval
  - simulation
  - indie
  - early access
  - strategy
  years:
  - 2019
  artists:
  - Polymorph Games
dates:
  published: 2021-05-17
  updated: 2021-06-08
  experienced:
  - started: 2021-05-17
completed: true
description: Pretty Banished clone
links:
- https://www.polymorph.games/
---

### Early access

2021-05-17 -- 2021-05-18

The game is graphically gorgeous, although the UI needs some love.
It was fun to play although I soon exhausted what it currently has to offer in economy mechanics.
Promising.
