---
titles:
- Tehanu
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1990
  artists:
  - Ursula K. Le Guin
collections:
- name: The Books of Earthsea
  number: 4
- name: Earthsea
  number: 6
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-03-12
    finished: 2022-04-02
links:
- https://en.wikipedia.org/wiki/Tehanu
---

