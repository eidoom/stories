---
titles:
- Abaddon’s Gate
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2013
  artists:
  - James S. A. Corey
collections:
- name: The Expanse
  number: 3
- name: The Expanse (books)
  number: 3
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Abaddon%27s_Gate
---
