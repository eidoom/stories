---
titles:
- Princess Mononoke
- もののけ姫
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 1997
  artists:
  - Studio Ghibli
dates:
  published: 2022-04-30
  updated: 2022-04-30
  experienced:
  - started: 2022-04-28
    finished: 2022-04-28
links:
- https://en.wikipedia.org/wiki/Princess_Mononoke
---

