---
titles:
- True Detective S03
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - crime
  - drama
  - fiction
  - anthology
  - mystery
  - detective
  - neo-noir
  - nonchronological
  - nonlinear
  years:
  - 2019
collections:
- name: True Detective
  number: 3
dates:
  published: 2021-04-22
  updated: 2022-08-28
  experienced:
  - started: 2021-10-16
    finished: 2021-10-19
completed: true
links:
- https://en.wikipedia.org/wiki/True_Detective_(season_3)
---

The third season returns to the tried-and-tested recipe of the first, but subverts some of the assumptions that come with it.

Our police partners, Wayne Hays and Roland West, have a close but tumultuous bond, Wayne often pushing the limits of their relationship.
They manage to come through in the end with an intact friendship, though.

The reveal of Will Purcell’s murder as accidental and Julie Purcell’s kidnapper as Isabel Hoyt, a mother driven to madness by the grief of losing her daughter in a car crash, is a sad and moving counterpoint to Elisa Montogomery’s season one-esque theories.

Harris James is the closest thing we have to a traditional antagonist, the psychopathic murderer, but what is his motive? Money? Loyalty to the Hoyts? Schadenfreude? He croaks before we can find out.

The racism that Native American Brett Woodard suffers is tragic.
His last stand is the season’s action high.

Amelia Reardon is keenly intelligent and empathetic.
She’s a better investigator than our cop duo.
As “his woman”, however, Wayne jealously disapproves of and disregards her sleuthing.
Wayne’s often awful treatment of Amelia, withholding the trust she deserves and taking out his frustration with his work on her, is the unforgiveable nadir of his character.
I hope he was a better husband after quitting the force in 1990.
He does at least come to understand and regret this in his last days, after she is gone.

Lucy Purcell is completely unlikeable.
It’s no surprise that Amelia’s suspicions were correct in that she was involved in the kidnapping.
Tom Purcell does an excellent job of being incredibly annoying.
The friendship that slowly precipitates between him and Roland is touching.

Behind Roland’s laddish exterior, his deep compassion is often revealed through moments like his caring of Tom.
He keeps Wayne under his wing from day one too.
His best friend is a dog.

Interwoven through the whole plot, Wayne’s struggle to live with dementia and recall his life is bittersweet.
An elderly and retired man in 2015, he is still a very capable detective, perhaps more so than in his youth because of his acceptance of Amelia’s work.
The conveyance of his state of mind, in particular when his mind transitions into forgetfulness or haunting, through sound and the expression on his face, is chilling and sympathetically emotional.
