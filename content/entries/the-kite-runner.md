---
titles:
- The Kite Runner
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2003
  artists:
  - Khaled Hosseini
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
