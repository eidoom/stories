---
titles:
- Inversions
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1998
  artists:
  - Iain M. Banks
collections:
- name: Culture
  number: 6
- name: The Culture
  number: 10
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Inversions_(novel)
---
