---
titles:
- Persepolis Rising
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2017
  artists:
  - James S. A. Corey
collections:
- name: The Expanse
  number: 7
- name: The Expanse (books)
  number: 7
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Persepolis_Rising
---
