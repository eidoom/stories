---
titles:
- 'Gwent: The Witcher Card Game'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - card game
  - collectible card game
  years:
  - 2018
  artists:
  - CD Projekt Red
collections:
- name: The Witcher
  number: -1
- name: The Witcher (games)
  number: 4
dates:
  published: 2021-06-26
  updated: 2021-06-26
  experienced:
  - started: 2021-06-25
description: “Wouldn’t mind a few rounds of cards.”
links:
- https://en.wikipedia.org/wiki/Gwent%3A_The_Witcher_Card_Game
---

I’ve never really played card deck games before but I now understand the appeal.
A bit of fun with friends in a familiar setting.
