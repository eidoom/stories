---
titles:
- Wingspan
taxonomies:
  media:
  - game
  formats:
  - board game
  - card game
  genres:
  - strategy
  - engine building
  - animals
  - birds
  years:
  - 2019
  artists:
  - Elizabeth Hargrave
dates:
  published: 2021-09-19
  updated: 2022-05-06
  experienced:
  - started: 2021-09-18
    finished: 2021-09-18
players:
  min: 1
  max: 5
links:
- https://en.wikipedia.org/wiki/Wingspan_(board_game)
- https://stonemaiergames.com/games/wingspan/
- https://boardgamegeek.com/boardgame/266192/wingspan
---

I played with three players.
