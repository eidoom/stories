---
titles:
- Advent of Code
taxonomies:
  media:
  - game
  - web
  formats:
  - puzzle
  - website
  years:
  - 2015
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2019-12-23
---

