---
titles:
- 'Recipes from the World of Tolkien: Inspired By the Legends'
taxonomies:
  media:
  - book
  years:
  - 2020
  formats:
  - cookbook
  genres:
  - food
  - recipes
  - cooking
  - nonfiction
  artists:
  - Robert Tuesley Anderson
collections:
- name: Middle-Earth
  number: 30.5
dates:
  published: 2022-12-24
  updated: 2022-12-24
  experienced:
  - started: 2022-12-20
    finished: 2022-12-20
---
