---
titles:
- 'Star Wars: Episode VII – The Force Awakens'
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - epic
  - science fiction
  - fantasy
  - speculative fiction
  - space opera
  - space
  - fiction
  years:
  - 2015
collections:
- name: Skywalker saga
  number: 7
- name: Star Wars sequel trilogy
  number: 1
- name: Star Wars
  number: 7
dates:
  published: 2020-11-29
  updated: 2020-11-29
  experienced:
  - started: 2020-11-28
    finished: 2020-11-28
links:
- https://en.wikipedia.org/wiki/Star_Wars:_The_Force_Awakens
---

