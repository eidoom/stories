---
titles:
- Bullet Train
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - action
  - comedy
  - thriller
  years:
  - 2022
  artists:
  - David Leitch
dates:
  published: 2022-10-15
  updated: 2022-10-15
  experienced:
  - started: 2022-10-15
    finished: 2022-10-15
links:
- https://en.wikipedia.org/wiki/Bullet_Train_(film)
---