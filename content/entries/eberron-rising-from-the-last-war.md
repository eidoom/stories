---
titles:
- 'Eberron: Rising from the Last War'
taxonomies:
  media:
  - book
  years:
  - 2019
  formats:
  - sourcebook
  artists:
  - Wizards of the Coast
  genres:
  - campaign
collections:
- name: Dungeons & Dragons
  number: 5.34
- name: Dungeons & Dragons 5th Edition
  number: 24
dates:
  published: 2022-11-13
  updated: 2022-11-13
  experienced:
  - started: 2022-11-13
    finished: 2022-11-13
links:
- https://en.m.wikipedia.org/wiki/Eberron:_Rising_from_the_Last_War
---
