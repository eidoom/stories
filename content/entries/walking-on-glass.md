---
titles:
- Walking on Glass
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  - speculative fiction
  - drama
  - comedy
  - literary
  places:
  - London
  - United Kingdom
  - England
  - Britain
  years:
  - 1985
  artists:
  - Iain Banks
dates:
  published: 2021-01-05
  updated: 2021-08-08
  experienced:
  - started: 2021-01-03
    finished: 2021-01-17
description: Madness or perception?
links:
- https://en.wikipedia.org/wiki/Walking_on_Glass
---

This story follows three characters: Graham, Steven, and Quiss.
The book is arranged in five regular parts, each with three chapters: one for each protagonist, in the order I named them above.
There is an epilogual part six by Graham.
Graham’s chapters are named for the street on London on which they mainly take place.
Steven’s are named after the person with whom he has a significant encounter in “Honourific Surname” form.
Quiss’s are called the name of a fictional game which he plays in that chapter.

Graham Park has moved from the country to study art in London.
He’s young at twenty years of age, innocent, and filled with bright optimism.
He’s in love with the mysterious and beautiful Sara ffitch.
Graham’s best friend is the flamboyant, melodramatic Richard Slater.
He’s gay and proud, and vocal with his socialist political views.

We see from the offset that Graham’s infatuation is unhealthy.
Sara, who has just left her husband, is in a relationship with the biker-dude Stock—which Graham knows.
She seems to be playing him along in full knowledge of his desire with no intention of being more than a friend, perhaps using him as an emotional crutch or unfairly taking advantage of his attention.
Unfortunately, Graham is blind to this in his naivety and feeling of love-at-first-sight.

Steven Grout is an unusual man, to say the least.
He has crackpot beliefs about how he is a Warrior from a galactic war, but he is being punished and tested by being forced to live a human life.
He has found plenty of proof to support this; for example, while he can’t quite put his finger on it, the estate agency name Hotblack Desiato seems to ring a bell (it’s actually a character from *The Hitch-Hiker’s Guide to the Galaxy* inspired by the same company, according to Richard).
He thinks that the Tormentors are manipulating his environment to make his life more difficult, like how they use a Microwave Gun to make him uncomfortable whenever he “talk[s] to somebody official”.
He’s looking for the Key, a Way Out.
His social ineptitude and inner monologues are hilarious.

In the far future, Quiss is imprisoned in the Castle of Bequest along with Ajayi.
They are serving penance in elderly bodies for crimes in an ultimate war, each coming from opposing sides.
To leave, they must guess the answer to a riddle, being allowed a guess each time they finish an improbable game.
Among other things, a talking red crow incessantly pesters them with insults.
This fantasy strikes a contrast to the urbanity of London and makes us reconsider Steven’s obvious insanity.
The description of the Castle, its attendants, and the games are absurd and amusing.

At the end of Graham’s story, Sara reveals to him that she and Stock were using him to fool a spy that her husband had sent after her, setting Graham up as the perpetrator of the affair.
Graham, heartbroken, leaves and reflects on his disgust with society.
The truth, however, unbeknownst to Graham, is that Richard and Sara are siblings who are engaged in a guilty sadistic incestuous relationship—Stock is Richard in disguise—and they are trying to hide this from their parents and the press, their father being a Conservative MP.
This reveals Richard as leftwing in rebellion only, and certainly not a follower of his advertised philosophy of “Ethical Hedonism”.

Richard misses this conversation because his bike fails him—Steven is known to sabotage vehicles in the area.
Richard ends up causing a traffic accident, with Steven the pedestrian casualty (his arational fear of cars was sensible after all, just without an understanding of the method).
A snippet of a police interview and a doctor’s recollection of the news reveals that Richard was later caught, creating a scandal for the family after all.

A year later, Steven is living in a Sheltered Unit indefinitely, having survived but with brain damage.
Having lost memory of his conspiracy theories, trading paranoid madness for a memory disability, he finds a happiness he never had before.
It’s a pleasant ending for him.
This is in juxtaposition to Graham, who in gaining knowledge loses his innocence and is led to unhappiness.

Steven notices an old couple “playing games over an old coffee table”.
It is noted that the hospital was opened after the First World War, and they are playing in the Unit’s library.
This is particularly reminiscent of the book-filled games room of the Castle with the unique red-jewelled games table.
Steven also finds on a littered matchbox the riddle of the Castle (finding also its solution, unlike the pair).
Is this hospital the reality of Quiss and Ajayi? 
The traumatic accidents they had, which they call crimes, could easily have been in the real world during the Great War.

Or is it Them?
Steven has a friend, Mike Williams, at the Unit, likely another patient, who gives him “an old big rusty key and a plastic sign which said ‘Way Out’”, echoing the things Steven previously searched for.
Mr Williams also gives Steven chess pieces, Scrabble tiles, and dominoes—all games which a crazy variation of was played at the Castle—that he’s stolen from the couple.
The crows around the Unit speak too, of a sort: they cry his name: “Get-out!”

Quiss uncovers that the Castle hides chambers containing “reverse goldfish-bowls”: holes which you stick your head into to commune with a special animal that can connect you with minds from the distant past.
This melding allows the viewer to experience, and maybe even direct, other lives: “Every mind contains its own universe. We can be sure of nothing.”
The name of the first viewer is hinted to be Grout.

After losing hope in escape, Quiss has the revelation that he can give himself up to a goldfish-bowl, or commit suicide.
Ajayi stops him from jumping from the parapet and he attacks her in retribution.
This chilling violence is a reminder of Richard’s sadistic sexual treatment of Sara.
Ajayi’s acceptance of it is also a parallel to Sara’s behaviour.
For example, she fully expected Graham to beat her when she told him a version of the truth, even seeming to goad him with this self-flagellating goal in mind.

The Castle storyline ends with Ajayi beginning to read their own narrative.

The novel is suffused with a feeling of societal dissatisfaction.
Steven very clearly doesn’t fit in.
Graham goes through the meat-grinder when he gets caught up in the machinations of some Tory bigwig’s children.
The Castle, a place designed to wear you down, is presented as the culmination of our future.
We even have Richard’s scathing commentary on Sun and Daily Mail tabloid readers, as well as Steven’s encounter with the xenophobic pub drunk Mr Sharpe.
