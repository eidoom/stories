---
titles:
- The Blue Planet
- 'The Blue Planet: Seas of Life'
taxonomies:
  media:
  - video
  formats:
  - television
  - documentary
  genres:
  - nature
  years:
  - 2001
  artists:
  - David Attenborough
dates:
  published: 2022-11-01
  updated: 2022-11-01
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Blue_Planet
---
