---
titles:
- To Be Taught, if Fortunate
taxonomies:
  media:
  - book
  formats:
  - novella
  genres:
  - science fiction
  years:
  - 2019
  artists:
  - Becky Chambers
dates:
  published: 2022-08-23
  updated: 2022-08-23
  experienced:
  - started: 2022-08-24
    finished: 2022-09-01
active: false
links:
- https://en.wikipedia.org/wiki/To_Be_Taught,_if_Fortunate
---

