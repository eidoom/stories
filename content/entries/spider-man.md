---
titles:
- Spider-Man
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2002
collections:
- name: Spider-Man
  number: 4
- name: Sam Raimi’s Spider-Man
  number: 1
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-04-08
    finished: 2022-04-08
links:
- https://en.wikipedia.org/wiki/Spider-Man_(2002_film)
---

