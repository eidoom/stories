---
titles:
- 'Altered Carbon: Download Blues'
taxonomies:
  media:
  - book
  formats:
  - comic
  years:
  - 2019
collections:
- name: Altered Carbon
  number: 4
dates:
  published: 2022-05-05
  updated: 2022-05-05
  experienced:
  - '?'
links:
- https://www.richardkmorgan.com/writing/altered-carbon-download-blues/
- https://comicvine.gamespot.com/altered-carbon-download-blues-1-hc/4000-712363/
---

