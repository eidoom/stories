---
titles:
- Red Seas Under Red Skies
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2007
  artists:
  - Scott Lynch
collections:
- name: Gentleman Bastard
  number: 2
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
