---
titles:
- Life is Strange
taxonomies:
  media:
  - book
  formats:
  - comic
  years:
  - 2018
dates:
  published: 2022-05-05
  updated: 2022-05-05
  experienced:
  - '?'
links:
- https://life-is-strange.fandom.com/wiki/Life_is_Strange_(Comic_Series)
---

Read:
Life Is Strange: Dust, Life Is Strange: Waves, Life Is Strange: Strings.

Unread:
Life Is Strange: Partners in Time: Tracks, Life is Strange: Coming Home, Life is Strange: Settling Dust.
