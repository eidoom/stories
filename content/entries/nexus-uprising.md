---
titles:
- Nexus Uprising
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2017
  artists:
  - Jason M. Hough
  - K. C. Alexander
collections:
- name: 'Mass Effect: Andromeda'
  number: 1
- name: Mass Effect
  number: 3.6
dates:
  published: 2019-12-26
  updated: 2021-07-30
  experienced:
  - started: 2019-12-06
    finished: 2019-12-27
links:
- https://en.wikipedia.org/wiki/Mass_Effect:_Andromeda_(book_series)#Nexus_Uprising
---
