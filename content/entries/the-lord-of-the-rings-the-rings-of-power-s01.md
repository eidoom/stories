---
titles:
- 'The Lord of the Rings: The Rings of Power S01'
taxonomies:
  media:
  - video
  formats:
  - television
  years:
  - 2022
collections:
- name: Middle-Earth
  number: 20.1
- name: 'The Lord of the Rings: The Rings of Power'
  number: 1
dates:
  published: 2022-07-11
  updated: 2022-10-15
  experienced:
  - started: 2022-09-02
    finished: 2022-10-14
links:
- https://en.wikipedia.org/wiki/The_Lord_of_the_Rings:_The_Rings_of_Power
---

Also [after](https://deadline.com/tag/inside-the-ring/) [show](https://www.youtube.com/playlist?list=PLWz2DO39R-NVKEweZX-Ww-FphRm_Lso4l).
