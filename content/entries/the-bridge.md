---
titles:
- The Bridge
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fiction
  - drama
  - dream
  - romance
  - amnesia
  - literary
  places:
  - United Kingdom
  - Britain
  years:
  - 1986
  artists:
  - Iain Banks
  provenances:
  - Scotland
dates:
  published: 2021-01-17
  updated: 2021-08-19
  experienced:
  - started: 2021-01-17
    finished: 2021-02-04
description: The Fifth Bridge? The Forth to Fife?
links:
- https://en.wikipedia.org/wiki/The_Bridge_(novel)
---

This is the story of John Orr, not that it’s his real name.
After a car crash, he falls into a coma in which he lives in a surreal dreamscape.
We jump between his personae: John on the Bridge, the barbarian scouring [a Culturesque universe](../../universe/the-culture), and memories of his life up to the crash.

Like [*Walking on Glass*](walking-on-glass.html) before it, the chapters are cleverly named.
We start with *Coma*, clearly setting the scene, and end, appropriately, on the alliterative and rhyming *Coda*.
Some take their titles from geological [periods](https://en.wikipedia.org/wiki/Geological_period) and [epochs](https://en.wikipedia.org/wiki/Epoch_(geology)): *[Triassic](https://en.wikipedia.org/wiki/Triassic)*, *[Eocene](https://en.wikipedia.org/wiki/Eocene)*, *[Oligocene](https://en.wikipedia.org/wiki/Oligocene)*, *[Miocene](https://en.wikipedia.org/wiki/Miocene)*, *[Pliocene](https://en.wikipedia.org/wiki/Pliocene)*, and *[Quaternary](https://en.wikipedia.org/wiki/Quaternary)*; the others are an indexed *[Metamorphosis](https://en.wikipedia.org/wiki/Metamorphism)*.
The allusion to geology is unsurprising considering the novel’s strong geographical rooting and [Scotland’s rich geology](https://en.wikipedia.org/wiki/Geology_of_Scotland).
The countdown through eras mirrors our approach to the present day in John’s dream trance reliving of his life and the metaphorism the changes within the splinters of his personality.

The Bridge is a construction of seemingly endless span with a city unraveled and spread along it.
It is a reflection of the [Forth Bridge](https://en.wikipedia.org/wiki/Forth_Bridge), a recurring landmark in John’s real life.
Directions are denoted Cityward and Kingdomward: echoes of the City of [Edinburgh](https://en.wikipedia.org/wiki/Edinburgh) and the Kingdom of [Fife](https://en.wikipedia.org/wiki/Fife).
The imagery of the Bridge fractally appears at every level of the dream, for example when John likens the pattern of Abberlaine Arrol’s stockings to the crossed beams of the Bridge[^arrol].
Life on the bridge is totalitarian and dystopian, an indication of socialist John’s political discontent in Tory Britain through the Eighties.

As in the later *Feersum Endjinn*, the barbarian sequences are entertainingly depicted in phonetic Scots.
Here lies John’s base emotions, his suppressed aggression, the feelings he is most shameful of.
He has a knockoff Knife Missile—perhaps he’s a [*Culture*](../../universe/the-culture) reader?

John’s real life is full of internal conflict and contradiction, a struggle for ownership of an identity; it’s no surprise he self-destructively ends up in a coma that he prefers to linger in, amnesiac, forgetting even his own name.
He grows up in working-class Glasgow, but flies the nest for university and to lead an engineering business on the East Coast.
The most important part of his life is Andrea Cramond (Abberlaine’s doppelgänger), with whom he is in an open relationship, a situation he both loves and tortures himself over.

[^arrol]: Civil engineer [William Arrol](https://en.wikipedia.org/wiki/William_Arrol) was one of the Forth Bridge builders.
