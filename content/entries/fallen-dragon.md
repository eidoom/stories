---
titles:
- Fallen Dragon
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2001
  artists:
  - Peter F. Hamilton
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Fallen_Dragon
---
