---
titles:
- The Last of Us S02
taxonomies:
  media:
  - video
  formats:
  - television
  years:
  - unreleased
  artists:
  - 'Bella Ramsey'
  - 'Craig Mazin'
  - 'David Fleming'
  - 'Gustavo Santaolalla'
  - 'Neil Druckmann'
  - 'Pedro Pascal'
  genres:
  - 'post-apocalyptic'
  - 'drama'
dates:
  published: 2023-03-29
  updated: 2023-03-29
#  experienced:
#  - started: 2023-01-19
#    finished: 2023-03-13
#links:
#- https://en.wikipedia.org/wiki/The_Last_of_Us_(TV_series)
---
