---
titles:
- The Hunger Games
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2012
  genres:
  - American
  - dystopian
  - science fiction
  - war
  provenances:
  - United States
collections:
- name: The Hunger Games films
  number: 1
dates:
  published: 2022-12-18
  updated: 2022-12-18
  experienced:
  - started: 2022-12-13
    finished: 2022-12-13
links:
- https://en.wikipedia.org/wiki/The_Hunger_Games_(film)
---
