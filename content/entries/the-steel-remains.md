---
titles:
- The Steel Remains
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2008
  artists:
  - Richard K. Morgan
collections:
- name: A Land Fit for Heroes
  number: 1
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
