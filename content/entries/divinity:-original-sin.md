---
titles:
- 'Divinity: Original Sin'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - rpg
  - fantasy
  - single-player
  - multiplayer
  - role-playing game
  - ficiton
  - speculative fiction
  - comedy
  - role-playing
  artists:
  - Larian Studios
  years:
  - 2014
  places:
  - Rivellon
collections:
- name: 'Divinity: Original Sin'
  number: 1
- name: Divinity
  number: 4
dates:
  published: 2022-05-13
  updated: 2022-11-24
  experienced:
  - started: 2022-05-14
active: true
links:
- https://en.wikipedia.org/wiki/Divinity:_Original_Sin
---

