---
titles:
- The Broken Kingdoms
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2010
  artists:
  - N. K. Jemisin
collections:
- name: The Inheritance Trilogy
  number: 2
- name: Inheritance
  number: 2
dates:
  published: 2020-08-02
  updated: 2021-07-30
  experienced:
  - started: 2020-08-04
    finished: 2020-08-11
links:
- https://en.wikipedia.org/wiki/The_Broken_Kingdoms
---
