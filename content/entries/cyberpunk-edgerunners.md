---
titles:
- 'Cyberpunk: Edgerunners'
- サイバーパンク エッジランナーズ
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - anime
  - cyberpunk
  - dubbed
  provenances:
  - Poland
  - Japan
  years:
  - 2022
  languages:
  - English
collections:
- name: Cyberpunk 2077
  number: 0
dates:
  published: 2022-10-09
  updated: 2022-10-09
  experienced:
  - started: 2022-10-08
    finished: 2022-10-09
links:
- https://en.wikipedia.org/wiki/Cyberpunk:_Edgerunners
---
