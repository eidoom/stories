---
titles:
- Before We Leave
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - city builder
  years:
  - 2020
  artists:
  - Balancing Monkey Games
dates:
  published: 2021-08-13
  updated: 2021-08-13
  experienced:
  - started: 2021-08-13
completed: true
description: Peaceful and pretty civilisation rebuilder
links:
- https://www.balancingmonkeygames.com/before-we-leave
---

