---
titles:
- Mr Robot S04
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - psychological thriller
  - drama
  - psychological
  - thriller
  - technology
  - corporate capitalism
  places:
  - United States
  - America
  years:
  - 2019
collections:
- name: Mr Robot
  number: 4
dates:
  published: 2021-02-24
  updated: 2022-08-28
  experienced:
  - started: 2021-03-06
    finished: 2021-03-10
description: versus Evil Corp
links:
- https://en.wikipedia.org/wiki/List_of_Mr._Robot_episodes#Season_4_(2019)
---

The DA execute Angela, spurning Phillip to aid Elliot in bringing down the Deus Group.
Tyrell and Elliot get lost in the woods trying to stop a DA operative who overheard Tyrell expressing disobedience; Tyrell is shot and stumbles off to die alone.
He’s done truly despicable things but I can’t help feeling sorry for him.
Dom gets an affable handler, Janice.
We glimpse some of Whiterose’s past: she had to hide who she was in China and her true love killed himself because of it.
Magda Alderson passes away.

After Angela’s death, Elliot loses his empathy and morality, doing anything to take down Whiterose.
He plans to take her money.
He seduces Olivia Cortez, an employee of the bank holding Deus’s money, to get access.
She turns out to be someone he could love.
Regardless, since she’s a recovered addict who can only see her son if she stays clean, he drugs her to blackmail her into cooperating.
He didn’t even try talking her into it; in the end that’s the way he convinces her.

Fernando Vera returns and a theatrical play ensues in which he forces Krista Gordon to reveal what she suspects of Elliot past.
The truth is his father sexually abused him.
Elliot struggles with this revelation and Mr Robot takes over for the Deus hack.

Janice orders Dom to murder Darlene but she can’t, resulting in an epic faceoff which Dom wins, though with a knife puncturing her lung.

Through Phillip conniving a Deus meeting, Darlene and Elliot manage to hack the phones of the 100 Deus members, allowing them to complete the hostile bank transfers with MFA access.
They [dox](https://en.wikipedia.org/wiki/doxing) Deus and equally distribute the funds over every Ecoin wallet.

Darlene convinces Dom to run away with her to Budapest (Cisco’s dream).
Leon’s gone freelance, so he escorts them out of the city.
Dom learns from a chance encounter with Irving that the DA are no longer a threat so decides to stay out of duty to her family, even through the FBI has her under investigation and won’t allow her to see them.
Left alone, Darlene panics and resolves to stay for Elliot.
Dom about turns and gets on the flight, missing Darlene in the bathroom.
Dom finally gets some sleep on the plane.
It seems they took what they need from each other: Dom could let go for once and Darlene could stick it out with her brother.

Elliot goes to the Washington Township power plant to destroy the Project.
The DA is one step ahead and capture him, placing him in a mindgame room like they did with Angela.
Whiterose claims everything she has done is born of love for people, to make the world a better place, to provide a fresh start.
Elliot admits he hates the world but also that people love him and that makes it worth fighting for.

What little we see of the Project resembles a particle collider.
Angela believes it will bring back the dead, Whiterose promises it will wipe away all hurt, Phillip defames it as delusional, Elliot thinks it will destroy the world.
I have no idea what Whiterose is planning.

Whiterose activates the Project and shoots herself, leaving Elliot to choose its fate.
He plays an adventure game which deactivates the Project when he chooses to stay with the friend—but is it too late?
There is an explosion.

He wakes in a perfect alternative world.
What?
What?
What?
Is the Project some sci-fi consciousness uploader to a virtual fantasy world?
Some multiverse traverser to skip to your favourite parallel universe?
It can’t be, that’s impossible.
We’re not out of the woods of Elliot’s mind yet.

It transpires that our protagonist is not the real Elliot but the Mastermind, a personality created as part of his [dissociative identity disorder](https://en.wikipedia.org/wiki/dissociative_identity_disorder) to literally change the world to protect him.
We have watched him transition from fighting to stop blowing up buildings as part of the plan, saving those lives, to someone who will remorselessly destroy innocents to achieve his goals.
He has become one of those he set out to stop:

> “a guy trying to play God without permission.”

Now, the Mastermind doesn’t want to let go of the reins.
He has fulfilled his purpose but he’s willing to kill Elliot to stay in control.

Waking in a hospital IRL, the Mastermind talks to Darlene.
He can’t let go for himself, but he loves Darlene and will let Elliot come back for her.

> “This whole time, I thought changing the world was something you did, and after you perform, something you fought for.
> I don’t know if that’s true anymore.
> What if changing the world is just about being here?
> By showing up, no matter how many times we get told we don’t belong, by staying true even when we’re ashamed into being false, by believing in ourselves even when we’re told we’re too different.
> And if we all held onto that, if we refused to budge and fall in line, if we stood our ground for long enough, just maybe the world can’t help but change around us.”

We’re a personality too: the Friend.

> “Come on. 
> This only works if you let go too.”
