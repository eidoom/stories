---
titles:
- Gladiator
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - epic
  - historical
  - drama
  - sword and sandal
  - action
  - fiction
  years:
  - 2000
dates:
  published: 2021-05-22
  updated: 2021-06-08
  experienced:
  - started: 2021-05-20
    finished: 2021-05-20
description: Epic
links:
- https://en.wikipedia.org/wiki/Gladiator_(2000_film)
---

It may not be the most literary but it sure is **EPIC**.
Much enjoyed.
