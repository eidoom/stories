---
titles:
- Catch Me If You Can
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - biography
  - drama
  - crime
  years:
  - 2002
dates:
  published: 2021-05-24
  updated: 2021-06-08
  experienced:
  - started: 2021-05-23
    finished: 2021-05-23
description: Con-boy
links:
- https://en.wikipedia.org/wiki/Catch_Me_If_You_Can
---

Painting the confidence artist so sympathetically and impressively is exactly how they’d want you to portray them...

In any case, I fell for it entirely.
