---
titles:
- My Neighbour Totoro
- となりのトトロ
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - anime
  - japan
  - fantasy
  - animated
  - family friendly
  years:
  - 1988
  - 2006
  artists:
  - Studio Ghibli
dates:
  published: 2021-08-10
  updated: 2021-08-10
  experienced:
  - started: 2021-08-10
    finished: 2021-08-10
description: Delightful, wholesome
---

