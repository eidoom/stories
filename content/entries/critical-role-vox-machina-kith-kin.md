---
titles:
- 'Critical Role: Vox Machina – Kith & Kin'
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  - literature
  years:
  - 2021
  artists:
  - Marieke Nijkamp
  places:
  - Exandria
  provenances:
  - United States
  languages:
  - English
collections:
- name: Critical Role novels
  number: 1
- name: Critical Role
  number: 21
dates:
  published: 2023-02-16
  updated: 2023-04-12
  experienced:
  - started: 2023-03-22
    finished: 2023-03-02
links:
- https://en.wikipedia.org/wiki/Critical_Role:_Vox_Machina_%E2%80%93_Kith_%26_Kin
---
