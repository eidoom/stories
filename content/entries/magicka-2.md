---
titles:
- Magicka 2
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2015
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-07-24
links:
- https://en.wikipedia.org/wiki/Magicka_2
---

