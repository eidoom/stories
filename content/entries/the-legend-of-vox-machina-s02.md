---
titles:
- The Legend of Vox Machina S02
taxonomies:
  media:
  - video
  formats:
  - television
  years:
  - 2023
  genres:
  - fantasy
  - adventure
  - comedy drama
  - fantasy
  - adult animated
  provenances:
  - United States
  places:
  - Exandria
  - Tal'Dorei
  artists:
  - Ashley Johnson
  - Brandon Auman
  - Chris Prynoski
  - Critical Role Productions
  - Laura Bailey
  - Liam O'Brien
  - Marisha Ray
  - Matthew Mercer
  - Neal Acree
  - Sam Riegel
  - Taliesin Jaffe
  - Travis Willingham
  languages:
  - English
collections:
- name: The Legend of Vox Machina
  number: 2
- name: Critical Role
  number: 2
- name: Dungeons & Dragons
  number: 5.92
dates:
  published: 2022-04-19
  updated: 2023-02-14
  experienced:
  - started: 2023-02-13
    finished: 2023-02-14
links:
- https://en.wikipedia.org/wiki/The_Legend_of_Vox_Machina#Season_2_(2023)
---

