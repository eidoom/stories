---
titles:
- Phasmophobia
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2020
dates:
  published: 2022-05-25
  updated: 2022-05-25
  experienced:
  - started: 2022-05-25
links:
- https://en.wikipedia.org/wiki/Phasmophobia_(video_game)
---

