---
titles:
- The Killing Moon
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  years:
  - 2012
  artists:
  - N.K. Jemisin
collections:
- name: Dreamblood
  number: 1
dates:
  published: 2020-08-20
  updated: 2021-07-30
  experienced:
  - started: 2020-08-22
    finished: 2020-08-29
links:
- https://en.wikipedia.org/wiki/The_Killing_Moon_(novel)
---
