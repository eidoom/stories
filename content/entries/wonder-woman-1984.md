---
titles:
- Wonder Woman 1984
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - superhero
  - fantasy
  - speculative fiction
  - fiction
  places:
  - United States
  - America
  years:
  - 2020
collections:
- name: DC Extended Universe
  number: 5
dates:
  published: 2021-01-06
  updated: 2021-01-06
  experienced:
  - started: 2021-01-05
    finished: 2021-01-05
description: A bit ridiculous but fun
links:
- https://en.wikipedia.org/wiki/Wonder_Woman_1984
---

