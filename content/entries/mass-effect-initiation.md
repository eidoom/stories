---
titles:
- Initiation
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  - space opera
  - speculative fiction
  - fiction
  years:
  - 2017
  artists:
  - N. K. Jemisin
  - Mac Walters
collections:
- name: 'Mass Effect: Andromeda'
  number: 2
- name: Mass Effect
  number: 3.4
dates:
  published: 2020-10-12
  updated: 2020-10-12
  experienced:
  - started: 2020-09-22
    finished: 2020-09-25
description: Harper joins the Initiative
links:
- https://en.wikipedia.org/wiki/Mass_Effect:_Andromeda_(book_series)#Initiation
---

The story of how Cora Harper met Alec Ryder (not to be confused with Alex Rider) and joined the Andromeda Initiative.
Since I found Cora to be less interesting than other crew members in Mass Effect: Andromeda, I was surprised to find I enjoyed this book a lot.
It played out with all the best features of an old-school Mass Effect mission.
