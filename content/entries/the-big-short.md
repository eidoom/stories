---
titles:
- The Big Short
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - biography
  - drama
  - comedy
  places:
  - United States
  - America
  years:
  - 2015
dates:
  published: 2021-02-08
  updated: 2021-02-08
  experienced:
  - started: 2021-02-08
    finished: 2021-02-08
description: The story behind the 2008 crash
links:
- https://en.wikipedia.org/wiki/The_Big_Short_(film)
---

Yes, this was inspired by the whole [GameStop thing](https://en.wikipedia.org/wiki/GameStop_short_squeeze).

A nice treatment of the [2008 crash](https://en.wikipedia.org/wiki/Financial_crisis_of_2007–2008).
Entertaining, informative, and utterly terrifying.
