---
titles:
- Dungeon Master's Guide 5th Edition
taxonomies:
  media:
  - book
  formats:
  - rulebook
  - sourcebook
  genres:
  - core
  years:
  - 2014
  artists:
  - Wizards of the Coast
collections:
- name: Dungeons & Dragons
  number: 5.13
- name: Dungeons & Dragons 5th Edition
  number: 3
dates:
  published: 2022-11-13
  updated: 2022-11-13
  experienced:
  - started: 2022-11-13
    finished: 2022-11-13
links:
- https://en.wikipedia.org/wiki/Dungeon_Master%27s_Guide#Dungeons_&_Dragons_5th_edition
---
