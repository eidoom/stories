---
titles:
- Leviathan Falls
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  years:
  - 2021
  artists:
  - James S. A. Corey
collections:
- name: The Expanse
  number: 9
- name: The Expanse (books)
  number: 9
dates:
  published: 2019-11-25
  updated: 2022-04-19
  experienced:
  - started: 2021-12-23
    finished: 2021-12-26
---

