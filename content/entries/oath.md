---
titles:
- 'Oath: Chronicles of Empire and Exile'
taxonomies:
  media:
  - game
  formats:
  - board game
  genres:
  - strategy
  - amerigame
  - fantasy
  - political
  - thematic
  years:
  - 2021
dates:
  published: 2021-09-01
  updated: 2022-05-06
  experienced:
  - started: 2021-08-31
    finished: 2021-08-31
description: Deep strategy
players:
  min: 1
  max: 6
links:
- https://www.boardgamegeek.com/boardgame/291572/oath-chronicles-empire-and-exile
- https://ledergames.com/products/oath-chronicles-of-empire-exile
---

The banner of the darkest secret and of the people’s favour; the oathkeeper and the usurper; citizens and exiles; advisors and denizens; relics; pawns and warbands; the Cradle, Provinces and Hinterland; the imperial chancellor.
The lingo of the game flows of the tongue.

A game of intricately linked and delicate rules.
Players are asymmetric and new cards can restructure the board.
I’m interested to see how the inter-game continuation plays out.
I enjoyed the “writing history” flavour and stylised artwork.

I played with four players.
