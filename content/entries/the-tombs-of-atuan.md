---
titles:
- The Tombs of Atuan
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1970
  artists:
  - Ursula K. Le Guin
collections:
- name: The Books of Earthsea
  number: 1
- name: Earthsea
  number: 3
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2021-12-27
    finished: 2022-01-23
links:
- https://en.wikipedia.org/wiki/The_Tombs_of_Atuan
---

