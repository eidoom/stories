---
titles:
- KSP Dev Diaries
taxonomies:
  media:
  - web
  formats:
  - blog
  years:
  - 2019
  artists:
  - Squad
dates:
  published: 2021-05-03
  updated: 2021-05-03
  experienced:
  - started: 2021-04-16
description: Kerbal Space Program 2 blog
links:
- https://www.kerbalspaceprogram.com/category/dev-diaries/
---

Interesting blog about making a rocket science game.
