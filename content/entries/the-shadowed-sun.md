---
titles:
- The Shadowed Sun
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2012
  artists:
  - N.K. Jemisin
collections:
- name: Dreamblood
  number: 2
dates:
  published: 2020-08-20
  updated: 2021-07-30
  experienced:
  - started: 2020-08-29
    finished: 2020-09-09
---
