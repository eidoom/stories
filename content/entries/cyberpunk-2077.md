---
titles:
- Cyberpunk 2077
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - cyberpunk
  - corporate capitalism
  - rpg
  - adventure
  - action
  - fiction
  - speculative fiction
  - science fiction
  - dystopian
  years:
  - 2020
  artists:
  - CD Projekt Red
collections:
- name: Cyberpunk 2077
  number: 1
dates:
  published: 2021-02-11
  updated: 2021-05-03
  experienced:
  - started: 2021-02-10
description: Down with the corpo scum!
completed: true
links:
- https://en.wikipedia.org/wiki/Cyberpunk_2077
- https://www.cyberpunk.net
---

### Streetkid playthough

2021-02-10 -- ?

I enjoyed this game.
