---
titles:
- Onward
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - family friendly
  - urban
  - fantasy
  - adventure
  - magic
  - animated
  - speculative fiction
  - fiction
  - urban fantasy
  places:
  - America
  - United States
  years:
  - 2020
  artists:
  - Pixar Animation Studios
dates:
  published: 2021-06-27
  updated: 2021-06-27
  experienced:
  - started: 2021-06-27
    finished: 2021-06-27
description: Surprising hilarious take on urban fantasy
links:
- https://en.wikipedia.org/wiki/Onward_(film)
---

