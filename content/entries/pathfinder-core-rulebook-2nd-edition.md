---
titles:
- Pathfinder Core Rulebook 2nd Edition
taxonomies:
  media:
  - book
  formats:
  - rulebook
  - sourcebook
  genres:
  - core
  years:
  - 2019
collections:
- name: Pathfinder
  number: 1
dates:
  published: 2023-01-16
  updated: 2023-01-16
  experienced:
  - started: 2023-01-14
    finished: 2023-01-15
links:
- https://en.wikipedia.org/wiki/List_of_Pathfinder_books#Pathfinder_Rulebooks_(2e)
---
