---
titles:
- 'Climate Change: The Facts'
taxonomies:
  media:
  - video
  formats:
  - television
  - documentary
  genres:
  - environment
  - ecology
  - factual
  years:
  - 2019
  artists:
  - David Attenborough
dates:
  published: 2021-02-18
  updated: 2021-02-18
  experienced:
  - started: 2021-02-18
    finished: 2021-02-18
description: “The future looks alarming indeed”
links:
- https://en.wikipedia.org/wiki/Climate_Change_%E2%80%93_The_Facts
---

It’s good to have a recommendation like this documentary in the pocket to suggest to just about everyone.
