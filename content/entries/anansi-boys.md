---
titles:
- Anansi Boys
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  - speculative fiction
  - fiction
  - comedy
  places:
  - United States
  - England
  - United Kingdom
  - America
  - Britain
  years:
  - 2005
  artists:
  - Neil Gaiman
collections:
collections:
- name: American Gods
  number: 2
- name: American Gods (novels)
  number: 2
dates:
  published: 2020-10-12
  updated: 2022-08-28
  experienced:
  - started: 2020-10-07
    finished: 2020-10-12
description: The story of Fat Charlie
links:
- https://en.wikipedia.org/wiki/Anansi_Boys
---

Reading [The City We Became](../the-city-we-became), I was reminded of American Gods, which I read some time ago.
This decided my next read as Anansi Boys.

This book is hilarious. 
It follows Charlie Nancy, who discovers that he has a brother who is a god.
We have run-ins with Charlie’s bankster boss, the coven of elderly ladies from Charlie’s childhood street, Charlie’s fiancé’s formidable mother, and the atavistic old gods of prehistory.
Charlie’s clashes with his brother, or rather himself, help him get over his fear of singing in public and some other shackles besides.

Charlie’s experience of working for an investment firm in London reminded me of [Cashback](../cashback).

Gaiman mixes the absurd and oddly pragmatic to create a magical reality, where the magic makes perfect sense until you think about it too much. 
It wouldn’t be divine, otherwise.
