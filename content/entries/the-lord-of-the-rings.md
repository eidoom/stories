---
titles:
- The Lord of the Rings
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1955
  artists:
  - J. R. R. Tolkien
collections:
- name: Middle-Earth
  number: 31
- name: Tolkien’s legendarium
  number: 4
dates:
  published: 2022-05-05
  updated: 2022-05-05
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Lord_of_the_Rings
---

