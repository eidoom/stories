---
titles:
- 'Fantastic Beasts: The Secrets of Dumbledore'
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2022
collections:
- name: Fantastic Beasts
  number: 3
- name: Wizarding World
  number: 11
dates:
  published: 2022-06-01
  updated: 2022-06-01
  experienced:
  - started: 2022-06-01
    finished: 2022-06-01
links:
- https://en.wikipedia.org/wiki/Fantastic_Beasts:_The_Secrets_of_Dumbledore
---

