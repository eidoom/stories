---
titles:
- The Garden of Roses
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  years:
  - unreleased
  artists:
  - Philip Pullman
collections:
- name: The Book of Dust
  number: 3
- name: Northern Lights
  number: 5
dates:
  published: 2021-07-30
  updated: 2021-07-30
---
Speculative title.
