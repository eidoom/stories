---
titles:
- The Expanse S03
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - science fiction
  - hard sci-fi
  - drama
  - space opera
  - mystery
  - thriller
  - horror
  - speculative fiction
  years:
  - 2018
collections:
- name: The Expanse (TV)
  number: 3
- name: The Expanse
  number: 103
dates:
  published: 2021-01-06
  updated: 2022-08-28
  experienced:
  - '?'
completed: true
links:
- https://en.wikipedia.org/wiki/List_of_The_Expanse_episodes#Season_3_(2018)
---
