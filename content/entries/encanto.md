---
titles:
- Encanto
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2021
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-01-03
    finished: 2022-01-03
links:
- https://en.wikipedia.org/wiki/Encanto_(film)
---

