---
titles:
- The Wasp Factory
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - psychological thriller
  - fiction
  - psychological
  - thriller
  - comedy
  - dark
  - scotland
  - literary
  places:
  - United Kingdom
  - Britain
  years:
  - 1984
  artists:
  - Iain Banks
dates:
  published: 2021-01-03
  updated: 2021-01-05
  experienced:
  - started: 2020-12-30
    finished: 2021-01-03
description: Very strange. Very good.
links:
- https://en.wikipedia.org/wiki/The_Wasp_Factory
---

Francis “Frank” Cauldhame is a psychopathic sixteen-year-old living with his aged hippy dad, Angus, on a wee island in the Moray Firth.
His mother Agnes, second wife of Angus, took to her heels on her “BSA 500” (a 500cc BSA A50 Wasp, [perhaps](https://en.wikipedia.org/wiki/List_of_BSA_motorcycles)?) after delivering Frank (the proximity to [Findhorn](https://en.wikipedia.org/wiki/Findhorn_Foundation) can’t be a coincidence).
He has a half-brother Eric from his father’s first marriage, but Eric’s in a [mental asylum](https://en.wikipedia.org/wiki/Lunatic_asylum#20th_century) in Belfast after suffering a total breakdown.

Frank used to have a younger half-brother too, Paul, but Frank murdered him by telling him to whack the “bell” on the beach—really an unexploded bomb from World War Two.
Their mother briefly returned to the island to deliver Paul before running off again,

> “determined to give birth in the lotus position (in which she claimed the child had been conceived) while going ‘Om’”.

At the moment of Paul’s delivery, Frank was savaged by the dog Old Saul, leaving him “eunuch but unique”.

Frank also murdered two of his cousins, one also blown up, except this time by an oversized kite, the other “subtracted... with an adder”.
And so the story goes on.

Frank’s internal monologue flits over past events and current secrets, mainly as odd, capitalised place names, begging the questions: what happened to Frank? Where is his mother? What happened to Eric? What’s in Angus’s office? What is the Factory?
Chapter by chapter, the answers are unveiled.

Frank is sexist, feeling intense misogyny.
At the climax, he undergoes anagnorisis on the revelation that he was born female but unknowingly fed male hormone treatment by his father as “an experimen” since the Old Saul incident.
His reaction, or lack thereof, and calm reflection is initially surprising until we remember that this is Frank, after all.

This novel is dark, deeply disturbing, and also witty, uproariously so at times.
It’s completely weird, and it’s thoroughly gripping.
The horror is never scary, usually delivered through disgust.
Both unsettling descriptions and punchlines are delivered with nonchalance. 
Frank’s alien perspective of the world and unfiltered honesty creates both a psychological thriller and a comedy.

