---
titles:
- Crouching Tiger, Hidden Dragon
- 臥虎藏龍
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - wuxia
  - fiction
  - martial arts
  - fantasy
  years:
  - 2000
  languages:
  - Mandarin
  artists:
  - Ang Lee
  - Chow Yun-fat
  - Michelle Yeoh
  - Zhang Ziyi
  - Chang Chen
  - Lang Sihung
  - Cheng Pei-pei
  - Tan Dun
dates:
  published: 2022-10-21
  updated: 2022-10-27
  experienced:
  - started: 2022-10-20
    finished: 2022-10-20
links:
- https://en.wikipedia.org/wiki/Crouching_Tiger,_Hidden_Dragon
---
