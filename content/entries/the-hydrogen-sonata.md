---
titles:
- The Hydrogen Sonata
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  years:
  - 2012
  artists:
  - Iain M. Banks
collections:
- name: Culture
  number: 10
- name: The Culture
  number: 8
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - started: 2019-08-21
    finished: 2019-10-03
links:
- https://en.wikipedia.org/wiki/The_Hydrogen_Sonata
---
