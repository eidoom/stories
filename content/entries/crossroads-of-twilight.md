---
titles:
- Crossroads of Twilight
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2003
  artists:
  - Robert Jordan
collections:
- name: The Wheel of Time
  number: 10
- name: The Wheel of Time universe
  number: 10
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
