---
titles:
- The Last of Us S01
taxonomies:
  media:
  - video
  formats:
  - television
  years:
  - 2023
  artists:
  - 'Bella Ramsey'
  - 'Craig Mazin'
  - 'David Fleming'
  - 'Gustavo Santaolalla'
  - 'Neil Druckmann'
  - 'Pedro Pascal'
  genres:
  - 'post-apocalyptic'
  - 'drama'
dates:
  published: 2023-01-19
  updated: 2023-01-19
  experienced:
  - started: 2023-01-19
    finished: 2023-03-13
links:
- https://en.wikipedia.org/wiki/The_Last_of_Us_(TV_series)
---
