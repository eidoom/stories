---
titles:
- 'Star Trek: Lower Decks S02'
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - science fiction
  - space
  - animated
  - cartoon
  - comedy
  years:
  - 2020
collections:
- name: Star Trek
  number: 6.2
- name: 'Star Trek: Lower Decks'
  number: 2
dates:
  published: 2021-05-14
  updated: 2022-08-28
  experienced:
  - started: 2021-09-30
    finished: 2021-10-14
links:
- https://en.wikipedia.org/wiki/Star_Trek:_Lower_Decks_(season_2)
---

An improvement on the first season, well enjoyed!
