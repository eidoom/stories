---
titles:
- Catan
- The Settlers of Catan
taxonomies:
  media:
  - game
  formats:
  - board game
  years:
  - 1995
dates:
  published: 2022-05-06
  updated: 2022-05-06
  experienced:
  - '?'
links:
- https://boardgamegeek.com/boardgame/13/catan
---

