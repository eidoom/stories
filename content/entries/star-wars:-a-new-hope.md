---
titles:
- 'Star Wars: Epsiode IV – A New Hope'
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - speculative fiction
  - fantasy
  - science fiction
  - space opera
  - epic
  - space
  - fiction
  years:
  - 1977
collections:
- name: Skywalker saga
  number: 4
- name: Star Wars original trilogy
  number: 1
- name: Star Wars
  number: 4
dates:
  published: 2021-03-20
  updated: 2021-03-20
  experienced:
  - started: 2021-03-20
    finished: 2021-03-20
description: Original Star Wars
links:
- https://en.wikipedia.org/wiki/Star_Wars_(film)
---

