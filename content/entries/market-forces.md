---
titles:
- Market Forces
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  - thriller
  - dystopian
  years:
  - 2004
  artists:
  - Richard K. Morgan
dates:
  published: 2020-03-01
  updated: 2021-07-30
  experienced:
  - started: 2020-04-06
    finished: 2020-04-13
links:
- https://en.wikipedia.org/wiki/Market_Forces
---
