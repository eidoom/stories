---
titles:
- Dune
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 1984
  artists:
  - David Lynch
collections:
- name: Dune
  number: 1.1
dates:
  published: 2021-10-24
  updated: 2021-10-24
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Dune_(1984_film)
---

