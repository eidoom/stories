---
titles:
- Kindred
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1979
  artists:
  - Octavia E. Butler
dates:
  published: 2022-12-10
  updated: 2022-12-10
links:
- https://en.wikipedia.org/wiki/Kindred_(novel)
---