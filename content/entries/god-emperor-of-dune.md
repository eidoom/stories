---
titles:
- God Emperor of Dune
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1981
  artists:
  - Frank Herbert
collections:
- name: Dune Chronicles
  number: 4
- name: Dune
  number: 4
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/God_Emperor_of_Dune
---
