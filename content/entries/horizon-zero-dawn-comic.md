---
titles:
- 'Horizon Zero Dawn: The Sunhawk'
taxonomies:
  media:
  - book
  formats:
  - comic
  years:
  - 2020
collections:
- name: 'Horizon: Zero Dawn'
  number: 1.1
dates:
  published: 2022-05-05
  updated: 2022-05-05
  experienced:
  - started: 2020-11-01
    finished: 2022-05-05
---

Issues 0-4.
