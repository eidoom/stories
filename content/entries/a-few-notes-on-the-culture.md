---
titles:
- A Few Notes on the Culture
taxonomies:
  media:
  - web
  formats:
  - essay
  years:
  - 1994
  artists:
  - Iain M. Banks
collections:
- name: The Culture
  number: 0
dates:
  published: 2021-01-27
  updated: 2021-02-07
  experienced:
  - started: 2021-01-27
    finished: 2021-01-27
description: Essential reading for Culture aficionados
links:
- http://www.vavatch.co.uk/books/banks/cultnote.htm
---
