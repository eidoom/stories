---
titles:
- Incredibles 2
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - family friendly
  - animated
  - superhero
  years:
  - 2018
  artists:
  - Pixar Animation Studios
dates:
  published: 2021-06-28
  updated: 2021-06-28
  experienced:
  - started: 2021-06-27
    finished: 2021-06-27
description: Super nostalgia
links:
- https://en.wikipedia.org/wiki/Incredibles_2
---

