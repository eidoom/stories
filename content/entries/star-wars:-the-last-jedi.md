---
titles:
- 'Star Wars: Episode VIII – The Last Jedi'
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - epic
  - science fiction
  - fantasy
  - speculative fiction
  - space opera
  - space
  - fiction
  years:
  - 2017
collections:
- name: Skywalker saga
  number: 8
- name: Star Wars sequel trilogy
  number: 2
- name: Star Wars
  number: 8
dates:
  published: 2020-11-29
  updated: 2020-11-29
  experienced:
  - started: 2020-11-28
    finished: 2020-11-28
links:
- https://en.wikipedia.org/wiki/Star_Wars:_The_Last_Jedi
---

