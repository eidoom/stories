---
titles:
- 'Avatar: The Last Airbender – Smoke and Shadow'
taxonomies:
  media:
  - book
  formats:
  - graphic novel
  years:
  - 2015
collections:
- name: 'Avatar: The Last Airbender'
  number: 65
dates:
  published: 2023-04-18
  updated: 2023-04-20
  experienced:
  - started: 2023-04-19
    finished: 2023-04-19
links:
- https://en.wikipedia.org/wiki/Avatar:_The_Last_Airbender_%E2%80%93_Smoke_and_Shadow
---
