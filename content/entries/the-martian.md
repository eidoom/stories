---
titles:
- The Martian
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2011
  artists:
  - Andy Weir
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
