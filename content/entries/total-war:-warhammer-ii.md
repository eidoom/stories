---
titles:
- 'Total War: Warhammer II'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - fantasy
  years:
  - 2017
dates:
  published: 2021-07-14
  updated: 2021-07-14
  experienced:
  - started: 2021-07-13
description: Nostalgic
links:
- https://en.wikipedia.org/wiki/Total_War:_Warhammer_II
---

