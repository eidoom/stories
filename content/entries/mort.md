---
titles:
- Mort
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  - fiction
  years:
  - 1983
  artists:
  - Terry Pratchett
collections:
- name: Death (Discworld)
  number: 1
- name: Discworld
  number: 4
dates:
  published: 2021-09-23
  updated: 2021-09-23
  experienced:
  - started: 2022-05-31
    finished: 2022-06-05
links:
- https://en.wikipedia.org/wiki/Mort
---

