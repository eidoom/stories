---
titles:
- Birdman or (The Unexpected Virtue of Ignorance)
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - drama
  years:
  - 2014
dates:
  published: 2021-04-11
  updated: 2021-04-11
  experienced:
  - started: 2021-04-11
    finished: 2021-04-11
description: A take on superhero movies
links:
- https://en.wikipedia.org/wiki/Birdman_(film)
---

Great long takes.
Superb drum track.

The character conflict and drama is intense.
Riggan’s crazy hallucinations keep us on our toes.
The film calls out its own playing with realism.
