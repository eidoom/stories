---
titles:
- Fargo S02
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - black comedy
  - crime
  - anthology
  - thriller
  - drama
  years:
  - 2015
collections:
- name: Fargo
  number: 2
dates:
  published: 2021-01-06
  updated: 2022-08-28
  experienced:
  - "?"
completed: true
links:
- https://en.wikipedia.org/wiki/Fargo_(season_2)
---
