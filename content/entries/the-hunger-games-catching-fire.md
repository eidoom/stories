---
titles:
- 'The Hunger Games: Catching Fire'
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2013
collections:
- name: The Hunger Games films
  number: 2
dates:
  published: 2022-12-18
  updated: 2022-12-18
  experienced:
  - started: 2022-12-14
    finished: 2022-12-14
links:
- https://en.wikipedia.org/wiki/The_Hunger_Games:_Catching_Fire
---
