---
titles:
- A Storm of Swords
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2000
  artists:
  - George R. R. Martin
collections:
- name: A Song of Ice and Fire
  number: 3
- name: Game of Thrones
  number: 3
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
