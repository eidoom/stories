---
titles:
- The Galaxy, and the Ground Within
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2021
  artists:
  - Becky Chambers
collections:
- name: Wayfarers
  number: 4
- name: Galactic Commons
  number: 4
dates:
  published: 2022-05-11
  updated: 2022-05-11
  experienced:
  - started: 2022-05-22
    finished: 2022-05-25
links:
- https://en.wikipedia.org/wiki/The_Galaxy,_and_the_Ground_Within
---

