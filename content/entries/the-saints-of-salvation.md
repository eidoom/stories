---
titles:
- The Saints of Salvation
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  - science fiction
  - space opera
  years:
  - 2020
  artists:
  - Peter F. Hamilton
collections:
- name: Salvation Sequence
  number: 3
dates:
  published: 2019-12-27
  updated: 2021-07-30
  experienced:
  - started: 2021-07-29
    finished: 2021-08-02
description: Culmination of the Salvation Sequence
---

It’s Hamilton, so there’re going to be delicious ideas of future societies and technologies, epic excitements, and deus ex machina.

## Background

In the near future, humanity is socially/politically/philosophically divided as capitalist Universals and progressive, post-scarcity communist Utopials.
The Utopials are genetically altered to be Omnia: they do not have fixed sex, but instead a thousand-day cycle.
Significant technologies include force field shields for cities, food printers, and “quantum spatial entanglement” portals.
A pair of portal doors, once conventionally positioned apart, allow instantaneous travel between them.
Civic power is produced and rocket propulsion provided by dropping one portal into a star: solar “powerwells”.
The corporation Connexion manages the Earth’s portal network and dominates Universal society, led by the Zangari family.
There are many inhabited orbitals, and terraformed exoplanets in nearby systems.

The Olyix aliens come to Sol peacefully, but are revealed to be religious fanatics whose purpose is to capture and cocoon all life in the galaxy for their god at the end of time.
They transport their harvest in “arkships” to their enclave, a system where time is manipulated to run slower than normal to accelerate transport to the end of the universe.
They have artificially augmented themselves so that each “quint”, as they are called, has five bodies but one mind.
Furthermore, their arkships and warships are the bodies of “oneminds”, and a “fullmind” exists in the enclave, with all Olyix connected into this hive mind network.

The mysterious Neána aliens, who hide between the stars, insidiously infiltrate human society, but are revealed to be trying to help humanity escape the Olyix.

In the far future, Utopial exodus humans spend a generation at a planet, living in fear and silence, then flee to the next one before they are found by the Olyix.
Their technology plateaued and hope fading, they create a generation of Binary soldiers to strike back.

We learn that in the history of the exodus, humans came together with other aliens to create the Factories, producing great warships with technologies now lost, before they all ran away in their own directions from the Olyix.

## Story

In London during Blitz2, Ollie Heslop, last survivor of the Southwark Legion gang, begrudgingly teams up with Kohei Yamada, Connexion Security, to take out criminal leader Nikolaj, really a human body of Olyix Gox quint, perishing in the attack.
His pregnant girlfriend, Lolo Maude, escapes to Delta Pavonis, the Utopial system, and joins the exodus.
Much later, her consciousness is transferred to a Factory ship.

Meanwhile, the Saints embark on their mission to infiltrate the Olyix enclave and broadcast its location.
They are: Yuri Alster, Connexion security; Callum Hepburn, old engineer turned Utopial advisor; Alik Monday, FBI; Kandara Martinez, black ops mercenary; and Jessika Mye, Neána metahuman.
Their mission turns out to be redundant, but their actions inspire millennia of exodus humans who treat them in a reverent manner.

In the far future, we join Dellian and Yirella on the *Morgan* in the aftermath of the Vayan disaster.
There, they lured the Olyix in an attempt to storm the enclave by using their portal, but were overwhelmed, saved only by the fortuitous appearance of Ainsley Zangari, Connexion founder and now Factory ship.
Yirella secretly seeds a neutron star system with humans who, by the time the *Morgan* reaches the system, have “elaborated” to become “corpus humans”: their consciousness is spread over many “aspects”, organic and artificial systems, and they have developed the technology to control the rate at which time passes in a region.
They have an egress faction who are isolationist and care for the naturalists, people who live low-technology lives in artificial environments, as well as a history faction who joins the *Morgan* and fleet to assault the Olyix enclave.
Their spokesperson is Immanueel.

The final strike on the enclave includes great losses but is successful.
The Saints, after a shoot out with Gox, are rescued, sans Alik, by Dellian and his squad.
The Olyix create temporal gradients to incapacitate the human fleet, but Yirella elaborates herself to counter their efforts.
Ainsley sacrifices himself to destroy the Olyix’s power source: a titanic ring encircling their star.
The arkships are seized with their suspended occupants.
The humans fire their neutron star at the primary star of the binary system, leaving a supernova in their wake.

The friendly saved species are given systems near Earth, forming an interstellar Alliance.
The hostile ones are placed rather further away.

Yirella splits into two beings, the original going with the corpus humans to eradicate the god at the end of time.
The clone Yirella stays with Dellian, enjoying some peace on recovered Earth before they embark to find Sanctuary, along with Horatio Seymour, who hopes to find his wife Gwendoline Seymour-Qing-Zangari there.
