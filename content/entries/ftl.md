---
titles:
- 'FTL: Faster Than Light'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2012
  artists:
  - Subset Games
dates:
  published: 2022-05-10
  updated: 2022-05-10
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/FTL:_Faster_Than_Light
---

