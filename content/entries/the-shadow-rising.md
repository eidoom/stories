---
titles:
- The Shadow Rising
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1992
  artists:
  - Robert Jordan
collections:
- name: The Wheel of Time
  number: 4
- name: The Wheel of Time universe
  number: 4
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
