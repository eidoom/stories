---
titles:
- The Adam Project
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2022
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-03-13
    finished: 2022-03-13
links:
- https://en.wikipedia.org/wiki/The_Adam_Project
---

