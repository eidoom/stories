---
titles:
- The Republic of Thieves
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2013
  artists:
  - Scott Lynch
collections:
- name: Gentleman Bastard
  number: 3
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
