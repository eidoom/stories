---
titles:
- Guildmasters' Guide to Ravnica
taxonomies:
  media:
  - book
  years:
  - 2018
  formats:
  - sourcebook
  artists:
  - Wizards of the Coast
  genres:
  - campaign
collections:
- name: Dungeons & Dragons
  number: 5.32
- name: Dungeons & Dragons 5th Edition
  number: 22
dates:
  published: 2022-11-13
  updated: 2022-11-13
  experienced:
  - started: 2022-11-13
    finished: 2022-11-13
links:
- https://en.m.wikipedia.org/wiki/Guildmasters'_Guide_to_Ravnica
---
