---
titles:
- Minecraft
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2009
  - 2011
dates:
  published: 2021-07-15
  updated: 2021-07-15
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Minecraft
---

