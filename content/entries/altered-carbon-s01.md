---
titles:
- Altered Carbon S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - speculative fiction
  - science fiction
  - cyberpunk
  - mystery
  - action
  - adventure
  - dystopian
  - social divide
  years:
  - 2018
collections:
- name: Altered Carbon
  number: 1.1
- name: Altered Carbon (TV)
  number: 1
dates:
  published: 2020-12-05
  updated: 2022-08-28
  experienced:
  - started: 2018-02-02
completed: true
links:
- https://en.wikipedia.org/wiki/Altered_Carbon_(TV_series)
---
