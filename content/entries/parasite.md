---
titles:
- Parasite
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - black comedy
  - thriller
  years:
  - 2019
  artists:
  - Bong Joon-ho
  places:
  - South Korea
dates:
  published: 2022-09-12
  updated: 2022-09-12
  experienced:
  - '?'
completed: true
links:
- https://en.wikipedia.org/wiki/Parasite_(2019_film)
---
