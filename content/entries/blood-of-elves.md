---
titles:
- Blood of Elves
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1994
  - 2008
  artists:
  - Andrzej Sapkowski
  - Danusia Stok
collections:
- name: The Witcher Saga
  number: 1
- name: The Witcher
  number: 3
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Blood_of_Elves
---
