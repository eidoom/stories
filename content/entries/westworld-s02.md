---
titles:
- Westworld S02
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - android
  - artificial intelligence
  - speculative fiction
  - fiction
  - science fiction
  - drama
  - western
  - dystopian
  - mystery
  - action
  - adventure
  - nonlinear
  years:
  - 2018
collections:
- name: Westworld
  number: 2
dates:
  published: 2020-12-05
  updated: 2022-08-28
  experienced:
  - "?"
completed: true
links:
- https://en.wikipedia.org/wiki/Westworld_(season_2)
---
