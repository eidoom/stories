---
titles:
- The Great Hunt
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1990
  artists:
  - Robert Jordan
collections:
- name: The Wheel of Time
  number: 2
- name: The Wheel of Time universe
  number: 2
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
