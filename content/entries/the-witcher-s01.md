---
titles:
- The Witcher S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - dark
  - fantasy
  - drama
  years:
  - 2019
collections:
- name: The Witcher (TV)
  number: 1
- name: The Witcher
  number: 0.91
dates:
  published: 2021-09-05
  updated: 2022-04-19
  experienced:
  - started: 2019-12-20
    finished: 2019-12-22
links:
- https://en.wikipedia.org/wiki/The_Witcher_(TV_series)#Season_1_(2019)
---
