---
titles:
- Serenity
taxonomies:
  media:
  - book
  formats:
  - comic
  years:
  - 2005
dates:
  published: 2022-05-05
  updated: 2022-05-05
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Serenity_(comics)
---

Read:
* Those Left Behind
* Better Days
* The Other Half
* Float Out
* The Shepherd's Tale
* Downtime and The Other Half
* It's Never Easy
* Leaves on the Wind
* The Warrior and the Wind
* No Power in the ’Verse
