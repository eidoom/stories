---
titles:
- Fizban's Treasury of Dragons
taxonomies:
  media:
  - book
  years:
  - 2021
  artists:
  - Wizards of the Coast
  genres:
  - supplement
  formats:
  - rulebook
  - sourcebook
collections:
- name: Dungeons & Dragons
  number: 5.25
- name: Dungeons & Dragons 5th Edition
  number: 15
dates:
  published: 2022-11-13
  updated: 2022-11-13
  experienced:
  - started: 2022-11-13
    finished: 2022-11-13
links:
- https://en.wikipedia.org/wiki/Fizban%27s_Treasury_of_Dragons
---
