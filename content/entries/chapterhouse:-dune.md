---
titles:
- 'Chapterhouse: Dune'
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1985
  artists:
  - Frank Herbert
collections:
- name: Dune Chronicles
  number: 6
- name: Dune
  number: 6
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Chapterhouse:_Dune
---
