---
titles:
- Observation
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - science fiction
  - horror
  - mystery
  - puzzle
  - thriller
  - fiction
  - speculative fiction
  years:
  - 2019
  artists:
  - No Code
dates:
  published: 2020-11-06
  updated: 2020-11-06
  experienced:
  - started: 2020-05-22
    finished: 2020-05-27
description: “I’m sorry, Dave. I’m afraid I can’t do that,” I said.
links:
- https://en.wikipedia.org/wiki/Observation_(video_game)
- https://www.observationgame.com/
---

This game evokes strong feelings of the last two sequences of *2001: A Space Odyssey*, except you play as HAL 9000, or rather Sam (Systems Administration and Maintenance) in this case.
It’s interesting to play as a space station rather than a human.

I found the game captivating.
The setting of the space station is very cool, the atmosphere is dark and deeply unsettling, the mysteries are intriguing, and I became quite attached to Dr. Emma Fisher for a computer.
