---
titles:
- Espedair Street
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fiction
  years:
  - 1987
  artists:
  - Iain Banks
dates:
  published: 2021-02-04
  updated: 2021-02-04
#  experienced:
#  - started: 2021-02-04
links:
- https://en.wikipedia.org/wiki/Espedair_Street
---
