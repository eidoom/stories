---
titles:
- 'Avatar: The Last Airbender – North and South'
taxonomies:
  media:
  - book
  formats:
  - graphic novel
  years:
  - 2016
collections:
- name: 'Avatar: The Last Airbender'
  number: 66
dates:
  published: 2023-04-18
  updated: 2023-04-20
  experienced:
  - started: 2023-04-20
#    finished: 2023-04-18
links:
- https://en.wikipedia.org/wiki/Avatar:_The_Last_Airbender_%E2%80%93_North_and_South
---
