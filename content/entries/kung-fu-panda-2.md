---
titles:
- Kung Fu Panda 2
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2011
  artists:
  - DreamWorks Animation
collections:
- name: Kung Fu Panda
  number: 2
dates:
  published: 2022-06-04
  updated: 2022-06-04
  experienced:
  - started: 2022-06-04
    finished: 2022-06-04
links:
- https://en.wikipedia.org/wiki/Kung_Fu_Panda_2
---

