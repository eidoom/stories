---
titles:
- The Castles of Burgundy
taxonomies:
  media:
  - game
  formats:
  - board game
  genres:
  - strategy
  - eurogame
  - medieval
  - point salad
  - territory building
  - dice
  years:
  - 2019
  artists:
  - Stefan Feld
dates:
  published: 2021-09-20
  updated: 2022-05-06
  experienced:
  - started: 2021-09-19
    finished: 2021-09-19
  - started: 2021-09-26
    finished: 2021-09-26
players:
  min: 1
  max: 4
links:
- https://en.wikipedia.org/wiki/The_Castles_of_Burgundy
- https://www.boardgamegeek.com/boardgame/271320/castles-burgundy
---

The 2019 edition includes eight expansions over the original 2011 version.

I played with three players.
