---
titles:
- The Drowning Faith
taxonomies:
  media:
  - book
  formats:
  - novella
  genres:
  - grimdark
  - epic fantasy
  - fantasy
  - high fantasy
  years:
  - 2020
  artists:
  - R. F. Kuang
collections:
- name: The Poppy War Trilogy
  number: 2.5
dates:
  published: 2022-07-10
  updated: 2022-07-10
  experienced:
  - started: 2022-07-10
    finished: 2022-07-10
---

