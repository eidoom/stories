---
titles:
- Things of Interest
taxonomies:
  media:
  - web
  formats:
  - collection
  - short story
  - blog
  years:
  - 2004
  artists:
  - Sam Hughes
dates:
  published: 2021-02-20
  updated: 2021-02-20
  experienced:
  - '?'
description: A gem of the Internet
links:
- https://qntm.org/
---

Someone who goes by the pseudonym qntm writes some fun short stories and posts them on their blog.

{{ short(title="The Difference", link="https://qntm.org/difference", year=2007) }}
Chatbot or human? A Turing test.

{{ short(title="I don’t know, Timmy, being God is a big responsibility", link="https://qntm.org/responsibility", year=2007, started="2021-02-20") }}

The stack of simulated universes.

Reminds me of [Cixin Liu’s Mirror](hold-up-the-sky.html#mirror) and [Devs](devs.html).

{{ short(title="Valuable Humans In Transit", link="https://qntm.org/transit", year=2006, started="2021-02-20") }}

Saved from extinction by the singularity.
An AI’s perspective.

{{ short(title="Lena", link="https://qntm.org/mmacevedo", started="2022-02-14") }}

A fictional article about the industry practice of using uploaded consciousnesses.
