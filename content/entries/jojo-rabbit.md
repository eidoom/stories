---
titles:
- Jojo Rabbit
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - comedy
  - drama
  - world war two
  - historical
  - fiction
  - nazism
  - satire
  - black comedy
  - nationalism
  - fascism
  - racism
  - war
  - bildungsroman
  years:
  - 2019
dates:
  published: 2021-10-03
  updated: 2021-10-03
  experienced:
  - started: 2021-10-02
    finished: 2021-10-02
links:
- https://en.wikipedia.org/wiki/Jojo_Rabbit
---

