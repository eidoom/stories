---
titles:
- The Jade Setter of Janloon
taxonomies:
  media:
  - book
  formats:
  - novella
  artists:
  - Fonda Lee
  years:
  - 2022
  genres:
  - prequel
  - fantasy
collections:
- name: The Green Bone Saga
  number: 0
dates:
  published: 2022-11-01
  updated: 2022-11-01
  experienced:
  - started: 2022-11-01
    finished: 2022-11-02
---
