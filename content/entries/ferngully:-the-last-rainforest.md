---
titles:
- 'FernGully: The Last Rainforest'
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - animated
  - musical
  - fantasy
  years:
  - 1992
dates:
  published: 2022-06-26
  updated: 2022-06-26
  experienced:
  - started: 2022-06-26
    finished: 2022-06-26
links:
- https://en.wikipedia.org/wiki/FernGully:_The_Last_Rainforest
---

