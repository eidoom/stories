---
titles:
- Babylon
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - epic
  - period
  - comedy
  - drama
  - dramedy
  - comedy drama
  years:
  - 2022
  artists:
  - Brad Pitt
  - Damien Chazelle
  - Diego Calva
  - Jean Smart
  - Jovan Adepo
  - Justin Hurwitz
  - Li Jun Li
  - Linus Sandgren
  - Marc Platt
  - Margot Robbie
  - Matthew Plouffe
  - Olivia Hamilton
  - Tom Cross
  provenances:
  - United States
dates:
  published: 2023-02-12
  updated: 2023-02-12
  experienced:
  - started: 2023-02-12
    finished: 2023-02-12
links:
- https://en.wikipedia.org/wiki/Babylon_(2022_film)
---