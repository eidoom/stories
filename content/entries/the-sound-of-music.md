---
titles:
- The Sound of Music
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - musical
  - drama
  years:
  - 1965
dates:
  published: 2021-01-06
  updated: 2021-01-06
  experienced:
  - started: 2020-12-31
    finished: 2020-12-31
description: A classic
links:
- https://en.wikipedia.org/wiki/The_Sound_of_Music_(film)
---

Considering I’m not so well versed in musicals or older films, I was surprised how much I enjoyed this.
