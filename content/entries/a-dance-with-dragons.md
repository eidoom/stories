---
titles:
- A Dance with Dragons
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2011
  artists:
  - George R. R. Martin
collections:
- name: A Song of Ice and Fire
  number: 5
- name: Game of Thrones
  number: 5
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
