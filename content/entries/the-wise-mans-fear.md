---
titles:
- The Wise Man’s Fear
- 'The Kingkiller Chronicle: Day Two'
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2011
  artists:
  - Patrick Rothfuss
collections:
- name: The Kingkiller Chronicle
  number: 2
dates:
  published: 2021-07-30
  updated: 2021-07-30
  experienced:
  - started: 2022-07-31
    finished: 2022-08-14
links:
- https://en.wikipedia.org/wiki/The_Wise_Man%27s_Fear
---
