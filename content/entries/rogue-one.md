---
titles:
- 'Rogue One: A Star Wars Story'
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - science fiction
  - fantasy
  - speculative fiction
  - space opera
  - space
  - fiction
  - epic
  - prequel
  years:
  - 2016
collections:
- name: Star Wars
  number: 3.9
dates:
  published: 2020-12-06
  updated: 2020-12-06
  experienced:
  - started: 2020-12-06
    finished: 2020-12-06
description: Saddest Star Wars
links:
- https://en.wikipedia.org/wiki/Rogue_One
---

