---
titles:
- Great Western Trail
taxonomies:
  media:
  - game
  formats:
  - board game
  genres:
  - point salad
  - strategy
  - animals
  - economic
  - western
  - cattle
  years:
  - 2016
dates:
  published: 2021-05-03
  updated: 2022-05-06
  experienced:
  - started: 2021-04-03
    finished: 2021-04-03
description: “Cows”
players:
  min: 2
  max: 4
links:
- https://boardgamegeek.com/boardgame/193738/great-western-trail
---

Plenty of different ways to earn victory points, yet presented in an intuitive manner using consistent iconography.
Fun to play while managing diverse strategies and tripping up your opponents, of course.

I played with three players.
