---
titles:
- 'Life is Feudal: Forest Village'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - city builder
  - medieval
  - survival
  - simulation
  - indie
  - early access
  - strategy
  years:
  - 2016
  - 2017
  artists:
  - Mindillusion
dates:
  published: 2021-06-08
  updated: 2021-06-08
  experienced:
  - started: 2021-04-30
description: Banished clone
links:
- https://en.wikipedia.org/wiki/Life_Is_Feudal
---

### Initial impressions

2021-05-04 -- 2021-05-04

A bit clunky but could be fun when in the mood.
