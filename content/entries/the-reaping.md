---
titles:
- The Reaping
taxonomies:
  media:
  - book
  formats:
  - short story
  years:
  - 2015
  artists:
  - Eric Fenstermaker
collections:
- name: Pillars of Eternity
  number: 1.23
dates:
  published: 2023-02-12
  updated: 2023-02-13
  experienced:
  - started: 2023-02-12
    finished: 2023-02-13
---
