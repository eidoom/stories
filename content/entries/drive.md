---
titles:
- Drive
taxonomies:
  media:
  - book
  formats:
  - short story
  genres:
  - science fiction
  years:
  - 2012
  artists:
  - James S. A. Corey
collections:
- name: The Expanse
  number: 2.6
- name: The Expanse (books)
  number: 2.6
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
