---
titles:
- Tales from the Loop
taxonomies:
  years:
  - 2020
  media:
  - video
  formats:
  - television
  genres:
  - science fiction
  - drama
  - fiction
  - speculative fiction
dates:
  published: 2020-10-08
  updated: 2020-11-07
  experienced:
  - started: 2020-07-12
    finished: 2020-07-19
description: Make the impossible possible
---

This is a collection of short stories all based around the town of Mercer, Ohio.
The pool of characters is explored through the episodes in an interesting manner that shows how their lives intersect.
The series has a quiet, melancholic feeling to it.

The aesthetic identity is strong; it feels Scandinavian with retro-future tech.
