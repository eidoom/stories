---
titles:
- The Northman
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - tragedy
  - epic
  - historical
  - fiction
  years:
  - 2022
dates:
  published: 2022-05-13
  updated: 2022-05-13
  experienced:
  - started: 2022-05-13
    finished: 2022-05-13
description: (H)amleth
links:
- https://en.wikipedia.org/wiki/The_Northman
---

Truly epic cinema.
A retelling of the tale of Hamlet, I mean, [Amleth](https://en.wikipedia.org/wiki/Amleth).
Could have done more interesting things with the story, though.
