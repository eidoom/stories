---
titles:
- The Silmarillion
taxonomies:
  media:
  - book
  formats:
  - collection
  genres:
  - mythopoeia
  years:
  - 1977
  artists:
  - J. R. R. Tolkien
  - Christopher Tolkien
collections:
- name: Tolkien’s legendarium
  number: 1
- name: Middle-Earth
  number: 20
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Silmarillion
---
