---
titles:
- 'Pathfinder: Kingmaker'
taxonomies:
  media:
  - game
  formats:
  - computer game
  genres:
  - crpg
  - isometric
  years:
  - 2018
  artists:
  - Owlcat Games
collections:
- name: Pathfinder
  number: 10
dates:
  published: 2022-08-28
  updated: 2022-08-28
links:
- https://en.wikipedia.org/wiki/Pathfinder:_Kingmaker
---
