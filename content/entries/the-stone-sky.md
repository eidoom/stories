---
titles:
- The Stone Sky
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2017
  artists:
  - N.K. Jemisin
collections:
- name: The Broken Earth
  number: 3
dates:
  published: 2020-07-20
  updated: 2021-07-30
  experienced:
  - started: 2020-07-20
    finished: 2020-07-25
links:
- https://en.wikipedia.org/wiki/The_Stone_Sky
---
