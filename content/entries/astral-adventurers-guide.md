---
titles:
- Astral Adventurer’s Guide
taxonomies:
  media:
  - book
  formats:
  - sourcebook
  years:
  - 2022
  artists:
  - Wizards of the Coast
  genres:
  - campaign
  artists:
  - Wizards of the Coast
collections:
- name: Dungeons & Dragons
  number: 5.40
- name: Dungeons & Dragons 5th Edition
  number: 30
- name: 'Spelljammer: Adventures in Space'
  number: 1
dates:
  published: 2023-01-16
  updated: 2023-01-16
  experienced:
  - started: 2023-01-15
    finished: 2023-01-15
links:
- https://en.wikipedia.org/wiki/Spelljammer:_Adventures_in_Space
---
