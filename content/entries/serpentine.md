---
titles:
- Serpentine
taxonomies:
  media:
  - book
  formats:
  - novella
  years:
  - 2004
  - 2020
collections:
- name: The Book of Dust
  number: 1.5
- name: Northern Lights
  number: 3.6
dates:
  published: 2021-08-09
  updated: 2022-11-01
  experienced:
  - started: 2022-10-31
    finished: 2022-10-31
links:
- https://en.wikipedia.org/wiki/Serpentine_(book)
---

