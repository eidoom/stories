---
titles:
- 'Dune: Part One'
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - science fiction
  - fiction
  - speculative fiction
  years:
  - 2021
  artists:
  - Denis Villeneuve
collections:
- name: Dune
  number: 1.3
dates:
  published: 2021-10-24
  updated: 2021-10-24
  experienced:
  - started: 2021-10-23
    finished: 2021-10-23
links:
- https://en.wikipedia.org/wiki/Dune_(2021_film)
---

