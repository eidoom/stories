---
titles:
- Half-Life
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 1998
collections:
- name: Half-Life
  number: 1
- name: Half-Life universe
  number: 1
dates:
  published: 2022-04-21
  updated: 2022-04-21
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Half-Life_(video_game)
---
Including:
* Half-Life: Opposing Force
* Half-Life: Blue Shift

Remade in [Black Mesa](black-mesa.html).
