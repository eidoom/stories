---
titles:
- Mr Robot S03
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - psychological thriller
  - drama
  - psychological
  - thriller
  - technology
  - corporate capitalism
  places:
  - United States
  - America
  years:
  - 2017
collections:
- name: Mr Robot
  number: 3
dates:
  published: 2021-02-24
  updated: 2022-08-28
  experienced:
  - started: 2021-03-01
    finished: 2021-03-06
links:
- https://en.wikipedia.org/wiki/List_of_Mr._Robot_episodes#Season_3_(2017)
---

Introducing Irving, master of spin, revealed to be Whiterose’s past lover.
Joanna is shot by secret lover Derek.
Turns out Ernesto Santiago is a DA mole; it’s not surprising from our perspective; they’re probably threatening his mother.
Mobley and Trenton are framed as fsociety’s leaders and executed by the DA.
Leon is a chill and chilling operative.
Trump is unknowingly Whiterose’s puppet.

Darlene works for the FBI, reporting on her brother.
They grow closer, despite the context.

It’s revealed that when Elliot tried to shoot Tyrell at the arcade, the gun failed, so he recruited him instead.
Then the DA turn up and manipulate delusional Tyrell to work for them.

Angela goes cold; she believes Whiterose’s Project will save everything so becomes Elliot’s hidden handler.
She lands him a job at E Corp to manage him, but he uses the position to sabotage Stage Two while Mr Robot works nights to make it happen.

Midway through the season, we have the epic day of Stage Two.
Whiterose has pressured Price to influence the UN to allow China to annex the Congo, where she will move her Project, but she had to ask twice so the attack proceeds.
The DA has roused a rebellion and chaos ensues as protestors violently break into E Corp’s buildings.
Angela has a super tense sequence with long single-shot scenes—it’s spectacular—in which she completes a critical part of Stage Two.
Elliot fights with Mr Robot to defuse the explosion, eventually convincing him to stop it when he reveals he has successfully diverted the target: E Corp’s paper records.
The New York City premises are saved, but the 71 facilities he redirected the records to were the real target; they are destroyed with thousands killed.

After this, Angela loses it.
She’s consumed by guilt and has to believe it was for a purpose.
After plenty of foreshadow, Price reveals he’s her father and convinces her she’s been played.

Meanwhile, Elliot resolves to commit suicide but is saved by a chance day out with Mohammad, Tr3nton’s younger brother.
He then hacks the DA.

Trenton’s dead man’s switch email reveals to Elliot that the Five/Nine encryption key was logged and is on Sentinel, the FBI’s system.
Darlene seduces Dom to steal her badge to get access, but is caught.
When Darlene reveals her plan, Santiago brings her and Dom to the DA’s estate; Irving brings Elliot.
Irving axes Santiago and recruits Dom, threatening her family.
Grant Chang, Whiterose’s number two and lover, jealously wants to kill Elliot but Elliot promises to get the Project moved to the Congo if he and Darlene live, which Whiterose calls in to accept from her bathtub, watching on a tablet.
Grant is cut loose and he shoots himself—turns out he really believed sacrifice for the cause is the greatest offering.

It transpires Mr Robot had the encryption key all along, on a blank disk we initially worry will be Darlene’s deletion disk.
Also, Darlene reveals Elliot wasn’t pushed: he jumped.
Elliot and Mr Robot make amends.
Elliot undoes Five/Nine.

At this point, we have to admit our hatred of Evil Corp was blinding us to the true devils.
Bringing them down didn’t help the people on the streets.
We were initially sympathetic to Whiterose but she is increasingly exposed as the real villain.
Don’t worry, we still don’t trust Price either.

It’s clear everything that has happen was allowed, indeed desired, by the elite.
Elliot vows to destroy them.
