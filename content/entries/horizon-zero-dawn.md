---
titles:
- 'Horizon: Zero Dawn'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - adventure
  - rpg
  - story
  - science fiction
  - open world
  - fiction
  - speculative fiction
  years:
  - 2020
  artists:
  - Guerrilla Games
collections:
- name: 'Horizon: Zero Dawn'
  number: 1
dates:
  published: 2020-10-08
  updated: 2020-10-08
  experienced:
  - started: 2020-08-08
description: Post-apocalyptic tribal future with robots
completed: true
links:
- https://en.wikipedia.org/wiki/Horizon_Zero_Dawn
- https://www.guerrilla-games.com/play/horizon
---

I love the setting: primitive societies living in the lush ruins of our existence.
I found the presence of animorphic robots to be jarring at first, but quickly accepted them as a central part of this beautiful world.
The tribes and individuals are diverse and carry real identity.
The combat, focused on identifying and exploiting the weaknesses of opponents, is clever and fun.
The stories are exciting; piecing together the history of our civilisation’s downfall with Aloy is an awe-inspiring journey.
