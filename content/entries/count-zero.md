---
titles:
- Count Zero
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - cyberpunk
  years:
  - 1986
  artists:
  - William Gibson
collections:
- name: The Sprawl
  number: 2
dates:
  published: 2022-12-31
  updated: 2023-01-14
  experienced:
  - started: 2022-12-31
    finished: 2023-01-14
links:
- https://en.wikipedia.org/wiki/Count_Zero
---
