---
titles:
- 'Kingdom Come: Deliverance'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2018
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2018-11-17
links:
- https://en.wikipedia.org/wiki/Kingdom_Come:_Deliverance
---

Including *The Amorous Adventures of Bold Sir Hans Capon*.
