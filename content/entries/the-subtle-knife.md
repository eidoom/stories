---
titles:
- The Subtle Knife
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1997
  artists:
  - Philip Pullman
collections:
- name: His Dark Materials
  number: 2
- name: Northern Lights
  number: 2
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - '?'
---
