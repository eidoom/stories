---
titles:
- Once Upon a Time in the North
taxonomies:
  media:
  - book
  formats:
  - novella
  years:
  - 2008
  artists:
  - Philip Pullman
collections:
- name: His Dark Materials
  number: 0.5
- name: Northern Lights
  number: 0.5
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Once_Upon_a_Time_in_the_North
---
