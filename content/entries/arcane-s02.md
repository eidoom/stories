---
titles:
- Arcane S02
taxonomies:
  media:
  - video
  formats:
  - television
  years:
  - unreleased
collections:
- name: League of Legends
  number: 11
- name: Arcane
  number: 2
dates:
  published: 2022-11-01
  updated: 2022-11-01
---

