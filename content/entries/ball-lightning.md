---
titles:
- Ball Lightning
- 球状闪电
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - prequel
  - science fiction
  - speculative fiction
  - fiction
  - hard sci-fi
  places:
  - China
  years:
  - 2004
  - 2018
  artists:
  - Joel Martinsen
  - Liu Cixin
collections:
- name: Three-Body
  number: 0
dates:
  published: 2020-11-27
  updated: 2020-11-29
  experienced:
  - started: 2020-11-27
    finished: 2020-11-29
description: The discovery of macro-particles
links:
- https://en.wikipedia.org/wiki/Ball_Lightning_(novel)
---
*Ball Lightning* (球状闪电) follows Chen life through his pursuit of understanding the phenomenon of [ball lightning](https://en.wikipedia.org/wiki/ball_lightning) after witnessing his parents being killed by it as a child.
He meets others who have devoted themselves to this task in vain and is joined by the “new-concepts weapons” miliary engineer Lin Yun and the theoretical physicist Ding Yi.

In reality, [many theories](https://arxiv.org/search/advanced?advanced=&terms-0-operator=AND&terms-0-term=ball+lightning&terms-0-field=title&classification-physics=y&classification-physics_archives=all&classification-include_cross_list=include&date-filter_by=all_dates&date-year=&date-from_date=2005&date-to_date=2020&date-date_type=submitted_date&abstracts=show&size=50&order=-announced_date_first) have been proposed to explain the phenomenon and a [detailed observation](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.112.035001) ([public text](https://www.researchgate.net/publication/260004540_Observation_of_the_Optical_and_Spectral_Characteristics_of_Ball_Lightning)) made, but the jury is still out.
Liu Cixin postulates a fantastical explanation of excited macro-electrons as the source of ball lightning.

The story is gripping with the unveiling of mysterious characters’ pasts as compelling as the unravelling of the scientific enigmas.
