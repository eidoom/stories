---
titles:
- Supernova Era
- 超新星纪元
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  - speculative fiction
  - fiction
  - hard sci-fi
  places:
  - China
  years:
  - 2003
  - 2019
  artists:
  - Joel Martinsen
  - Liu Cixin
dates:
  published: 2020-11-29
  updated: 2021-02-20
  experienced:
  - started: 2020-11-29
    finished: 2020-12-12
description: Children inherit the Earth
links:
- https://en.wikipedia.org/wiki/Liu_Cixin#Novels
---

*Supernova Era* (超新星纪元) marks somewhat of a departure from Liu Cixin’s other novels.
The story goes from strange to absurd as genetically resilient children take over the world after all adults die due to radiation from a nearby supernova.
Their actions reflect modern society’s floundering in the face of new technologies as demonstrated by global social division and the continuing ecological damage of the last century.
We end on an uplifting note, however, as thirty years into the Supernova Era we see people watching the Earth rise over the Martian horizon.
