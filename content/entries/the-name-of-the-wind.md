---
titles:
- The Name of the Wind
- 'The Kingkiller Chronicle: Day One'
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fiction
  - speculative fiction
  - story within a story
  - fantasy
  - heroic fantasy
  - epic fantasy
  years:
  - 2007
  artists:
  - Patrick Rothfuss
collections:
- name: The Kingkiller Chronicle
  number: 1
- name: Gollancz 50
  number: 9
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - started: 2022-07-24
    finished: 2022-07-31
links:
- https://en.wikipedia.org/wiki/The_Name_of_the_Wind
---
