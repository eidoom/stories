---
titles:
- The Lightning Tree
taxonomies:
  media:
  - book
  formats:
  - novella
  years:
  - 2014
  artists:
  - Patrick Rothfuss
collections:
- name: The Kingkiller Chronicle
  number: 0.9
- name: Rogues
  number: 20
dates:
  published: 2022-08-14
  updated: 2022-08-14
  experienced:
  - started: 2022-08-14
    finished: 2022-08-16
---

