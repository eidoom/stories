---
titles:
- Animal Farm
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1945
  artists:
  - George Orwell
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
