---
titles:
- Planet Earth II
taxonomies:
  media:
  - video
  formats:
  - television
  - documentary
  genres:
  - nature
  years:
  - 2016
  artists:
  - David Attenborough
dates:
  published: 2022-11-01
  updated: 2022-11-01
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Planet_Earth_II
---
