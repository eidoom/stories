---
titles:
- Noughts + Crosses S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - drama
  - alternative history
  - romance
  - bildungsroman
  - tragedy
  - racism
  - prejudice
  - discrimination
  places:
  - England
  years:
  - 2020
collections:
- name: Noughts + Crosses
  number: 1
dates:
  published: 2020-12-05
  updated: 2020-12-05
  experienced:
  - started: 2020-03-06
    finished: 2020-04-10
links:
- https://en.wikipedia.org/wiki/Noughts_%2B_Crosses
---
