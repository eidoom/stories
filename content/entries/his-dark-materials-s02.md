---
titles:
- His Dark Materials S02
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - fantasy
  - speculative fiction
  - drama
  - adventure
  - bildungsroman
  - mystery
  - parallel universe
  years:
  - 2020
collections:
- name: His Dark Materials (TV)
  number: 2
- name: Northern Lights
  number: 0.92
dates:
  published: 2020-12-05
  updated: 2022-08-28
  experienced:
  - started: 2020-11-03
    finished: 2021-01-05
links:
- https://en.wikipedia.org/wiki/His_Dark_Materials_(TV_series)#Series_2_(2020)
---
