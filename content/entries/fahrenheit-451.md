---
titles:
- Fahrenheit 451
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - dystopian
  - science fiction
  years:
  - 1953
  artists:
  - Ray Bradbury
dates:
  published: 2019-12-02
  updated: 2021-07-30
  experienced:
  - started: 2020-03-31
    finished: 2020-04-06
links:
- https://en.wikipedia.org/wiki/Fahrenheit_451
---
