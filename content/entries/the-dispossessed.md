---
titles:
- The Dispossessed
- 'The Dispossessed: An Ambiguous Utopia'
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - speculative fiction
  - science fiction
  - fiction
  - far future
  - utopian
  - solarpunk
  years:
  - 1974
  artists:
  - Ursula K. Le Guin
collections:
- name: Hainish Cycle
  number: 6
- name: The Ekumen
  number: 1
dates:
  published: 2021-06-23
  updated: 2021-08-07
  experienced:
  - started: 2021-06-24
    finished: 2021-06-30
description: Powerful politically, socially
links:
- https://en.wikipedia.org/wiki/The_Dispossessed
---

Wow.
These stories just keep getting better.
Very thought provoking on the structure of power within society.

The inhabitants of Anarres and Urras, a binary planet system of the star Tau Ceti, are the Cetians.
The superpowers on the primary planet, Anarres, are A-Io, a familiar capitalist society, and Thu, a authoritarian communist state.
The moon, Urras, is inhabited by the anarcho-syndicalist Odonians—our frontier outpost utopia.
The protagonist, Shevek, conceives of the theory that leads to the ansible (and maybe [later](rocannon's-world.html) to FTL travel, although Le Guin herself [reminds](the-birthday-of-the-world-and-other-stories.html#foreword) us that consistency was never the purpose of the so-called Hainish Cycle).
