---
titles:
- Kung Fu Panda 3
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2016
  artists:
  - DreamWorks Animation
collections:
- name: Kung Fu Panda
  number: 3
dates:
  published: 2022-06-07
  updated: 2022-06-07
  experienced:
  - started: 2022-06-07
    finished: 2022-06-07
links:
- https://en.wikipedia.org/wiki/Kung_Fu_Panda_3
---

