---
titles:
- Portal
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2007
collections:
- name: Portal
  number: 1
- name: Half-Life
  number: -2
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2010-05-19
completed: true
links:
- https://en.wikipedia.org/wiki/Portal_(video_game)
---

Spiritual successor to [*Narbacular Drop*](https://en.wikipedia.org/wiki/Narbacular_Drop).
