---
titles:
- A Psalm for the Wild-Built
taxonomies:
  media:
  - book
  formats:
  - novella
  genres:
  - solarpunk
  - science fiction
  years:
  - 2022
  artists:
  - Becky Chambers
collections:
- name: Monk & Robot
  number: 1
dates:
  published: 2022-05-25
  updated: 2022-05-25
  experienced:
  - started: 2022-08-24
    finished: 2022-08-24
links:
- https://en.wikipedia.org/wiki/A_Psalm_for_the_Wild-Built
---

