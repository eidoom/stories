---
titles:
- The Green Planet
taxonomies:
  media:
  - video
  formats:
  - television
  - documentary
  genres:
  - nature
  years:
  - 2022
  artists:
  - David Attenborough
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-02-07
    finished: 2022-03-02
links:
- https://en.wikipedia.org/wiki/The_Green_Planet_(TV_series)
---

