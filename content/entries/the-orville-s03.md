---
titles:
- The Orville S03
- "The Orville: New Horizons"
taxonomies:
  media:
  - video
  formats:
  - television
  years:
  - 2022
collections:
- name: The Orville
  number: 3
dates:
  published: 2022-06-15
  updated: 2022-08-28
  experienced:
  - started: 2022-06-13
    finished: 2022-08-24
links:
- https://en.wikipedia.org/wiki/The_Orville_(season_3)
---
