---
titles:
- American Gods
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  years:
  - 2001
  artists:
  - Neil Gaiman
collections:
collections:
- name: American Gods
  number: 1
- name: American Gods (novels)
  number: 1
dates:
  published: 2022-08-28
  updated: 2022-08-28
  experienced:
  - "?"
completed: true
links:
- https://en.wikipedia.org/wiki/American_Gods
---
