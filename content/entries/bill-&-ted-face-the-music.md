---
titles:
- Bill & Ted Face the Music
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - comedy
  - science fiction
  - fiction
  - time travel
  - speculative fiction
  - fantasy
  years:
  - 2020
collections:
- name: Bill & Ted
  number: 3
dates:
  published: 2020-11-22
  updated: 2020-11-22
  experienced:
  - started: 2020-11-09
    finished: 2020-11-09
description: Dude
---

Bill and Ted as middle age fathers still have not written the great song to save the planet, with Wyld Stallyns an unpopular, mediocre band.
They travel forwards in time to attempt to plagarise the song from themselves.
