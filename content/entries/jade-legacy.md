---
titles:
- Jade Legacy
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  years:
  - 2021
  artists:
  - Fonda Lee
collections:
- name: The Green Bone Saga
  number: 3
dates:
  published: 2022-08-23
  updated: 2022-08-23
  experienced:
  - started: 2022-10-02
    finished: 2022-10-18
links:
- https://en.wikipedia.org/wiki/Jade_Legacy
---

