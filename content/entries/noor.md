---
titles:
- Noor
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2022
  artists:
  - Nnedi Okorafor
dates:
  published: 2022-12-10
  updated: 2022-12-10
links:
- https://en.wikipedia.org/wiki/Noor_(novel)
---