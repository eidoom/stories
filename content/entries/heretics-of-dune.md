---
titles:
- Heretics of Dune
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1984
  artists:
  - Frank Herbert
collections:
- name: Dune Chronicles
  number: 5
- name: Dune
  number: 5
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Heretics_of_Dune
---
