---
titles:
- 'Star Trek: Lower Decks S04'
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - science fiction
  - space
  - animated
  - cartoon
  - comedy
  years:
  - unreleased
collections:
- name: Star Trek
  number: 6.4
- name: 'Star Trek: Lower Decks'
  number: 4
dates:
  published: 2022-11-01
  updated: 2022-11-01
#experienced:
# - started: 2021-09-30
#   finished: 2021-10-14
links:
- https://en.wikipedia.org/wiki/Star_Trek:_Lower_Decks_(season_4)
---
