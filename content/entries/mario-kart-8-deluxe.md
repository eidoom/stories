---
titles:
- Mario Kart 8 Deluxe
taxonomies:
  media:
  - game
  formats:
  - video game
  genres:
  - nintendo switch
  years:
  - 2017
  artists:
  - Nintendo EAD
dates:
  published: 2022-08-18
  updated: 2022-08-18
  experienced:
  - started: 2022-08-17
links:
- https://en.wikipedia.org/wiki/Mario_Kart_8#Mario_Kart_8_Deluxe
---

