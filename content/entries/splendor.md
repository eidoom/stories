---
titles:
- Splendor
taxonomies:
  media:
  - game
  formats:
  - board game
  years:
  - 2014
dates:
  published: 2022-05-06
  updated: 2022-05-06
  experienced:
  - '?'
links:
- https://boardgamegeek.com/boardgame/148228/splendor
---

