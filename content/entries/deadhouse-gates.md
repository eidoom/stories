---
titles:
- Deadhouse Gates
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2000
  artists:
  - Steven Erikson
collections:
- name: Malazan Book of the Fallen
  number: 2
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - '?'
---
