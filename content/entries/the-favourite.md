---
titles:
- The Favourite
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - comedy
  - drama
  - historical
  years:
  - 2018
dates:
  published: 2021-07-03
  updated: 2021-07-10
  experienced:
  - started: 2021-07-03
    finished: 2021-07-03
description: Striking mise-en-scène, amusing, doleful
---

