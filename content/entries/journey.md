---
titles:
- Journey
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - arthouse
  - adventure
  years:
  - 2012
  - 2019
  artists:
  - Thatgamecompany
  - Santa Monica Studio
  - Austin Wintory
dates:
  published: 2021-04-22
  updated: 2021-04-22
  experienced:
  - started: 2021-04-18
links:
- https://en.wikipedia.org/wiki/Journey_(2012_video_game)
---

