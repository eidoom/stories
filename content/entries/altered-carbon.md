---
titles:
- Altered Carbon
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2002
  artists:
  - Richard K. Morgan
collections:
- name: Takeshi Kovacs
  number: 1
- name: Altered Carbon
  number: 1
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Altered_Carbon
---
