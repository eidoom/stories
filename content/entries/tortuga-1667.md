---
titles:
- Tortuga 1667
taxonomies:
  media:
  - game
  formats:
  - board game
  years:
  - 2017
dates:
  published: 2022-08-10
  updated: 2022-08-10
  experienced:
  - started: 2022-08-10
    finished: 2022-08-10
links:
- https://boardgamegeek.com/boardgame/218530/tortuga-1667
---

Played with 5 players.
