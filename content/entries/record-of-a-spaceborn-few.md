---
titles:
- Record of a Spaceborn Few
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2018
  artists:
  - Becky Chambers
collections:
- name: Wayfarers
  number: 3
- name: Galactic Commons
  number: 3
dates:
  published: 2022-05-11
  updated: 2022-05-11
  experienced:
  - started: 2022-05-15
    finished: 2022-05-21
links:
- https://en.wikipedia.org/wiki/Record_of_a_Spaceborn_Few
---

## Setting

The home of humanity—with the exception of emigrants to Galactic Commons (GC) central space and the independent colonies, and Solans—is the Exodus Fleet.
The Fleet is comprised of several homesteaders, ships where people live permanently.

The purpose of these generation ships was to carry humanity to a new home in a fresh star system after the ecosystem of Earth fell apart.
The Fleet now orbits a star given to them by the GC.
The Exodan culture is highly traditional, and the people who live have embraced their identity as spacers.
Some take this too far and are bigoted towards grounders.
The victims of this attitude include those who leave and those who come to join the Fleet.

The Exodans are pacifist communists.
They had to live harmoniously in scarcity on their ships, drifting though the darkness of interstellar space, although meeting and joining the GC has somewhat alleviated this.
The effect is due to increased material wealth and access to technology, as well as a decreasing population due to emigration.
However, they still have far less than others in the GC, for example, lacking advanced healthcare (unless you have the creds) or AI technology.

In the prologue, the *Oxomoco* homesteader suffered a catastrophic decompression due to a shuttle crash, with tens of thousands dying.

The Solans, who are mostly Martians, have a more capitalist, American society.
While Earth is now uninhabitable, the Gaiist cult live there as hunter gatherers.

## Characters

We follow the lives of several people on the *Asteria* homesteader.
In this closed system, the community is tightly knit, and everyone’s paths are intimately interwoven.

### Ghuh’loloan Mok Chutp

The only non-human perspective: a Harmagian.
An ethnographer.
Visiting Isabel on the Exodus Fleet to learn.
We read her research diary.

### Tessa Santoso

Ashby’s sister.
Wife to long haul miner George Santoso (notice people take the surname of the house they live in).
Mother to two small children, Aya and Ky.
Also looks after her elderly father.
Works in logistics and managing supplies.

Aya experienced a containment breach when she was very young and is now terrified by living on a ship.
Sawyer’s death increases her anxiety.

Ghuh‘loloan’s publications about her visit lead to increased support from GC individuals.
This includes donations of AIs to improve resource management, and costs Tessa her job.
Tessa decides to move with her family to the independent colonies.

### Isabel Itoh

Older woman, wife to Tamsin.
Head archivist on the *Asteria*.
Hosting Ghuh’loloan.

### Eyas Parata

Adult woman.
A caretaker: she manages funerals, supports the bereaved, and composts corpses—nothing goes to waste in the traditionally closed cycle of a homesteader.
She struggles with the social distance at which her vocation puts her from everyone else.

Her best friend is Sunny, a host at the White Door tryst club (similar to a brothel).

She has a chance encounter with Sawyer, so recognises him when his body is found.
This prompts her to start a centre for helping immigrants to settle in the fleet with Sunny.

### Kristofer ‘Kip’ Madaki

Adolescent boy.
Completely bored by life in the Fleet, dreams of a bigger life “out there”.
This restlessness, and the influence of his friend Ras, get him into a lot of trouble.

He overhears the scavenger crew taking about Sawyer dying, and how they covered it up.
He reports this and attends the subsequent funeral with Isabel, Tamsin, and Eyas.
That chapter is a real tear-jerker.

Isabel tells him to go out and experience the galaxy, then come back and apprentice with her.

### Sawyer Gursky

Young man.
Born planetside on Mushtullo, core GC.
Orphaned at six.
Immigrates to the Fleet, drawn by the (genuine) promise of a place where no one goes hungry and everyone has a home.
Struggles to integrate into Exodan society, is shunned by most as an outsider.
Accepts a job with a scavenger crew, who Sawyer doesn’t realise are operating illegally.
Is killed in an accident on his first gig.
