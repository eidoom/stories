---
titles:
- True Detective S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - crime
  - drama
  - fiction
  - anthology
  - mystery
  - detective
  - neo-noir
  - nonchronological
  - nonlinear
  years:
  - 2014
collections:
- name: True Detective
  number: 1
dates:
  published: 2021-04-22
  updated: 2022-08-28
  experienced:
  - started: 2020-06-06
completed: true
links:
- https://en.wikipedia.org/wiki/True_Detective_(season_1)
---

Outstanding.
