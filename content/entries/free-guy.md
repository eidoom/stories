---
titles:
- Free Guy
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - comedy
  - romance
  - action
  - science fiction
  years:
  - 2021
dates:
  published: 2021-10-01
  updated: 2021-10-01
  experienced:
  - started: 2021-09-30
    finished: 2021-09-30
links:
- https://en.wikipedia.org/wiki/Free_Guy
---

