---
titles:
- His Dark Materials S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - fantasy
  - speculative fiction
  - drama
  - adventure
  - bildungsroman
  - mystery
  - parallel universe
  years:
  - 2019
collections:
- name: His Dark Materials (TV)
  number: 1
- name: Northern Lights
  number: 0.91
dates:
  published: 2020-12-05
  updated: 2022-08-28
  experienced:
  - started: 2020-11-03
completed: true
links:
- https://en.wikipedia.org/wiki/His_Dark_Materials_(TV_series)#Series_1_(2019)
---
