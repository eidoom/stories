---
titles:
- 'Mass Effect: Andromeda'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2017
collections:
- name: Mass Effect
  number: 4
dates:
  published: 2022-05-07
  updated: 2022-05-07
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Mass_Effect:_Andromeda
---

