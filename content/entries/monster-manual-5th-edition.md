---
titles:
- Monster Manual 5th Edition
taxonomies:
  media:
  - book
  formats:
  - rulebook
  - sourcebook
  genres:
  - core
  - bestiary
  years:
  - 2014
  artists:
  - Wizards of the Coast
collections:
- name: Dungeons & Dragons
  number: 5.12
- name: Dungeons & Dragons 5th Edition
  number: 2
dates:
  published: 2022-11-13
  updated: 2022-11-13
  experienced:
  - started: 2022-11-13
    finished: 2022-11-13
links:
- https://en.wikipedia.org/wiki/Monster_Manual#Dungeons_&_Dragons_5th_edition
---
