---
titles:
- Gardens of the Moon
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1999
  artists:
  - Steven Erikson
collections:
- name: Malazan Book of the Fallen
  number: 1
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - '?'
---
