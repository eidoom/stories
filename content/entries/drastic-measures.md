---
titles:
- Drastic Measures
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2018
  artists:
  - Dayton Ward
collections:
- name: Star Trek
  number: 6.6
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
