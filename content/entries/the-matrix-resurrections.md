---
titles:
- The Matrix Resurrections
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2021
collections:
- name: The Matrix
  number: 4
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-01-08
    finished: 2022-01-08
links:
- https://en.wikipedia.org/wiki/The_Matrix_Resurrections
---

