---
titles:
- The Last Flight of the Cassandra
taxonomies:
  media:
  - book
  formats:
  - short story
  years:
  - 2019
collections:
- name: The Expanse
  number: 1.1
- name: The Expanse (books)
  number: 1.1
dates:
  published: 2021-07-30
  updated: 2021-07-30
  experienced:
  - '?'
description: It’ll definitely be fine to land on this asteroid
---

