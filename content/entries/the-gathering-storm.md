---
titles:
- The Gathering Storm
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2009
  artists:
  - Robert Jordan
  - Brandon Sanderson
collections:
- name: The Wheel of Time
  number: 12
- name: The Wheel of Time universe
  number: 12
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
