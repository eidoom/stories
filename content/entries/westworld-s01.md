---
titles:
- Westworld S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - android
  - artificial intelligence
  - speculative fiction
  - fiction
  - science fiction
  - drama
  - western
  - dystopian
  - mystery
  - action
  - adventure
  - nonlinear
  years:
  - 2016
collections:
- name: Westworld
  number: 1
dates:
  published: 2020-12-05
  updated: 2022-08-28
  experienced:
  - started: 2017-10-01
completed: true
links:
- https://en.wikipedia.org/wiki/Westworld_(season_1)
---
