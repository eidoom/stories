---
titles:
- 'Star Trek: Discovery S01'
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - speculative fiction
  - science fiction
  - action
  - adventure
  - drama
  - space
  - future
  - aliens
  years:
  - 2017
collections:
- name: 'Star Trek: Discovery'
  number: 1
- name: Star Trek
  number: 7.1
dates:
  published: 2020-12-05
  updated: 2022-08-28
  experienced:
  - '?'
completed: true
links:
- https://en.wikipedia.org/wiki/Star_Trek:_Discovery_(season_1)
---
