---
titles:
- The Rise of Endymion
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1997
  artists:
  - Dan Simmons
collections:
- name: Hyperion Cantos
  number: 4
dates:
  published: 2022-08-23
  updated: 2022-08-23
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Rise_of_Endymion
---

