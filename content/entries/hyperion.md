---
titles:
- Hyperion
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1989
  artists:
  - Dan Simmons
collections:
- name: Hyperion Cantos
  number: 1
- name: Gollancz 50
  number: 8
dates:
  published: 2022-08-23
  updated: 2022-08-23
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Hyperion_(Simmons_novel)
---

