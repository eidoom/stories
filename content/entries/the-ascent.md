---
titles:
- The Ascent
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - cyberpunk
  years:
  - 2021
dates:
  published: 2021-10-14
  updated: 2021-10-14
  experienced:
  - started: 2021-10-10
links:
- https://en.wikipedia.org/wiki/The_Ascent_(video_game)
---

