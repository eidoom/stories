---
titles:
- The Pianist
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - drama
  - historical
  - biography
  - war
  - world war two
  - nonfiction
  years:
  - 2002
dates:
  published: 2020-10-12
  updated: 2020-10-12
  experienced:
  - started: 2020-10-03
    finished: 2020-10-03
description: Or, the life of a Jew in Nazi-occupied Poland
---

Being a pianist myself, I thought I must watch The Pianist.
The beautiful music, however, takes second stage to the attrocity of the Holocaust.
Szpilman’s struggle for survival is heart-rending.

There are unforgettable scenes which make very real this dark era of history.
In one such scene, a child attempts to crawl through a gutter to cross the wall separating the Jewish ghetto from the rest of Warsaw.
As Szpilman sees this, the child is violently attacked by an assailant on the free size of the wall.
The wall hides our sight of the violence, but what we can hear is sickening and the child is screaming.
Szpilman helps pull him through, but the child is already dead.

This film contains many moments of beauty and compassion, but is of course very painful.
Viewing this pain is important, lest we forget.
