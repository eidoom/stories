---
titles:
- 'The Lord of the Rings: The Rings of Power S02'
taxonomies:
  media:
  - video
  formats:
  - television
  years:
  - unreleased
collections:
- name: Middle-Earth
  number: 20.2
- name: 'The Lord of the Rings: The Rings of Power'
  number: 2
dates:
  published: 2022-07-11
  updated: 2022-10-15
---
