---
titles:
- The Evolutionary Void
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2009
  artists:
  - Peter F. Hamilton
collections:
- name: The Void Trilogy
  number: 3
- name: Commonwealth universe
  number: 6
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
