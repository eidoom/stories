---
titles:
- Dungeons & Dragons 5th Edition
taxonomies:
  media:
  - game
  formats:
  - tabletop
  - role-playing game
  genres:
  - fantasy
  years:
  - 2014
  artists:
  - Wizards of the Coast
dates:
  published: 2022-04-19
  updated: 2022-11-13
  experienced:
  - started: 2022-01-23
    finished: 2022-01-23
collections:
- name: Dungeons & Dragons
  number: 5
- name: Dungeons & Dragons 5th Edition
  number: 0
links:
- https://en.wikipedia.org/wiki/Dungeons_%26_Dragons#5th_Edition
- https://en.wikipedia.org/wiki/Editions_of_Dungeons_%26_Dragons#Dungeons_&_Dragons_5th_edition
- https://en.wikipedia.org/wiki/List_of_Dungeons_%26_Dragons_rulebooks#Dungeons_&_Dragons_5th_edition
- http://media.wizards.com/2018/dnd/downloads/DnD_BasicRules_2018.pdf
- http://media.wizards.com/2016/downloads/DND/SRD-OGL_V5.1.pdf
---

