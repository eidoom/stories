---
titles:
- 'Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb'
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - comedy
  - satire
  - political
  - black comedy
  - cold war
  places:
  - America
  - United States
  years:
  - 1964
  artists:
  - Stanley Kubrick
dates:
  published: 2021-06-24
  updated: 2021-06-24
  experienced:
  - '?'
description: Cold war satire
links:
- https://en.wikipedia.org/wiki/Dr._Strangelove
---

