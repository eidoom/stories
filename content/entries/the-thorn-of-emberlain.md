---
titles:
- The Thorn of Emberlain
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - unreleased
  artists:
  - Scott Lynch
collections:
- name: Gentleman Bastard
  number: 4
dates:
  published: 2019-11-25
  updated: 2021-07-30
---
