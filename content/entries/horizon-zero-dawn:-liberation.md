---
titles:
- 'Horizon Zero Dawn: Liberation'
taxonomies:
  media:
  - book
  formats:
  - comic
  years:
  - 2021
collections:
- name: 'Horizon: Zero Dawn'
  number: 1.2
dates:
  published: 2022-05-05
  updated: 2022-05-05
  experienced:
  - started: 2022-05-05
    finished: 2022-05-06
---

Issues 1-4.
