---
titles:
- Cashback
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - comedy
  - drama
  - romance
  - fiction
  places:
  - England
  years:
  - 2006
dates:
  published: 2020-10-08
  updated: 2020-10-08
  experienced:
  - started: 2020-09-27
    finished: 2020-09-27
description: The secret life of supermarket workers
---

This is a film about an insomniac, but quite different from Fight Club.
Ben, an art student, works nights in Sainsbury’s.
He shares experiences which are either fantasy or the delusions of his sleep-deprived mind; it doesn’t matter which.
They are punctuated by creative shots, insights into working class subculture, and an obsession with the beauty of the female form.
It’s very funny, in a distinctly naughties British fashion.
