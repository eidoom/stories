---
titles:
- 'The Hunger Games: Mockingjay – Part 1'
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2014
  artists:
  - 'Alan Edward Bell'
  - 'Danny Strong'
  - 'Donald Sutherland'
  - 'Elizabeth Banks'
  - 'Jeffrey Wright'
  - 'Jennifer Lawrence'
  - 'Jon Kilik'
  - 'Josh Hutcherson'
  - 'Julianne Moore'
  - 'Liam Hemsworth'
  - 'Mark Yoshikawa'
  - 'Nina Jacobson'
  - 'Peter Craig'
  - 'Philip Seymour Hoffman'
  - 'Stanley Tucci'
  - 'Woody Harrelson'
collections:
- name: The Hunger Games films
  number: 3
dates:
  published: 2023-01-12
  updated: 2023-01-12
  experienced:
  - started: 2023-01-11
    finished: 2023-01-11
links:
- https://en.wikipedia.org/wiki/The_Hunger_Games:_Mockingjay_–_Part_1
---
