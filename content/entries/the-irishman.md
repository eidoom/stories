---
titles:
- The Irishman
- I Heard You Paint Houses
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - biography
  - crime
  - epic
  - drama
  years:
  - 2019
  artists:
  - Martin Scorsese
dates:
  published: 2021-11-07
  updated: 2021-11-07
  experienced:
  - started: 2021-11-06
    finished: 2021-11-06
description: The story of Frank Sheeran
links:
- https://en.wikipedia.org/wiki/The_Irishman
---

This ticks all the boxes to be a film that I like, and I think it is a good film, but I didn’t enjoy watching it all that much.
Perhaps I was in the wrong frame of mind, maybe it was too soon after [True Detective Season 3](true-detective-s03.html).
