---
titles:
- 'Pillars of Eternity II: Deadfire'
- "Pillars of Eternity II: Deadfire - Obsidian Edition"
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2018
collections:
- name: Pillars of Eternity
  number: 2
dates:
  published: 2022-05-13
  updated: 2023-03-05
  experienced:
  - started: 2023-03-04
links:
- https://en.wikipedia.org/wiki/Pillars_of_Eternity_II:_Deadfire
active: true
---

