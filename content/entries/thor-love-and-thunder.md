---
titles:
- 'Thor: Love and Thunder'
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2022
  artists:
  - Chris Hemsworth
  - Christian Bale
  - Tessa Thompson
  - Jaimie Alexander
  - Taika Waititi
  - Russell Crowe
  - Natalie Portman
collections:
- name: Marvel Cinematic Universe
  number: 29
- name: MCU Thor
  number: 4
dates:
  published: 2022-09-09
  updated: 2022-09-09
  experienced:
  - started: 2022-09-08
    finished: 2022-09-08
links:
- https://en.wikipedia.org/wiki/Thor:_Love_and_Thunder
---
