---
titles:
- A Crown of Swords
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1996
  artists:
  - Robert Jordan
collections:
- name: The Wheel of Time
  number: 7
- name: The Wheel of Time universe
  number: 7
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
