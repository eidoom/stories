---
titles:
- Alex Rider S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - espionage
  - thriller
  - action
  - adventure
  - mystery
  - bildungsroman
  places:
  - England
  years:
  - 2020
dates:
  published: 2020-12-05
  updated: 2020-12-05
  experienced:
  - started: 2020-06-04
    finished: 2020-06-11
links:
- https://en.wikipedia.org/wiki/Alex_Rider_(TV_series)
---

