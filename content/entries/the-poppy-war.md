---
titles:
- The Poppy War
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - grimdark
  - epic fantasy
  - fantasy
  - high fantasy
  years:
  - 2018
  artists:
  - R. F. Kuang
collections:
- name: The Poppy War Trilogy
  number: 1
dates:
  published: 2022-06-11
  updated: 2022-06-11
  experienced:
  - started: 2022-06-11
    finished: 2022-06-28
links:
- https://en.wikipedia.org/wiki/The_Poppy_War
---

