---
titles:
- The Amber Spyglass
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2000
  artists:
  - Philip Pullman
collections:
- name: His Dark Materials
  number: 3
- name: Northern Lights
  number: 3
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - '?'
---
