---
titles:
- The Neutronium Alchemist
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1997
  artists:
  - Peter F. Hamilton
collections:
- name: The Night’s Dawn Trilogy
  number: 2
- name: Confederation universe
  number: 2
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
