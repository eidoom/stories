---
titles:
- The Burning God
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - grimdark
  - epic fantasy
  - fantasy
  - high fantasy
  years:
  - 2020
  artists:
  - R. F. Kuang
collections:
- name: The Poppy War Trilogy
  number: 3
dates:
  published: 2022-06-28
  updated: 2022-06-28
  experienced:
  - started: 2022-07-10
    finished: 2022-07-24
---

