---
titles:
- Sourcery
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1988
  artists:
  - Terry Pratchett
collections:
- name: Rincewind
  number: 3
- name: Discworld
  number: 5
dates:
  published: 2022-06-05
  updated: 2022-06-05
  experienced:
  - started: 2022-06-05
    finished: 2022-06-11
links:
- https://en.wikipedia.org/wiki/Sourcery
---

