---
titles:
- Turning Red
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2022
  artists:
  - Pixar Animation Studios
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-03-15
    finished: 2022-03-15
links:
- https://en.wikipedia.org/wiki/Turning_Red
---

