---
titles:
- The Peripheral S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - speculative fiction
  - science fiction
  - post-apocalypse
  - time travel
  - virtual reality
  - 2160p
  - 16:9
  - drama
  - thriller
  years:
  - 2022
  places:
  - London
  - North Carolina
  provenances:
  - United States
dates:
  published: 2022-11-21
  updated: 2022-12-02
  experienced:
  - started: 2022-11-21
    finished: 2022-12-02
links:
- https://en.wikipedia.org/wiki/The_Peripheral_(TV_series)
---
