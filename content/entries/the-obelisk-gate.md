---
titles:
- The Obelisk Gate
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2016
  artists:
  - N.K. Jemisin
collections:
- name: The Broken Earth
  number: 2
dates:
  published: 2020-07-18
  updated: 2021-07-30
  experienced:
  - started: 2020-07-18
    finished: 2020-07-20
links:
- https://en.wikipedia.org/wiki/The_Obelisk_Gate
---
