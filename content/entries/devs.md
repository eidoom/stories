---
titles:
- Devs
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - psychological thriller
  - drama
  - fiction
  - thriller
  - science fiction
  - speculative fiction
  - miniseries
  years:
  - 2020
dates:
  published: 2020-12-05
  updated: 2021-02-20
  experienced:
  - started: 2020-07-18
    finished: 2020-07-25
description: Deus
links:
- https://en.wikipedia.org/wiki/Devs
---

An entertaining thought experiment and engaging drama around the psychological effects of prescience and the conundrum of free will in a deterministic universe.
