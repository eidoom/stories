---
titles:
- A Feast for Crows
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2005
  artists:
  - George R. R. Martin
collections:
- name: A Song of Ice and Fire
  number: 4
- name: Game of Thrones
  number: 4
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
