---
titles:
- 'The Witcher: Nightmare of the Wolf'
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - dark
  - fantasy
  - anime
  - animated
  - origin
  - prequel
  years:
  - 2021
collections:
- name: The Witcher (TV)
  number: 0
- name: The Witcher
  number: 0
dates:
  published: 2021-09-05
  updated: 2021-09-05
  experienced:
  - started: 2021-09-05
    finished: 2021-09-05
description: Vesemir’s story
links:
- https://en.wikipedia.org/wiki/The_Witcher%3A_Nightmare_of_the_Wolf
---

