---
titles:
- Wednesday S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - comedy horror
  - coming-of-age
  - supernatural
  years:
  - 2022
  artists:
  - Alfred Gough
  - Chris Bacon
  - Christina Ricci
  - Danny Elfman
  - Emma Myers
  - Georgie Farmer
  - Gwendoline Christie
  - Hunter Doohan
  - Jamie McShane
  - Jenna Ortega
  - Joy Sunday
  - Miles Millar
  - Moosa Mostafa
  - Naomi J. Ogawa
  - Percy Hynes White
  - Riki Lindhome
collections:
- name: The Addams Family
  number: 20
dates:
  published: 2023-02-10
  updated: 2023-02-10
  experienced:
  - started: 2023-02-10
    finished: 2023-02-11
links:
- https://en.wikipedia.org/wiki/Wednesday_(TV_series)
---
