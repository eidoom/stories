---
titles:
- Hearts of Ice
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  years:
  - 2019
  artists:
  - David Hair
collections:
- name: Sunsurge Quartet
  number: 3
- name: Mage’s Blood
  number: 7
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://davidhairauthor.com/Books/The-Sunsurge-Quartet/Hearts-of-Ice
---
