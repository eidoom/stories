---
titles:
- This War of Mine
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2014
  artists:
  - 11 bit studios
dates:
  published: 2022-05-10
  updated: 2022-05-10
  experienced:
  - started: 2022-05-09
links:
- https://en.wikipedia.org/wiki/This_War_of_Mine
---

