---
titles:
- 'XCOM: Enemy Within'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2012
  - 2013
collections:
- name: XCOM
  number: 7
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2015-09-12
completed: true
links:
- https://en.wikipedia.org/wiki/XCOM:_Enemy_Unknown
---

Expansion of *XCOM: Enemy Unknown*.
