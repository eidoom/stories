---
titles:
- 'Star Trek: Discovery S05'
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - speculative fiction
  - science fiction
  - action
  - adventure
  - drama
  - space
  - future
  - aliens
  years:
  - unreleased
collections:
- name: 'Star Trek: Discovery'
  number: 5
- name: Star Trek
  number: 7.5
dates:
  published: 2022-11-01
  updated: 2022-11-01
#  experienced:
#  - started: 2021-11-18
#    finished: 2022-03-19
links:
- https://en.wikipedia.org/wiki/Star_Trek:_Discovery_(season_5)
---
