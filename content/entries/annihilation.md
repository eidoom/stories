---
titles:
- Annihilation
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  years:
  - 2018
  artists:
  - Catherynne M. Valente
collections:
- name: 'Mass Effect: Andromeda'
  number: 3
- name: Mass Effect
  number: 3.5
dates:
  published: 2019-12-27
  updated: 2021-07-30
  experienced:
  - started: 2020-06-09
    finished: 2020-06-14
links:
- https://en.wikipedia.org/wiki/Mass_Effect:_Andromeda_(book_series)#Annihilation
---
