---
titles:
- The Jargon File
- The Hacker's Dictionary
taxonomies:
  media:
  - web
  formats:
  - usage dictionary
  - website
  years:
  - 1975
dates:
  published: 2022-12-31
  updated: 2022-12-31
  experienced:
  - '?'
links:
- https://www.catb.org/jargon/html/index.html
- https://en.wikipedia.org/wiki/Jargon_File
---
