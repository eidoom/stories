---
titles:
- Salvation
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  years:
  - 2018
  artists:
  - Peter F. Hamilton
collections:
- name: Salvation Sequence
  number: 1
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---

