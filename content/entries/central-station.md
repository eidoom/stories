---
titles:
- Central Station
- Central do Brasil
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - drama
  - subtitled
  years:
  - 1998
  languages:
  - Portuguese
dates:
  published: 2021-09-19
  updated: 2021-09-19
  experienced:
  - started: 2021-09-16
    finished: 2021-09-16
links:
- https://en.wikipedia.org/wiki/Central_Station_%28film%29
---

