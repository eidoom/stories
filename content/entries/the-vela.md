---
titles:
- The Vela
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  years:
  - 2019
  artists:
  - Yoon Ha Lee
  - Rivers Solomon
  - S.L. Huang
  - Becky Chambers
collections:
- name: The Vela
  number: 1
dates:
  published: 2022-08-23
  updated: 2023-04-12
  experienced:
  - started: 2023-04-08
    finished: 2023-04-12
---

