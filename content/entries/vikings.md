---
titles:
- Vikings
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - historical
  - drama
  - action
  - adventure
  years:
  - 2013
dates:
  published: 2020-12-05
  updated: 2021-02-07
  experienced:
  - started: 2018-11-12
    finished: 2021-01-30
description: The legacy of Ragnar Lothbrok
links:
- https://en.wikipedia.org/wiki/Vikings_(2013_TV_series)
---

This series tells the saga of the [Viking](https://en.wikipedia.org/wiki/Viking) [Ragnar Lothbrok](https://en.wikipedia.org/wiki/Ragnar_Lothbrok) and his sons, inspired by real-world legend and history.
It’s a good drama, at times great.
