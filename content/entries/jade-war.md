---
titles:
- Jade War
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  years:
  - 2019
  artists:
  - Fonda Lee
collections:
- name: The Green Bone Saga
  number: 2
dates:
  published: 2022-08-23
  updated: 2022-08-23
  experienced:
  - started: 2022-09-12
    finished: 2022-10-02
links:
- https://en.wikipedia.org/wiki/Jade_War
---

