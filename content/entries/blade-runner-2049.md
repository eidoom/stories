---
titles:
- Blade Runner 2049
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2017
collections:
- name: Blade Runner
  number: 2
dates:
  published: 2021-10-25
  updated: 2021-10-25
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Blade_Runner_2049
---

