---
titles:
- The First Tree
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - story
  - walking simulator
  - exploration
  - arthouse
  - indie
  years:
  - 2017
dates:
  published: 2021-05-09
  updated: 2021-05-09
  experienced:
  - started: 2021-05-08
    finished: 2021-05-09
description: and the fox’s journey
links:
- https://www.thefirsttree.com/
---

A touching wee story about a man’s relationship with his recently deceased father, narrated over a dream about a fox searching for her lost cubs.
