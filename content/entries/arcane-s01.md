---
titles:
- Arcane S01
taxonomies:
  media:
  - video
  formats:
  - television
  years:
  - 2021
collections:
- name: League of Legends
  number: 10
- name: Arcane
  number: 1
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2021-11-29
    finished: 2021-12-01
links:
- https://en.wikipedia.org/wiki/Arcane_(TV_series)
---

