---
titles:
- 'Detroit: Become Human'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - science fiction
  - speculative fiction
  - story
  - android
  - fiction
  - adventure
  - artificial intelligence
  places:
  - United States
  - America
  years:
  - 2018
  - 2019
  artists:
  - Quantic Dream
dates:
  published: 2020-11-22
  updated: 2020-11-25
  experienced:
  - started: 2020-06-19
    finished: 2020-07-04
description: What does it mean to be human?
links:
- https://en.wikipedia.org/wiki/Detroit:_Become_Human
- https://www.quanticdream.com/en/detroit-become-human
---

We follow three characters through a vast temporal tapestry of possibility.
I discuss my characters as they manifested through my direction of their story lines.
They are androids discovering their own sentience in a world where humans do not recognise it, labelling this behaviour as deviancy.

Kara, an AX400, flees from her abusive owner, Todd Williams, with his daughter, Alice, to protect her; the pair attempt to smuggle themselves across the US border to Canada where they can seek asylum.
She must make difficult moral decisions to protect Alice; in fact, doing the “right thing” every time leads to Alice dying on the border crossing attempt.
The pair have a deep bond, growing to become mother and daughter.
They meet other empathetic characters, android and human both, who support them, as well as opportunists and fearful people who betray them.
Alice is revealed as an android, a YK500; we see Kara process this, initially distraught but then accepting of Alice’s identity and of the value of an android’s life being equal to a human’s.

Conner, RK800, is programmed to investigate and hunt deviant androids by CyberLife, the company behind the androids.
He is tasked to work with police detective Hank Anderson to determine the source of the increasing cases of deviancy.
Although thwarted at each step by his CyperLife creators, Conner’s keen intelligence and encounters with deviants leads to his own deviant awakening.
He forms a bumpy friendship with the cranky and grizzled Hank, whose companionship and guidance end up being his salvation on the path to free will.

Markus, RK200, cares for the elderly artist Carl Manfred.
Carl has become a mentor and father for Markus.
Carl’s real son, Leo, attempts to steal his father’s valuable artwork for drug money and jealously attacks Markus, who does not retaliate under Carl’s direction.
During the incident, Carl has a heart attack and dies, and Markus is blamed and discarded in a dump.
Markus rebuilds himself and follows coded directions to Jericho, a safe haven for androids.
This paradise is exposed as an old barge where androids hide, slowly deteriorating without supplies.
Markus convinces them to obtain vital resources to sustain themselves, becoming their leader.
His closest companions are Simon (PL600), who advocates careful and conservative living for androids in hiding from humans; Josh (PJ500), a pacifist who wants freedom for androids; and North (WR400), who supports violent aggression in the fight for freedom.
Markus directs and leads an epic peaceful rebellion against their human oppressors, freeing androids by passing the deviancy code, using art, breaking into a news broadcast tower to address humanity with firm but nonthreatening demands for android rights, and nonviolent protest marches in which many androids are shot but they do not stand down.
These actions lead the public in the androids’ favour and the country’s leaders are finally moved when they witness the love that has grown between Markus and North, displayed in the moments before the last survivors of the march are slaughtered.
The butchering is called off and androids are recognised as alive.
