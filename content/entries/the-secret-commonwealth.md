---
titles:
- The Secret Commonwealth
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  years:
  - 2019
  artists:
  - Philip Pullman
collections:
- name: The Book of Dust
  number: 2
- name: Northern Lights
  number: 4
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - started: 2019-12-28
    finished: 2019-12-31
links:
- https://en.wikipedia.org/wiki/The_Secret_Commonwealth
---
