---
titles:
- Brave New World
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1932
  artists:
  - Aldous Huxley
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
