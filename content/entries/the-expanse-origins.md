---
titles:
- The Expanse Origins
taxonomies:
  media:
  - book
  formats:
  - comic
  years:
  - 2017
collections:
- name: The Expanse
  number: 100
dates:
  published: 2022-05-05
  updated: 2022-05-05
  experienced:
  - '?'
links:
- https://expanse.fandom.com/wiki/The_Expanse_Origins
---

