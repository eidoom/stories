---
titles:
- Planet of Exile
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - speculative fiction
  - science fiction
  - fiction
  - far future
  - fantasy
  years:
  - 1966
  artists:
  - Ursula K. Le Guin
collections:
- name: Hainish Cycle
  number: 2
- name: The Ekumen
  number: 4
dates:
  published: 2021-06-02
  updated: 2021-06-08
  experienced:
  - started: 2021-06-02
    finished: 2021-06-05
description: Going native
links:
- https://en.wikipedia.org/wiki/Planet_of_Exile
---

An intriguing primitive humanoid society bound by their climate, sharing curious interactions with stranded interlopers.

In the cycle, we establish Werel and that the Enemy has engaged in the War.
