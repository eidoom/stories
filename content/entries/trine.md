---
titles:
- 'Trine: Enchanted Edition'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2009
  - 2014
  artists:
  - Frozenbyte
collections:
- name: Trine
  number: 1
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-04-15
completed: true
links:
- https://en.wikipedia.org/wiki/Trine_(video_game)
---

