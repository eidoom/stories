---
titles:
- Five Ways to Forgiveness
taxonomies:
  media:
  - book
  formats:
  - collection
  - short story
  - novella
  genres:
  - anthropological
  - speculative fiction
  - science fiction
  - fiction
  years:
  - 1995
  - 2017
  artists:
  - Ursula K. Le Guin
collections:
- name: Hainish Cycle
  number: 7
- name: The Ekumen
  number: 7
dates:
  published: 2021-07-01
  updated: 2021-07-10
  experienced:
  - started: 2021-07-01
    finished: 2021-07-10
description: Liberation
links:
- https://en.wikipedia.org/wiki/Four_Ways_to_Forgiveness
---

Set in a system that the Ekumen has just made contact with.
The primary planet is called Werel (not the same one as [before](planet-of-exile.html), the translation being World), with dominant nation Voe Deo.
There is also Yeowe, a recently liberated slave colony planet.
After the violent rebellion, Yeowe descended into tribal power struggles for may years before unifying under a constitution.
The people of this system are not strangers to corporate capitalism, unscrupulous political intrigue, and savage war.
Worse is the misogyny and slavery that structures their society.

{{ short(title="Betrayals",years=["1995"],started="2021-07-01",finished="2021-07-04") }}

The paths cross of two very different elderly people living their retirements in desolate isolation on Yeowe: Yoss, who was a physics teacher; and Abberkam, the disgraced former leader of the World Party, who fought for the liberation.

{{ short(title="Forgiveness Day",years=["1995"],started="2021-07-04",finished="2021-07-06") }}

We follow Solly, the young first Ekumenical Envoy to Gatay of Werel.
She struggles as a woman to cope with this.
We also meet Rega Teyeo, her local bodyguard, a man of supreme honour.
He is a veot, the warrior caste of Owners, and friend of Esdardon Aya.
They learn to understand one another despite the gulf of culture.

Hilarious mutual misunderstandings of cultural cues abound, as is stock Le Guin.

{{ short(title="A Man of the People",years=["1995"],link="https://en.wikipedia.org/wiki/A_Man_of_the_People_(short_story)",started="2021-07-06",finished="2021-07-09") }}

After a tantalising glimpse of the highly ritualised society of Hain, we follow the troubled Yehedarhed Havzhiva as he becomes the Ekumenical Sub-Envoy (under Solly) to Yeowe at Yotebber City.
The brutally treated ex-slaves there have developed an extremely patriarchal culture.
Isolationists attack him and he befriends his nurse, Dr. Yeron, a member of the Liberation Movement who fight for women’s rights.
While we only see his observations, including a visit to a rural village where an appalling tribal hierarchy persists involving treatment of women as property, over time Havzhiva does what he can to support the women in gaining equality.

{{ short(title="A Woman's Liberation",years=["1995","2001"],started="2021-07-09") }}

Rakam tells the story of her life: born a slave on a plantation in Voe Deo, serving as a houseperson, manumitted by a well-meaning Owner; captured and made a usewoman, escaping to the city; learning from books at the library, befriending Dr. Yeron and Esdardon Aya, speaking out politically, fleeing to liberated Yeowe; thrust into a sexist tribe, earning passage to Yotebber City, teaching, befriending Yehedarhed Havzhiva, lending her voice to the Liberation Movement for women, lecturing at Yeowe University and editing the press there, partnering with Yehedarhed, seeing woman gain legal equality.

{{ short(title="Old Music and the Slave Women",years=["1999","2017"],started="2021-07-10",link="https://en.wikipedia.org/wiki/Old_Music_and_the_Slave_Women") }}

Esdardon Aya, meaning “Old Music”, is the chief intelligence officer at the Ekumenical embassy in Voe Deo.
During the liberation of Werel, he attempts to assist the Liberation but is captured by a Legitimate faction and imprisoned at a plantation where he is tortured. 
He befriends some of the few remaining bondswomen there.
An unfriendly Liberation faction overpowers the plantation, then they are later killed in another raid.
Esdardon Aya and his friends await the Liberation.
