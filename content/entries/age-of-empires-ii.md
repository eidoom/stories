---
titles:
- Age of Empires II
- 'Age of Empires II: HD Edition'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 1999
  - 2013
collections:
- name: Age of Empires
  number: 2
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2018-11-22
links:
- https://en.wikipedia.org/wiki/Age_of_Empires_II#HD_Edition
---

Including the HD edition expansions:
* *The Forgotten*
* *The African Kingdoms*
* *Rise of the Rajas*
