---
titles:
- Towers of Midnight
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2010
  artists:
  - Robert Jordan
  - Brandon Sanderson
collections:
- name: The Wheel of Time
  number: 13
- name: The Wheel of Time universe
  number: 13
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
