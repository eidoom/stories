---
titles:
- Excession
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1996
  artists:
  - Iain M. Banks
collections:
- name: Culture
  number: 5
- name: The Culture
  number: 2
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Excession
---
