---
titles:
- Caliban’s War
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2012
  artists:
  - James S. A. Corey
collections:
- name: The Expanse
  number: 2
- name: The Expanse (books)
  number: 2
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Caliban%27s_War
---
