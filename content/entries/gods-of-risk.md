---
titles:
- Gods of Risk
taxonomies:
  media:
  - book
  formats:
  - novella
  years:
  - 2012
  artists:
  - James S. A. Corey
collections:
- name: The Expanse
  number: 2.5
- name: The Expanse (books)
  number: 2.5
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
