---
titles:
- Eragon
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2002
  artists:
  - Christopher Paolini
collections:
- name: The Inheritance Cycle
  number: 1
dates:
  published: 2019-11-25
  updated: 2021-07-30
  experienced:
  - '?'
---
