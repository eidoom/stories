---
titles:
- 'Star Wars: Episode IX – The Rise of Skywalker'
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - epic
  - science fiction
  - fantasy
  - speculative fiction
  - space opera
  - space
  - fiction
  years:
  - 2019
collections:
- name: Skywalker saga
  number: 9
- name: Star Wars sequel trilogy
  number: 3
- name: Star Wars
  number: 9
dates:
  published: 2020-11-29
  updated: 2021-02-17
  experienced:
  - started: 2020-11-29
    finished: 2020-11-29
description: It’s always Palpatine
links:
- https://en.wikipedia.org/wiki/Star_Wars:_The_Rise_of_Skywalker
---

