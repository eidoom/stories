---
titles:
- Alien
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - science fiction
  - horror
  - speculative fiction
  - fiction
  - aliens
  - thriller
  - mystery
  years:
  - 1979
  artists:
  - Ridley Scott
collections:
- name: Alien original series
  number: 1
- name: Alien
  number: 3
dates:
  published: 2021-01-17
  updated: 2021-01-23
  experienced:
  - started: 2021-01-17
    finished: 2021-01-17
description: A film for cat lovers
links:
- https://en.wikipedia.org/wiki/Alien_(film)
---

