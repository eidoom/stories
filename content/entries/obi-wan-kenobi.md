---
titles:
- Obi-Wan Kenobi
taxonomies:
  media:
  - video
  formats:
  - television
  years:
  - 2022
collections:
- name: Star Wars
  number: 3.5
dates:
  published: 2022-05-21
  updated: 2022-05-21
  experienced:
  - started: 2022-05-31
    finished: 2022-05-30
links:
- https://en.wikipedia.org/wiki/Obi-Wan_Kenobi_(TV_series)
---

