---
titles:
- Neuromancer
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - cyberpunk
  years:
  - 1984
  artists:
  - William Gibson
collections:
- name: The Sprawl
  number: 1
dates:
  published: 2022-12-02
  updated: 2022-12-31
  experienced:
  - started: 2022-12-10
    finished: 2022-12-31
links:
- https://en.wikipedia.org/wiki/Neuromancer
---
