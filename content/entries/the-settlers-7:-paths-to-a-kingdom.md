---
titles:
- 'The Settlers 7: Paths to a Kingdom'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2010
  artists:
  - Blue Byte
collections:
- name: The Settlers
  number: 7
dates:
  published: 2022-05-05
  updated: 2022-05-05
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Settlers_7:_Paths_to_a_Kingdom
---

