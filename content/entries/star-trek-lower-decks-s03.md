---
titles:
- 'Star Trek: Lower Decks S03'
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - science fiction
  - space
  - animated
  - cartoon
  - comedy
  years:
  - 2022
collections:
- name: Star Trek
  number: 6.3
- name: 'Star Trek: Lower Decks'
  number: 3
dates:
  published: 2022-11-01
  updated: 2022-11-01
  experienced:
  - started: 2022-11-01
    finished: 2022-11-02
links:
- https://en.wikipedia.org/wiki/Star_Trek:_Lower_Decks_(season_3)
---
