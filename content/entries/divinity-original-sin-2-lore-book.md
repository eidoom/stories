---
titles:
- 'Divinity Original Sin 2: Lore Book'
taxonomies:
  media:
  - book
  years:
  - 2017
  formats:
  - lorebook
  - sourcebook
collections:
- name: 'Divinity: Original Sin'
  number: 2.1
dates:
  published: 2023-03-19
  updated: 2023-03-19
  experienced:
  - started: 2023-03-19
    finished: 2023-03-19
---
