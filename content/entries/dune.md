---
titles:
- Dune
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1965
  artists:
  - Frank Herbert
collections:
- name: Dune Chronicles
  number: 1
- name: Gollancz 50
  number: 5
- name: Dune
  number: 1
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Dune_(novel)
---
