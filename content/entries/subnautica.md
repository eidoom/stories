---
titles:
- Subnautica
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - exploration
  - story
  - crafting
  - survival
  - open world
  - fiction
  - science fiction
  - speculative fiction
  years:
  - 2018
  artists:
  - Unknown Worlds Entertainment
collections:
- name: Subnautica
  number: 1
dates:
  published: 2020-10-08
  updated: 2020-10-08
  experienced:
  - started: 2019-02-11
description: 'Thalassophobia: the game'
completed: true
links:
- https://en.wikipedia.org/wiki/Subnautica
- https://unknownworlds.com/subnautica/
---

This game is beautiful. 
I love swimming around the various biomes and studying the alien fauna.
Uncovering the secrets of this world was captivating.
