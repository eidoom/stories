---
titles:
- Transition
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2009
  artists:
  - Iain M. Banks
dates:
  published: 2020-03-05
  updated: 2021-07-30
links:
- https://en.wikipedia.org/wiki/Transition_(novel)
---
