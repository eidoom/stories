---
titles:
- Surviving Mars
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2018
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2019-05-19
completed: true
links:
- https://en.wikipedia.org/wiki/Surviving_Mars
---

