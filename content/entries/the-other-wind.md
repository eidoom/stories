---
titles:
- The Other Wind
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2001
  artists:
  - Ursula K. Le Guin
collections:
- name: The Books of Earthsea
  number: 6
- name: Earthsea
  number: 8
dates:
  published: 2022-04-19
  updated: 2022-05-02
  experienced:
  - started: 2022-05-02
    finished: 2022-05-02
links:
- https://en.wikipedia.org/wiki/The_Other_Wind
---

