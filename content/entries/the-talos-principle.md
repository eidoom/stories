---
titles:
- The Talos Principle
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2014
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2015-09-08
completed: true
links:
- https://en.wikipedia.org/wiki/The_Talos_Principle
---

