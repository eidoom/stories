---
titles:
- Frozen Planet II
taxonomies:
  media:
  - video
  formats:
  - television
  - documentary
  genres:
  - nature
  years:
  - 2022
  artists:
  - David Attenborough
dates:
  published: 2022-11-01
  updated: 2023-01-16
  experienced:
  - started: 2023-01-15
    finished: 2023-01-19
links:
- https://en.wikipedia.org/wiki/Frozen_Planet_II
---
