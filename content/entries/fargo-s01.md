---
titles:
- Fargo S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - black comedy
  - crime
  - anthology
  - thriller
  - drama
  years:
  - 2014
collections:
- name: Fargo
  number: 1
dates:
  published: 2021-01-06
  updated: 2022-08-28
  experienced:
  - started: 2017-10-01
completed: true
links:
- https://en.wikipedia.org/wiki/Fargo_(season_1)
---
