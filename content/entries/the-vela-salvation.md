---
titles:
- "The Vela: Salvation"
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  years:
  - 2020
  artists:
  - Ashley Poston
  - Maura Milan
  - Nicole Givens Kurtz
  - Sangu Mandanna
collections:
- name: The Vela
  number: 2
dates:
  published: 2023-04-12
  updated: 2023-04-12
#  experienced:
#  - started: 2023-04-12
#    finished: 2023-04-12
#active: true
---

