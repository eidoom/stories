---
titles:
- Winter’s Heart
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2000
  artists:
  - Robert Jordan
collections:
- name: The Wheel of Time
  number: 9
- name: The Wheel of Time universe
  number: 9
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
