---
titles:
- The Dark Defiles
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2014
  artists:
  - Richard K. Morgan
collections:
- name: A Land Fit for Heroes
  number: 3
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
