---
titles:
- How Long ’til Black Future Month
taxonomies:
  media:
  - book
  formats:
  - collection
  - short story
  genres:
  - fantasy
  - speculative fiction
  - fiction
  years:
  - 2018
  artists:
  - N. K. Jemisin
dates:
  published: 2020-10-12
  updated: 2020-10-12
  experienced:
  - started: 2020-09-09
    finished: 2020-09-21
description: The sketchpad of an incredible author
links:
- https://en.wikipedia.org/wiki/How_Long_%27til_Black_Future_Month%3F
---

A great collection of fantasy and sci-fi short stories.
They are diverse and test out wonderfully imagined, ambitious ideas.
