---
titles:
- A Thousand Splendid Suns
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2007
  artists:
  - Khaled Hosseini
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
