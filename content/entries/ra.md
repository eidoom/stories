---
titles:
- Ra
taxonomies:
  media:
  - book
  - web
  formats:
  - serial
  genres:
  - science fiction
  - magic
  - speculative fiction
  - fiction
  years:
  - 2011
  artists:
  - Sam Hughes
dates:
  published: 2021-02-21
  updated: 2021-02-21
  experienced:
  - started: 2021-02-20
    finished: 2021-02-26
description: Physics, Chemistry, Biology, and Magic
links:
- https://qntm.org/ra
---

My first experience of a [web serial](https://en.wikipedia.org/wiki/Web_fiction#Web_serial); I enjoyed seeing the author’s comments, appendices, and discarded draft ending.

The concept of magic as just another boring old field of science is great.
Uncovering the lore of this world, we discover it’s a [Clarke’s third law](https://en.wikipedia.org/wiki/Clarke’s_three_laws) flavoured magic, which is also great.
The magic system is full of programming references too.

The story follows sisters Laura and Natalie Ferno as they master magic and discover that life on Earth in the present day is a recreation of the world after Actual Humanity (physical humans) very nearly lost a war against Virtual Humanity involving an endgame technological wish-fulfillment system called Ra in the real far future. The Virtuals want to dedicate the entire energy-mass budget of the solar system to their hardware—it’s an age-old resource war, and, of course, it’s not over yet.
