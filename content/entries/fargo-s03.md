---
titles:
- Fargo S03
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - black comedy
  - crime
  - anthology
  - thriller
  - drama
  years:
  - 2017
collections:
- name: Fargo
  number: 3
dates:
  published: 2021-01-06
  updated: 2022-08-28
  experienced:
  - "?"
completed: true
links:
- https://en.wikipedia.org/wiki/Fargo_(season_3)
---
