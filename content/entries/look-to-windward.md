---
titles:
- Look to Windward
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - science fiction
  years:
  - 2000
  artists:
  - Iain M. Banks
collections:
- name: Culture
  number: 7
- name: The Culture
  number: 7
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - started: 2019-06-05
    finished: 2019-06-29
links:
- https://en.wikipedia.org/wiki/Look_to_Windward
---
