---
titles:
- The Shadow of the Wind
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2001
  artists:
  - Carlos Ruiz Zafón
  - Lucia Graves
collections:
- name: The Cemetery of Forgotten Books
  number: 1
dates:
  published: 2019-07-01
  updated: 2021-07-30
  experienced:
  - '?'
---
