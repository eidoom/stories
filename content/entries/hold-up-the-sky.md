---
titles:
- Hold Up the Sky
taxonomies:
  media:
  - book
  formats:
  - short story
  - collection
  genres:
  - speculative fiction
  - science fiction
  - hard sci-fi
  - fiction
  places:
  - China
  years:
  - 2020
  artists:
  - Liu Cixin
  - Adam Lanphier
  - Joel Martinsen
  - John Chu
  - Carmen Yiling Yan
dates:
  published: 2020-11-29
  updated: 2021-02-20
  experienced:
  - started: 2020-12-12
    finished: 2020-12-30
description: A second collection of Liu Cixin’s translated short stories
links:
- https://en.wikipedia.org/wiki/Liu_Cixin#Works_of_short_fiction
---

{{ short(title="The Village Teacher", alt="乡村教师", year=2000) }}

The purest, most wholesome teacher pours his entire life into a Chinese rural mountain village primary school despite the resistance of the rest of the village, who only value manual labour and money.
Dying of cancer that he can’t afford to treat because he has spent his every last penny on the school, with his last breaths he teaches Newton’s three Laws of Motion to the children.

Meanwhile, the carbon-based galactic civilisations are sealing off the silicon-based civilisations at the end of a lengthy war, destroying solar systems to create an unpassable rift in the galactic arm.
They randomly sample lifeforms to test for intelligence on inhabited worlds; the children’s knowledge of Newtonian mechanics convince them that humanity is worth preserving.

{{ short(title="The Time Migration", alt="时间移民", year=2014) }}

A group of people are cryogenically preserved to migrate to the future.
Their leader periodically emerges to see if they’ve reached an appropriate time to live in, but is put back to sleep when the future cannot support them or when she decides it’s not a future she wants them to live in.
Humanity eventually sublimates into virtual reality and nature reclaims the planet, leaving an Eden for the travellers at the end of their journey through time.

{{ short(title="2018-04-01", alt="2018年4月1日", year=2009) }}

Longevity treatments agitate the state of social divide in the world.
Meanwhile, the Internet evolves into a place where people can join virtual countries, which start demanding political status in the physical world.
Denied this request, they retaliate by tearing down the digital financial infrastruture.
Amongst this tumult, a man steals funds from his employers to pay for his Gene Extension and reflects on his new longevity.

{{ short(title="Fire in the Earth", alt="地火", year=2000) }}

The son of a coal miner is commanded to never enter the mine by his dying father, whose lungs have failed due to coal dust exposure.
The son goes to study and eventually returns to the mine to test an experimental coal liquefaction technique that will transform the coal mining industry, obsoleting the job of the miner.
The process goes out of hand and starts a decades-long subterranean fire that destroys the local area and town.

In the future, a group of school children visit a coal mine museum and marvel at how hard life was in the past—and how stupid people were.

{{ short(title="Contraction", alt="坍缩", year=1999) }}

Ding Yi solves the Theory of Everything and predicts the universe will soon start contracting, and when it does, time will reverse.
He explains this to an audeince in suitably melodramatic form around an hour before the bounce occurs.

{{ short(title="Mirror", alt="镜子", year=2004, desc="A thrilling reflection on the double-edged sword of the surveillance state") }}

Politician Song Cheng attempts to reveal the corruption he has uncovered within the Chinese Communist Party, but is imprisoned on false pretenses by his superior, the Senior Officer, to stop him.
Meanwhile, engineer Bai Bing has created the “Digital Mirror”, a perfect simulation of the universe, and attempted to use this omnicience to blackmail money from corrupt politicians.
When this plot goes askew, Bing runs to the only trustworthy politician: the righteous Cheng.

Cheng wants to use this technology to clean society, eradicating privacy to create a pure and uncorrupted world.
However, the Digital Mirror predicts this will bring around a state of stagnation, eventually leading to humanity’s nadir.
Cheng and Bing destroy the Mirror, but shortly afterwards it is independently reinvented in America, sealing the fate of the future.

{{ short(title="Ode to Joy", alt="欢乐颂", year=2005, desc="A shared sense of wonder reunites the globe") }}

A sentient astronomical mirror visits our solar system to perform a concert using the Sun as its instrument.
At the final meeting of the UN, the leaders of the world are [moved](https://en.wikipedia.org/wiki/sense_of_wonder) by this spectacle to reinstate the UN.

{{ short(title="Full-Spectrum Barrage Jamming", alt="全频带阻塞干扰", year=2001) }}

When Russia declares a new communist regime in the near future, NATO initiates a war against them.
Its conclusion is decided by three people: Marshal Levchenko, commander-in-chief of the Russian army; his son, astophysicist Misha, aboard a space station; and Misha’s girlfriend, Major Dr. Kalina, electronic-warfare specialist. 

NATO has the edge on electronic-warface, so Russia level the playing field by reducing both sides to pre-radio capabilities with full-spectrum barrage jamming.
In a last-ditch effort to sustain this state, Misha rams his space station into the sun at the right spot to instigate a coroncal mass ejection, causing blanket EM interference in the Earth’s atmosphere.

{{ short(title="Sea of Dreams", alt="梦之海", year=2017) }}

A highly-advanced alien sentience, the Low-Temperature Artist, visits the Earth.
It creates a ring of ice around the planet, depleting the oceans, as art.
Of all humanity, it will only communicate with an ice sculptor, Yan Dong, who it views as a colleague.
She is unable to dissuade it from its work, and life on Earth starts to die without water.

Years after the Artist’s departure, humanity is able to complete a massive engineering project to knock the ice cubes back to the ground.
Inspired by the alien encounter and moreover this success, they start planning the ambitious project of transporting more water from Jupiter’s and Saturn’s satellites, their gaze turning towards the stars.

{{ short(title="Cloud of Poems", alt="诗云", year=2003, uni="Dinosaur Universe", num=3, rest=[1,"../the-wandering-earth#of-ants-and-dinosaurs",2,"../the-wandering-earth#devourer"]) }}

On their departure from the Solar System, the dinosaurs about-turn on detecting the arrival of a “god”—a being of vast technological superiority compared to the Devouring Empire.
The dinosaur emissary Bigtooth brings from their human livestock a poet, Yi Yi, as a gift to the god.
Yi Yi challenges the god to best the poetry of [Li Bai](https://en.wikipedia.org/wiki/Li_Bai).

The god takes human form and Li Bai’s name, partaking in human experience for months in an effort to do so, but it eventually admits it cannot.
Instead, it builds a quantum computer to brute-force produce every possible Chinese poem, the eponymous storage device of which consumes all mass in the Solar System, including the Devouring Empire.
To honour the species who created such poetry, the god constructs a planet-sized spherical shell and deposits the Earth’s land and seas on the inside, creating a new home for humanity.

“Li Bai” must admit defeat even after this great undertaking because it cannot write software to identify good poetry within the database, so although it has been written within the Cloud of Poems, it cannot be found.

{{ short(title="The Thinker", alt="思想者", year=2003) }}

A brain surgeon, the Doctor, meets the Astronomer by chance at an observatory on Mount Siyun.
The Astronomer tell the Doctor of the particular twinkling of stars she is studying.
They meet again by chance at the same place around ten years later; the Doctor notices that Alpha Centauri A now shows the same waveform that the Sun emitted at their last meeting.
The Astronomer is astonished at this coincidence and they agree to meet again in seven years when Sirius, the next closest observable star, could have received and transmitted this signal too.
It doesn’t disappoint.
Finally, they meet again seventeen years later when Altair again confirms the observation.
They speculate that the stars are like neurons in an astronomical brain.
