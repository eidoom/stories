---
titles:
- Severance S02
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - psychological thriller
  - thriller
  - psychological
  - drama
  - dystopian
  - mystery
  - speculative fiction
  - science fiction
  years:
  - unreleased
  artists:
  - 'Adam Scott'
  - 'Aoife McArdle'
  - 'Ben Stiller'
  - 'Britt Lower'
  - 'Christopher Walken'
  - 'Dichen Lachman'
  - 'Jen Tullock'
  - 'John Turturro'
  - 'Michael Chernus'
  - 'Patricia Arquette'
  - 'Tramell Tillman'
  - 'Zach Cherry'
  provenances:
  - United States
collections:
- name: Severance
  number: 2
dates:
  published: 2023-03-29
  updated: 2023-03-29
#  experienced:
#  - started: 2023-01-06
#    finished: 2021-01-07
#links:
#- https://en.wikipedia.org/wiki/Severance_(TV_series)
---
