---
titles:
- Knife of Dreams
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2005
  artists:
  - Robert Jordan
collections:
- name: The Wheel of Time
  number: 11
- name: The Wheel of Time universe
  number: 11
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
