---
titles:
- Woken Furies
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2005
  artists:
  - Richard K. Morgan
collections:
- name: Takeshi Kovacs
  number: 3
- name: Altered Carbon
  number: 3
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Woken_Furies
---
