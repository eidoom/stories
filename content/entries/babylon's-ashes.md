---
titles:
- Babylon’s Ashes
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2016
  artists:
  - James S. A. Corey
collections:
- name: The Expanse
  number: 6
- name: The Expanse (books)
  number: 6
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Babylon%27s_Ashes
---
