---
titles:
- Pharaoh
- Pharaoh Gold
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 1999
  - 2001
  artists:
  - Impressions Games
dates:
  published: 2022-05-05
  updated: 2022-05-05
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Pharaoh_(video_game)
---

Including *Cleopatra: Queen of the Nile* in bundle *Pharaoh Gold*.
