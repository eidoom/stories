---
titles:
- The Handmaiden
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - psychological thriller
  - psychological
  - thriller
  years:
  - 2016
  artists:
  - Park Chan-wook
  places:
  - South Korea
dates:
  published: 2022-09-12
  updated: 2022-09-12
  experienced:
  - '?'
completed: true
links:
- https://en.wikipedia.org/wiki/The_Handmaiden
---
