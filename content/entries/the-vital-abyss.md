---
titles:
- The Vital Abyss
taxonomies:
  media:
  - book
  formats:
  - novella
  years:
  - 2015
  artists:
  - James S. A. Corey
collections:
- name: The Expanse
  number: 5.5
- name: The Expanse (books)
  number: 5.5
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
