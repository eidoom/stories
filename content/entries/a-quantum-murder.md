---
titles:
- A Quantum Murder
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1994
  artists:
  - Peter F. Hamilton
collections:
- name: Greg Mandel
  number: 2
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
