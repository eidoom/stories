---
titles:
- Use of Weapons
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1990
  artists:
  - Iain M. Banks
collections:
- name: Culture
  number: 3
- name: The Culture
  number: 5
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Use_of_Weapons
---
