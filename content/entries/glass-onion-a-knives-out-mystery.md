---
titles:
- 'Glass Onion: A Knives Out Mystery'
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - mystery
  years:
  - 2022
  artists:
  - 'Daniel Craig'
  - 'Dave Bautista'
  - 'Edward Norton'
  - 'Janelle Monáe'
  - 'Jessica Henwick'
  - 'Kate Hudson'
  - 'Kathryn Hahn'
  - 'Leslie Odom Jr.'
  - 'Madelyn Cline'
  - 'Ram Bergman'
  - 'Rian Johnson'
collections:
- name: Knives Out
  number: 2
dates:
  published: 2023-01-29
  updated: 2023-01-29
  experienced:
  - started: 2023-01-29
    finished: 2023-01-29
links:
- https://en.wikipedia.org/wiki/Glass_Onion%3A_A_Knives_Out_Mystery
---
