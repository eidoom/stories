---
titles:
- The Kingdom of Gods
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2011
  artists:
  - N. K. Jemisin
collections:
- name: The Inheritance Trilogy
  number: 3
- name: Inheritance
  number: 3
dates:
  published: 2020-08-02
  updated: 2021-07-30
  experienced:
  - started: 2020-08-11
    finished: 2020-08-19
links:
- https://en.wikipedia.org/wiki/The_Kingdom_of_Gods
---
