---
titles:
- RuneScape
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - mmo
  - rpg
  - fantasy
  years:
  - 2001
dates:
  published: 2021-07-15
  updated: 2021-07-15
  experienced:
  - started: 2007-11-09
links:
- https://en.wikipedia.org/wiki/RuneScape
---

