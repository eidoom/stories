---
titles:
- The Orville S01
taxonomies:
  media:
  - video
  formats:
  - television
  years:
  - 2017
collections:
- name: The Orville
  number: 1
dates:
  published: 2022-06-15
  updated: 2022-08-28
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Orville_(season_1)
---
