---
titles:
- Mass Effect 3
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  years:
  - 2012
collections:
- name: Mass Effect
  number: 3
dates:
  published: 2022-05-07
  updated: 2022-05-07
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Mass_Effect_3
---

