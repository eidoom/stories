---
titles:
- The Abyss Beyond Dreams
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2014
  artists:
  - Peter F. Hamilton
collections:
- name: The Chronicle of the Fallers
  number: 1
- name: Commonwealth universe
  number: 7
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
