---
titles:
- 'Animal Crossing: New Horizons'
taxonomies:
  media:
  - game
  formats:
  - video game
  years:
  - 2020
dates:
  published: 2022-08-24
  updated: 2022-08-24
  experienced:
  - started: 2022-08-23
links:
- https://en.wikipedia.org/wiki/Animal_Crossing#Animal_Crossing:_New_Horizons_(2020)
---

