---
titles:
- Mansions of Madness
- Mansions of Madness Second Edition
taxonomies:
  media:
  - game
  formats:
  - tabletop
  - board game
  - role-playing game
  genres:
  - strategy
  - lovecraft
  - horror
  - coop
  - cooperative
  - adventure
  - mystery
  years:
  - 2011
  - 2016
dates:
  published: 2022-04-19
  updated: 2022-05-06
  experienced:
  - started: 2022-04-14
links:
- https://en.wikipedia.org/wiki/Mansions_of_Madness
- https://boardgamegeek.com/boardgame/205059/mansions-madness-second-edition
---

I played with five players.
