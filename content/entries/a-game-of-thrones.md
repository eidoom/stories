---
titles:
- A Game of Thrones
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1996
  artists:
  - George R. R. Martin
collections:
- name: A Song of Ice and Fire
  number: 1
- name: Game of Thrones
  number: 1
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
