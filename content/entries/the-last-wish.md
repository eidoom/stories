---
titles:
- The Last Wish
taxonomies:
  media:
  - book
  formats:
  - short story
  - collection
  years:
  - 1993
  - 2007
  artists:
  - Andrzej Sapkowski
  - Danusia Stok
collections:
- name: The Witcher
  number: 1
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/The_Last_Wish_(book)
---
