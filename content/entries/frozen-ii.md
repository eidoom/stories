---
titles:
- Frozen II
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2019
  artists:
  - Walt Disney Animation Studios
collections:
- name: Frozen
  number: 1
dates:
  published: 2022-04-19
  updated: 2022-04-19
  experienced:
  - started: 2022-01-24
    finished: 2022-01-24
links:
- https://en.wikipedia.org/wiki/Frozen_II
---

