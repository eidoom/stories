---
titles:
- Empress of the Fall
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  years:
  - 2017
  artists:
  - David Hair
collections:
- name: Sunsurge Quartet
  number: 1
- name: Mage’s Blood
  number: 5
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - started: 2020-05-11
    finished: 2020-06-03
links:
- https://davidhairauthor.com/Books/The-Sunsurge-Quartet/Empress-of-the-Fall
---
