---
titles:
- "Avatar: The Last Airbender – Toph Beifong's Metalbending Academy"
taxonomies:
  media:
  - book
  formats:
  - comic
  - graphic novel
  years:
  - 2021
collections:
- name: 'Avatar: The Last Airbender'
  number: 64.5
dates:
  published: 2023-04-18
  updated: 2023-04-18
#  experienced:
#  - started: 2023-04-18
#    finished: 2023-04-18
---
