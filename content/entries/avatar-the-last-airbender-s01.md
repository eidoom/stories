---
titles:
- 'Avatar: The Last Airbender S01'
- 'Book One: Water'
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - action
  - adventure
  - comedy drama
  - fantasy
  years:
  - 2005
  artists:
  - Benjamin Wynn
  - Bryan Konietzko
  - Dante Basco
  - Dee Bradley Baker
  - Greg Baldwin
  - Grey DeLisle
  - Jack DeSena
  - Jeremy Zuckerman
  - Jessie Flower
  - Mae Whitman
  - Mako
  - Mark Hamill
  - Michael Dante DiMartino
  - Zach Tyler Eisen
  provenances:
  - United States
collections:
- name: 'Avatar: The Last Airbender'
  number: 1
dates:
  published: 2023-03-19
  updated: 2023-03-19
  experienced:
  - started: 2023-03-17
    finished: 2023-03-23
links:
- https://en.wikipedia.org/wiki/Avatar:_The_Last_Airbender
- https://en.wikipedia.org/wiki/Avatar:_The_Last_Airbender_(season_1)
---
