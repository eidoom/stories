---
titles:
- Sword of Destiny
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 1992
  - 2015
  artists:
  - Andrzej Sapkowski
  - David French
collections:
- name: The Witcher
  number: 2
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Sword_of_Destiny
---
