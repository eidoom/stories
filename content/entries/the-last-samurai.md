---
titles:
- The Last Samurai
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - historical
  - action
  - drama
  - japan
  - epic
  - war
  places:
  - America
  - United States
  - Japan
  years:
  - 2003
dates:
  published: 2021-07-06
  updated: 2021-07-06
  experienced:
  - started: 2021-07-06
    finished: 2021-07-06
description: Cue thoughtful flutes
links:
- https://en.wikipedia.org/wiki/The_Last_Samurai
---

