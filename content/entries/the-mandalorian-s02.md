---
titles:
- The Mandalorian S02
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - science fiction
  - fantasy
  - speculative fiction
  - space opera
  - space
  - fiction
  years:
  - 2020
collections:
- name: Star Wars
  number: 6.52
- name: The Mandalorian
  number: 2
- name: The Mandalorian +
  number: 2
dates:
  published: 2020-12-05
  updated: 2021-01-10
  experienced:
  - started: 2020-10-30
    finished: 2020-12-18
links:
- https://en.wikipedia.org/wiki/The_Mandalorian_(season_2)
---

