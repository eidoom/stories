---
titles:
- Land of Hope and Glory
taxonomies:
  media:
  - video
  formats:
  - film
  - documentary
  years:
  - 2018
  genres:
  - veganism
dates:
  published: 2023-01-19
  updated: 2023-01-19
#  experienced:
#  - started: 2023-01-19
#    finished: 2023-01-19
links:
- https://www.landofhopeandglory.org/
---
