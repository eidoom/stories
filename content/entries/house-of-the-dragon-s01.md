---
titles:
- House of the Dragon S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fantasy
  years:
  - 2022
collections:
- name: House of the Dragon
  number: 1
- name: Game of Thrones
  number: 0.1
dates:
  published: 2022-09-02
  updated: 2022-09-03
  experienced:
  - started: 2022-09-03
    finished: 2022-10-29
links:
- https://en.wikipedia.org/wiki/House_of_the_Dragon
---
