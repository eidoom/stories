---
titles:
- Doctor Strange in the Multiverse of Madness
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - superhero
  years:
  - 2022
collections:
- name: Doctor Strange
  number: 2
- name: Marvel Cinematic Universe
  number: 28
dates:
  published: 2022-06-29
  updated: 2022-06-29
  experienced:
  - started: 2022-06-28
    finished: 2022-06-28
links:
- https://en.wikipedia.org/wiki/Doctor_Strange_in_the_Multiverse_of_Madness
---

