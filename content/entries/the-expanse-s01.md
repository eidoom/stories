---
titles:
- The Expanse S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - science fiction
  - hard sci-fi
  - drama
  - space opera
  - mystery
  - thriller
  - horror
  - speculative fiction
  years:
  - 2015
collections:
- name: The Expanse (TV)
  number: 1
- name: The Expanse
  number: 101
dates:
  published: 2021-01-06
  updated: 2022-08-28
  experienced:
  - started: 2017-10-01
completed: true
links:
- https://en.wikipedia.org/wiki/List_of_The_Expanse_episodes#Season_1_(2015%E2%80%9316)
---
