---
titles:
- The Dreaming Void
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2007
  artists:
  - Peter F. Hamilton
collections:
- name: The Void Trilogy
  number: 1
- name: Commonwealth universe
  number: 4
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
