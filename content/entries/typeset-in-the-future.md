---
titles:
- Typeset In The Future
taxonomies:
  media:
  - web
  formats:
  - blog
  years:
  - 2014
dates:
  published: 2021-08-12
  updated: 2021-08-12
  experienced:
  - '?'
description: Nerd humour and the stories in the fonts of sci-fi films
links:
- https://typesetinthefuture.com/
---

