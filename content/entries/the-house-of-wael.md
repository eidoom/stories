---
titles:
- The House of Wael
taxonomies:
  media:
  - book
  formats:
  - short story
  years:
  - 2016
  artists:
  - Chris Avellone
collections:
- name: Pillars of Eternity
  number: 1.25
dates:
  published: 2023-02-12
  updated: 2023-02-14
  experienced:
  - started: 2023-02-13
    finished: 2023-02-14
---
