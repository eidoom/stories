---
titles:
- The Slow Regard of Silent Things
taxonomies:
  media:
  - book
  formats:
  - novella
  years:
  - 2014
  artists:
  - Patrick Rothfuss
collections:
- name: The Kingkiller Chronicle
  number: 2.1
dates:
  published: 2022-08-14
  updated: 2022-08-14
  experienced:
  - started: 2022-08-16
    finished: 2022-08-18
links:
- https://en.wikipedia.org/wiki/The_Slow_Regard_of_Silent_Things
---

