---
titles:
- The Quarry
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - horror
  - interactive drama
  - survival
  - survival horror
  years:
  - 2022
  artists:
  - Supermassive Games
dates:
  published: 2022-08-20
  updated: 2022-08-20
  experienced:
  - started: 2022-08-19
    finished: 2022-08-20
links:
- https://en.wikipedia.org/wiki/The_Quarry_(video_game)
---

Played with 9 people.
