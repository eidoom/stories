---
titles:
- Luca
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - family friendly
  - italy
  - fantasy
  - bildungsroman
  - fiction
  - animated
  - speculative fiction
  years:
  - 2021
  artists:
  - Pixar Animation Studios
dates:
  published: 2021-06-27
  updated: 2021-06-27
  experienced:
  - started: 2021-06-26
    finished: 2021-06-26
description: Molto buono
links:
- https://en.wikipedia.org/wiki/Luca_(2021_film)
---

