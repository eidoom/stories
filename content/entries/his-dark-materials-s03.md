---
titles:
- His Dark Materials S03
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fiction
  - fantasy
  - speculative fiction
  - drama
  - adventure
  - bildungsroman
  - mystery
  - parallel universe
  years:
  - 2022
collections:
- name: His Dark Materials (TV)
  number: 3
- name: Northern Lights
  number: 0.93
dates:
  published: 2022-11-01
  updated: 2023-01-10
  experienced:
  - started: 2023-01-08
    finished: 2023-01-10
---
