---
titles:
- The Wandering Earth
- 流浪地球
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - fiction
  - science fiction
  - speculative fiction
  - disaster
  - action
  - thriller
  - subtitled
  years:
  - 2019
  languages:
  - Mandarin
dates:
  published: 2021-10-03
  updated: 2021-10-03
  experienced:
  - started: 2021-10-02
    finished: 2021-10-02
links:
- https://en.wikipedia.org/wiki/The_Wandering_Earth
---

