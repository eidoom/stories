---
titles:
- American History X
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - crime
  - drama
  - white supremacy
  - neo-nazi
  - racism
  - poverty
  - indoctrination
  - prison
  years:
  - 1998
dates:
  published: 2021-05-27
  updated: 2021-06-08
  experienced:
  - started: 2021-05-26
    finished: 2021-05-26
description: The skinheads
links:
- https://en.wikipedia.org/wiki/American_History_X
---

Wow.
This is a powerful film.
