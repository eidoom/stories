---
titles:
- The Sandman S01
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - fantasy
  - drama
  years:
  - 2022
  artists:
  - Neil Gaiman
dates:
  published: 2022-11-03
  updated: 2022-11-05
  experienced:
  - started: 2022-11-02
    finished: 2022-11-05
links:
- https://en.wikipedia.org/wiki/The_Sandman_(TV_series)
---
