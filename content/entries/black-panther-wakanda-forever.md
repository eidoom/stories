---
titles:
- 'Black Panther: Wakanda Forever'
taxonomies:
  media:
  - video
  formats:
  - film
  genres:
  - superhero
  years:
  - 2022
  artists:
  - 'Angela Bassett'
  - 'Danai Gurira'
  - 'Dominique Thorne'
  - 'Florence Kasumba'
  - 'Jennifer Lame'
  - 'Joe Robert Cole'
  - 'Julia Louis-Dreyfus'
  - 'Kelley Dixon'
  - 'Kevin Feige'
  - 'Letitia Wright'
  - "Lupita Nyong'o"
  - 'Mabel Cadena'
  - 'Martin Freeman'
  - 'Michael P. Shawver'
  - 'Michaela Coel'
  - 'Nate Moore'
  - 'Ryan Coogler'
  - 'Tenoch Huerta Mejía'
  - 'Winston Duke'
collections:
- name: Marvel Cinematic Universe
  number: 30
- name: Black Panther
  number: 2
dates:
  published: 2023-02-05
  updated: 2023-02-05
  experienced:
  - started: 2023-02-05
    finished: 2023-02-05
links:
- https://en.wikipedia.org/wiki/Black_Panther:_Wakanda_Forever
---
