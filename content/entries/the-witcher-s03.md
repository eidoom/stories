---
titles:
- The Witcher S03
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - dark
  - fantasy
  - drama
  years:
  - unreleased
collections:
- name: The Witcher (TV)
  number: 3
- name: The Witcher
  number: 0.93
dates:
  published: 2021-09-05
  updated: 2022-04-19
---
