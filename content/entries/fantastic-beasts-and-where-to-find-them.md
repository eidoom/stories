---
titles:
- Fantastic Beasts and Where to Find Them
taxonomies:
  media:
  - video
  formats:
  - film
  years:
  - 2016
collections:
- name: Fantastic Beasts
  number: 1
- name: Wizarding World
  number: 9
dates:
  published: 2022-06-02
  updated: 2022-06-02
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Fantastic_Beasts_and_Where_to_Find_Them_(film)
---

