---
titles:
- Stranger Things S05
taxonomies:
  media:
  - video
  formats:
  - television
  genres:
  - horror
  - drama
  - science fiction
  - speculative fiction
  years:
  - unreleased
collections:
- name: Stranger Things
  number: 5
dates:
  published: 2022-07-19
  updated: 2022-08-28
---
