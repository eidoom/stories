---
titles:
- 'Outer Wilds: Echoes of the Eye'
taxonomies:
  media:
  - game
  formats:
  - video game
  - computer game
  genres:
  - temporal loop
  - science fiction
  - fiction
  - speculative fiction
  - aliens
  - exploration
  - mystery
  - story
  years:
  - 2021
  artists:
  - Mobius Digital
collections:
- name: Outer Wilds
  number: 2
dates:
  published: 2022-06-02
  updated: 2022-08-27
  experienced:
  - started: 2022-09-03
active: true
---
