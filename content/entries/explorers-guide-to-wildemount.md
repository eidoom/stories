---
titles:
- Explorer's Guide to Wildemount
taxonomies:
  media:
  - book
  years:
  - 2020
  formats:
  - sourcebook
  artists:
  - Wizards of the Coast
  genres:
  - campaign
collections:
- name: Dungeons & Dragons
  number: 5.35
- name: Dungeons & Dragons 5th Edition
  number: 25
- name: Critical Role
  number: 0
dates:
  published: 2022-11-13
  updated: 2022-11-13
  experienced:
  - started: 2022-11-13
    finished: 2022-11-13
links:
- https://en.m.wikipedia.org/wiki/Explorer%27s_Guide_to_Wildemount
---
