---
titles:
- Warhammer
- Warhammer Fantasy Battle
taxonomies:
  media:
  - game
  formats:
  - miniature wargame
  years:
  - 1983
dates:
  published: 2022-05-06
  updated: 2022-05-06
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Warhammer_(game)
---

