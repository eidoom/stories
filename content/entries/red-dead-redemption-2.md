---
titles:
- Red Dead Redemption 2
taxonomies:
  media:
  - game
  formats:
  - computer game
  genres:
  - RAGE
  - action-adventure
  years:
  - 2018
  - 2019
  artists:
  - Aaron Garbut
  - Dan Houser
  - Imran Sarwar
  - Michael Unsworth
  - Phil Hooker
  - Rob Nelson
  - Rockstar Studios
  - Rupert Humphries
  - Woody Jackson
collections:
- name: Red Dead
  number: 3
dates:
  published: 2023-03-19
  updated: 2023-03-19
  experienced:
  - started: 2023-03-19
#    finished: 2023-03-19
links:
- https://en.wikipedia.org/wiki/Red_Dead_Redemption_2
active: true
---
