---
titles:
- The Difference Engine 
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - alternate history
  - steampunk
  years:
  - 1990
  artists:
  - William Gibson
  - Bruce Sterling
dates:
  published: 2023-01-29
  updated: 2023-01-29
  experienced:
  - started: 2023-01-28
    finished: 2023-02-11
links:
- https://en.wikipedia.org/wiki/The_Difference_Engine
---
