---
titles:
- Ori and the Will of the Wisps
taxonomies:
  media:
  - game
  formats:
  - computer game
  genres:
  - unity
  - platform-adventure
  - metroidvania
  years:
  - 2020
  artists:
  - Alexander O. Smith
  - Ander Goenaga Iriondo
  - Boris Hiestand
  - Błażej Żywiczyński
  - Chris McEntee
  - Daniel van Leeuwen
  - Franciska Csongrady
  - Gareth Coker
  - Gennadiy Korol
  - Jeremy Gritton
  - Moon Studios
  - Thomas Mahler
collections:
- name: Ori
  number: 2
dates:
  published: 2023-02-11
  updated: 2023-02-11
links:
- https://www.orithegame.com/
- https://en.wikipedia.org/wiki/Ori_and_the_Will_of_the_Wisps
---