---
titles:
- Thirteen
taxonomies:
  media:
  - book
  formats:
  - novel
  years:
  - 2007
  artists:
  - Richard K. Morgan
collections:
- name: Genetically modified
  number: 1
dates:
  published: 2020-03-01
  updated: 2021-07-30
  experienced:
  - started: 2020-04-24
    finished: 2020-05-11
links:
- https://en.wikipedia.org/wiki/Black_Man_(novel)
---
