---
titles:
- Unholy War
taxonomies:
  media:
  - book
  formats:
  - novel
  genres:
  - fantasy
  years:
  - 2014
  artists:
  - David Hair
collections:
- name: Moontide Quartet
  number: 3
- name: Mage’s Blood
  number: 3
dates:
  published: 2019-06-15
  updated: 2021-07-30
  experienced:
  - '?'
---
