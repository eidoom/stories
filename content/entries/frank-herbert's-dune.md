---
titles:
- Frank Herbert’s Dune
taxonomies:
  media:
  - video
  formats:
  - television
  - miniseries
  genres:
  - science fiction
  years:
  - 2000
  artists:
  - John Harrison
collections:
- name: Dune
  number: 1.2
dates:
  published: 2021-09-05
  updated: 2021-09-05
  experienced:
  - '?'
links:
- https://en.wikipedia.org/wiki/Frank_Herbert's_Dune
---

