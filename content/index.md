---
titles:
- Stories
description: A collection of thoughts inspired by media
template: index.html
date: 2020-10-01
updated: 2021-06-27
short: about
---

DEPRECATED [new site here](https://stories.eidoom.duckdns.org)

I started this website as a project to maintain discourse in letter form on stories enjoyed during the isolation of coronavirus quarantine.
In more recent times, with the freedom to spontaneously converse in person again, it is coming to more resemble a media log.
I also used it as a pedagogical  exercise in developing a website, although I have yet to correct all the mistakes I've learnt from...

The old site can be found [here](https://somestories.vercel.app/).

Get started by checking out the [entries](entries).

Warning: [spoilers](https://en.wikipedia.org/wiki/Spoiler_(media)) and [protologisms](https://en.wikipedia.org/wiki/Protologism).

Continue the conversation on [Matrix](https://matrix.to/#/@ryan:eidoom.duckdns.org).
