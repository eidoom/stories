---
titles:
- Legal
dates:
  published: 2021-02-07
  updated: 2021-04-03
description: Details of licencing, copyright, privacy, and terms of use
template: licence.html
---

## Author

The *author* of this *website* is Ryan Moodie, who can be contacted [here](mailto:ryanmoodie@gmail.com).

This is a personal project which is non-commercial, with no affiliations.

## Copyright

The *website* is copyright of the *author*.

## Licencing

The *website* comprises both
* the source code which is used to build the web pages and is publicly available as a Git repository [here](https://gitlab.com/eidoom/stories/), henceforth called the *source*.
* the compiled web pages which are served [here](..) as viewed in the browser, henceforth called the *contents*, which can be compiled from the source manually or accessed online at <https://eidoom.gitlab.io/stories>.

The *contents* and *source* are licenced separately as defined in the following sections.

Where the ownership of any code or content does not lie with the *author*, this is clearly stated and appropriate attribution given.

### Contents

The *contents* are licenced under the terms of the [Creative Commons Attribution-ShareAlike 4.0 International Licence (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/).
The full licence can be read in HTML [here](https://creativecommons.org/licenses/by-sa/4.0/legalcode) or in plain text [here](https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt).

### Source

The *source* is licenced under the terms of the [GNU Affero General Public License v3.0](https://www.gnu.org/licenses/agpl-3.0.en.html).
The public repository carries a copy of the licence that can be read [here](https://gitlab.com/eidoom/stories/-/raw/master/LICENSE).

## Privacy

The *website* does not ask for, track, or hold any data on its users, nor does it use any external service to do so.

No statement is made for the practices of the hosting service GitLab Pages from [GitLab](https://gitlab.com), whose privacy policy can be found [here](https://about.gitlab.com/privacy/).

## Cost

There is no monetary charge to access the *website*.

There are no advertisements on the *website*.

The *website* is hosted for free by GitLab Pages and the [source code](https://gitlab.com/eidoom/stories/) is hosted for free on [GitLab](https://gitlab.com/).
This is on the *author’s* personal account on the [Free Plan](https://about.gitlab.com/pricing/).

All software used to develop the *website* is free and open source.
