---
titles:
- His Dark Materials (TV)
description: The adventures of Lyra Silvertongue
links:
- https://en.wikipedia.org/wiki/His_Dark_Materials_(TV_series)
---

A worthy cinematisation of the nostalgic novels.
