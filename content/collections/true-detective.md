---
titles:
- True Detective
description: Superbly dark neo-noir
links:
- https://en.wikipedia.org/wiki/True_Detective
---

Great title sequences.

Society is *messed up*.
The immoral precipitate out at the top.
Policing exacts the highest toll on its officers.

My only complaint is that it is extremely difficult for me to understand what the detectives are actually saying in their aesthetic gravelly accents.
