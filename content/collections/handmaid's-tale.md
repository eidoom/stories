---
titles:
- The Handmaid’s Tale
description: Blessed be the Fruit Loops
links:
- https://en.wikipedia.org/wiki/The_Handmaid%27s_Tale_(TV_series)
---

This is very painful to watch, overall.
There are moments of reprise: cherished instants of gaiety and intimacy between our oppressed characters, flashbacks to a normal past, some rare successful escapes to Canada, even brief empowerment as opportunities arise. 
But the Lord giveth and the Lord taketh away; fleeting happiness comes at a price in Gilead and suffering is sure to follow—“There will be consequences.”
That most precious thing, hope, rises and ebbs.
Even the lives of the fortunate free are traumatised by their experiences and the thought of those who didn’t make it.

In a dystopian future, human birth rates have plummeted worldwide with fertility of men and women becoming increasingly uncommon, likely as a result of aggressive industrial and technological ravaging of Earth’s ecosystem.
The majority of the United States of America has been conquered by a theocratic totalitarian oligarchy called Gilead.
In this Christian society, male Commanders preside over districts, together forming a national government.
The country is strongly militant, with armed men omnipresent in civilian locations, many of whom are Gestapo-reminiscent Eyes.
Women are either Wives to the Commanders, servant Marthas, Aunts, or Handmaids: fertile women who are held captive as slaves in order to produce offspring.
The Handmaids have their spirits broken by the Aunts at processing centres, then are raped monthy by the Commanders in the Ceremony and have their babies stolen from them.
Criminals are either hanged, or sent to the Colonies, Gilead’s nuclear wastelands, to be slowly worked to death.

The high-class women of Gilead who have chosen this life for themselves, the Wives and Aunts, present a conundrum.
Wife Serena Joy Waterford’s character is impossible to read for me; she is only predictable in her chaos.
She shows apparent compassion and empathy one moment, then swings into bouts of rage and tyrannical villainy at a single wrong move.
Similarly, Aunt Lydia Clements seems to genuinely care for her girls, the Handmaids, but is happy to violently attack them and punish them with torture, not to mention forcing them into their role.
They both seem to be driven by strong maternal instincts, but the reality of their world has driven them to mental instability.

The protagonist, June Osbourne, is a Handmaid who is recounting her tragic experiences to us.
This character endures some of the worst pain and suffering I have ever witnessed in any story.
She is incredibly strong.
Her one driving goal is to save her child daughter, who has been captured to be raised by a Gilead Commander’s family.

We are constantly seeing mirror images of character’s faces.
Often, the person’s face and its reflection are presented together on the screen.
This reminds me of how June and all members of the Gilead society live a dual reality, suppressing the true parts of themselves while outwardly presenting a “perfect” persona to survive.
