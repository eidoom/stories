#!/usr/bin/env python3

import argparse, pathlib, datetime, itertools, urllib.request, urllib.parse, json, re

import jinja2, bs4

import lib

BOOK = ("novel", "short story", "novella", "graphic novel", "comic")


def chunks(iterable, n=2):
    # chunks('ABCDEFG', 3) --> ABC DEF G
    it = iter(iterable)
    while chunk := tuple(itertools.islice(it, n)):
        yield chunk


def url_arg(arg):
    url = urllib.parse.urlparse(arg)
    if url.netloc:
        return urllib.parse.urlunparse(url)
    raise ArgumentTypeError("Invalid URL")


def get_args():
    parser = argparse.ArgumentParser(description="Create new entry for a story")
    parser.add_argument(
        "-n",
        "--titles",
        metavar="T",
        nargs="+",
        type=str,
        default=[],
        help="The title(s) of the story",
    )
    parser.add_argument(
        "-t",
        "--type",
        type=str,
        choices=["entry", "collection"],
        default="entry",
        help="The type of story",
    )
    parser.add_argument(
        "-k",
        "--kind",
        metavar="K",
        type=str,
        help="The medium or format of the story",
    )
    parser.add_argument(
        "-g",
        "--genres",
        metavar="G",
        nargs="+",
        type=str,
        default=[],
        help="The genre(s) of the story",
    )
    parser.add_argument(
        "-a",
        "--artists",
        metavar="A",
        nargs="+",
        type=str,
        default=[],
        help="The artist(s) of the story",
    )
    parser.add_argument(
        "-y",
        "--years",
        metavar="Y",
        nargs="+",
        type=int,
        default=[],
        help="The release year(s) of the story",
    )
    parser.add_argument(
        "-p",
        "--provenances",
        metavar="P",
        nargs="+",
        type=str,
        default=[],
        help="The place(s) where the story is from",
    )
    parser.add_argument(
        "-m",
        "--places",
        metavar="M",
        nargs="+",
        type=str,
        default=[],
        help="The place(s) which feature in the story",
    )
    parser.add_argument(
        "-v",
        "--languages",
        metavar="V",
        nargs="+",
        type=str,
        default=[],
        help="The language(s) of the story",
    )
    parser.add_argument(
        "-l",
        "--links",
        metavar="L",
        nargs="+",
        type=url_arg,
        default=[],
        help="Link(s) related to the story",
    )
    parser.add_argument(
        "-c",
        "--collections",
        metavar="C",
        nargs="+",
        type=str,
        default=[],
        help="Collection and index pair(s) that the story belongs to",
    )
    parser.add_argument(
        "-b",
        "--force",
        action="store_true",
        help="Overwrite existing file",
    )
    parser.add_argument(
        "-w",
        "--wikipedia",
        type=url_arg,
        metavar="W",
        help="Wikipedia link to scrape",
    )
    parser.add_argument(
        "-e",
        "--active",
        action="store_true",
        help="Mark as active",
    )

    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "-s",
        "--started",
        action="store_true",
        help="Select if only started the story",
    )
    group.add_argument(
        "-f",
        "--finished",
        action="store_true",
        help="Select if finished the story today",
    )
    group.add_argument(
        "-o",
        "--old",
        action="store_true",
        help="Select if finished the story a long time ago",
    )

    args = parser.parse_args()

    args.titles = [title.strip() for title in args.titles]

    if args.type == "collection":
        vargs = vars(args)
        if any(
            vargs[arg] is not None
            for arg in (
                "artists",
                "genres",
                "kind",
                "years",
                "collections",
            )
        ):
            raise Exception(f"Argument {arg} is incompatible with type collection")

    args.artists = [artist.strip() for artist in args.artists]
    args.genres = [genre.strip() for genre in args.genres]
    args.languages = [language.strip() for language in args.languages]
    args.links = [link.strip() for link in args.links]
    args.places = [place.strip() for place in args.places]
    args.provenances = [provenance.strip() for provenance in args.provenances]

    if len(args.collections) % 2:
        raise Exception(f"Argument collections must have an even number of arguments")

    args.collections = [
        {"name": nam, "number": num} for nam, num in chunks(args.collections)
    ]

    if args.kind == "tv":
        args.kind = "television"

    return args


def scrape(args):
    args.links.append(args.wikipedia)

    page = urllib.parse.urlparse(args.wikipedia).path[6:]

    html = json.loads(
        urllib.request.urlopen(
            f"https://en.wikipedia.org/w/api.php?action=parse&page={page}&prop=text&formatversion=2&format=json"
        ).read()
    )["parse"]["text"]

    soup = bs4.BeautifulSoup(html, features="html.parser")

    infobox = soup.find("table", class_="infobox")

    for cc in ("Country", "Country of origin"):
        try:
            td = infobox.find("th", string=cc).parent.td
            args.provenances += [a.string for a in td.find_all("li")]
            if (lone := td.string) is not None:
                args.provenances.append(lone)
        except AttributeError:
            pass

    if args.kind in ("television", "film"):
        artists = set()

        for heading in (
            "Cinematography",
            "Composer",
            "Composers",
            "Created by",
            "Creative director",
            "Developed by",
            "Directed by",
            "Edited by",
            "Music by",
            "Narrated by",
            "Produced by",
            "Screenplay by",
            "Story by",
            "Starring",
            "Theme music composer",
            "Voices of",
            "Written by",
        ):
            try:
                td = infobox.find("th", string=heading).parent.td
                artists |= set(
                    a.string for a in td.find_all("li") if a.string is not None
                )
                if (lone := td.string) is not None:
                    artists.add(lone)
            except AttributeError:
                pass

        args.artists += sorted(artists)

    if args.kind == "television":
        if not args.titles:
            args.titles = [infobox.find("i").string]

        args.genres += [
            li.find("a").string.lower()
            for li in infobox.find("th", string="Genre").parent.find_all("li")
        ]

        args.years.append(
            datetime.date.fromisoformat(
                infobox.find("span", class_="published").string
            ).year
        )

    elif args.kind == "film":
        if not args.titles:
            args.titles = [infobox.find("th", class_="infobox-above").string]

        args.years.append(
            min(
                datetime.date.fromisoformat(d.string).year
                for d in infobox.find_all("span", class_="published")
            )
        )

    elif args.kind in ["graphic novel", "comic"]:
        args.titles = [" ".join(infobox.find("i").stripped_strings)]

        tr = infobox.find("th", string="Date").parent

        args.years.append(
            min(
                map(
                    int,
                    re.findall(
                        r"\d{4}",
                        " ".join(tr.td.stripped_strings),
                    ),
                )
            )
        )

    elif args.kind in ["book", "novel", "short story", "novella"]:
        args.titles = [infobox.find("caption", class_="infobox-title").contents[0]]

        args.genres += [
            a.string.lower()
            for a in infobox.find("th", string="Genre").parent.find_all("a")
        ]

        try:
            tr = infobox.find("div", string="Publication date").parent.parent
        except AttributeError:
            tr = infobox.find("th", string="Published").parent

        args.years.append(
            int(
                re.search(
                    r"\d{4}",
                    tr.td.string,
                ).group(0)
            )
        )

        args.artists += [
            a.string for a in infobox.find("th", string="Author").parent.find_all("a")
        ]

        if not args.collections:
            try:
                args.collections = [
                    {
                        "name": infobox.find("th", string="Series").parent.td.string,
                        "number": 0,
                    }
                ]
            except AttributeError:
                pass

        args.languages.append(infobox.find("th", string="Language").parent.td.string)

        try:
            args.places.append(infobox.find("th", string="Set in").parent.td.string)
        except AttributeError:
            pass

    elif args.kind in (
        "game",
        "computer game",
        "video game",
    ):
        if not args.titles:
            args.titles = [infobox.find("th", class_="infobox-above").string]

        artists = set()

        for heading in (
            "Developer(s)",
            "Director(s)",
            "Producer(s)",
            "Designer(s)",
            "Programmer(s)",
            "Artist(s)",
            "Writer(s)",
            "Composer(s)",
        ):
            try:
                td = infobox.find("th", string=heading).parent.td
                artists |= set(a.string for a in td.find_all("li"))
                if (lone := td.string) is not None:
                    artists.add(lone)
            except AttributeError:
                pass

        args.artists += sorted(artists)

        for heading in (
            "Engine",
            "Genre(s)",
            "Modes(s)",
        ):
            try:
                args.genres += [
                    a.string.lower()
                    for a in infobox.find("th", string=heading).parent.td.find_all("a")
                ]
            except AttributeError:
                pass

        args.years.append(
            min(
                int(m.group(0))
                for m in (
                    re.search(r"\d{4}", y.string)
                    for y in infobox.find("th", string="Release").parent.find_all("li")
                    if y.string
                )
                if m
            )
        )

    else:
        raise Exception(f"Wikipedia scrape not supported for kind {args.kind}")

    return args


def create_entry(args):
    if args.wikipedia:
        args = scrape(args)

    args.titles = [f"'{title}'" if ":" in title else title for title in args.titles]

    slug = lib.slugify(args.titles[0])

    folder = {"entry": "entries", "collection": "collections"}[args.type]

    file = pathlib.Path(f"content/{folder}/{slug}.md")

    if file.exists():
        if args.force:
            print(f"Overwriting {file}")
        else:
            print(f"File {file} already exists! Use `-b` to overwrite")
            return
    else:
        print(f"Creating {file}")

    arg_dict = vars(args)
    data = {
        thing: arg_dict[thing]
        for thing in (
            "artists",
            "collections",
            "finished",
            "genres",
            "languages",
            "links",
            "old",
            "places",
            "provenances",
            "started",
            "titles",
            "years",
        )
        if thing in arg_dict
    }

    data["date"] = datetime.date.today().isoformat()

    if args.active:
        data["active"] = True

    if args.kind in ("web", "game", "book", "video"):
        data["media"] = [args.kind]
        data["formats"] = []
    elif args.kind:
        data["formats"] = [args.kind]

        if args.kind in ("film", "television"):
            data["media"] = ["video"]
        elif args.kind in (
            "board game",
            "card game",
            "computer game",
            "video game",
        ):
            data["media"] = ["game"]
        elif args.kind == "blog":
            data["media"] = ["web"]
        elif args.kind in BOOK:
            data["media"] = ["book"]

    archetypes = pathlib.Path("archetypes")
    env = jinja2.Environment(loader=jinja2.FileSystemLoader(archetypes))
    template = {
        filename.stem.split(".")[0]: env.get_template(filename.name)
        for filename in archetypes.glob("*.jinja2")
    }

    output = template[args.type].render(data)

    file.write_text(output)


if __name__ == "__main__":
    args = get_args()

    create_entry(args)
