#!/usr/bin/env python3

# WIP

import urllib.request, json

from new import get_args


def last_frag(url):
    return url.split("/")[-1]


def extract(data):
    return [last_frag(item["value"]).replace("_", " ") for item in data]


if __name__ == "__main__":
    args = get_args()

    wiki = next((l for l in args.links if "wikipedia" in l), None)
    if args.links and wiki:
        slug = last_frag(wiki)
    else:
        slug = args.titles[0].replace(" ", "_")

    base = "http://dbpedia.org"

    data = json.loads(urllib.request.urlopen(f"{base}/data/{slug}.json").read())[
        f"{base}/resource/{slug}"
    ]

    args = vars(args)

    if f"{base}/ontology/literaryGenre" in data:
        genres = extract(data[f"{base}/ontology/literaryGenre"])
        if not args["genres"]:
            args["genres"] = []
        args["genres"] += genres

    if f"{base}/property/author" in data:
        authors = extract(data[f"{base}/property/author"])
        if not args["artists"]:
            args["artists"] = []
        args["artists"] += authors

    if f"{base}/property/series" in data:
        collections = extract(data[f"{base}/property/series"])
        args["collections"] = [{"name": n, "number": "?"} for n in collections]

    print(args)
