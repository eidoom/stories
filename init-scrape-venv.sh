if [ "$0" != "-bash" ]; then
	echo "This must be run as \`source init-scrape-venv.sh\`"
fi

DIR=.venv-scrape
REQ=requirements-scrape.txt

if [ ! -d "${DIR}" ]; then
	python3 -m venv "${DIR}"
	source "${DIR}/bin/activate"
	if [ -f "${REQ}" ]; then
		python3 -m pip install -r "${REQ}"
	fi
else
	source "${DIR}/bin/activate"
fi

echo "venv activated"
echo "use \`deactivate\` when done"
