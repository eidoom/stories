#!/usr/bin/env bash

echo http://localhost:8000

(trap 'kill 0' SIGINT;
[[ -d node_modules ]] || npm i
source init-venv.sh
npx rollup -c -w --no-watch.clearScreen &
while sleep 0.1; do find content/ static/ layout/ compile.py -type f | entr -d ./compile.py; done &
python3 -m http.server --directory public
)
