import svelte from "rollup-plugin-svelte";
import resolve from "@rollup/plugin-node-resolve";
import { createFilter, dataToEsm } from "@rollup/pluginutils";
import { terser } from "rollup-plugin-terser";
import css from "rollup-plugin-css-only";

const production = !process.env.ROLLUP_WATCH;

const date = /(\d{4})-([01]\d)-([0-3]\d)/;

function dateReviver(key, value) {
  if (typeof value === "string") {
    const match = date.test(value);
    if (match) {
      return new Date(value);
    }
  }
  return value;
}

// edit of https://github.com/rollup/plugins/tree/master/packages/json
function json(options = {}) {
  const filter = createFilter(options.include, options.exclude);
  const indent = "indent" in options ? options.indent : "\t";

  return {
    name: "json",

    // eslint-disable-next-line no-shadow
    transform(code, id) {
      if (id.slice(-5) !== ".json" || !filter(id)) return null;

      try {
        const parsed = JSON.parse(code, dateReviver);
        return {
          code: dataToEsm(parsed, {
            preferConst: options.preferConst,
            compact: options.compact,
            namedExports: options.namedExports,
            indent,
          }),
          map: { mappings: "" },
        };
      } catch (err) {
        const message = "Could not parse JSON file";
        const position = parseInt(/[\d]/.exec(err.message)[0], 10);
        this.warn({ message, id, position });
        return null;
      }
    },
  };
}

export default {
  input: ["src/entries_index.js", "src/collections_index.js"],
  output: {
    dir: "public",
  },
  plugins: [
    svelte({
      compilerOptions: {
        // enable run-time checks when not in production
        dev: !production,
      },
    }),

    // we'll extract any component CSS out into
    // a separate file - better for performance
    css({ output: "bundle.css" }),

    // If you have external dependencies installed from
    // npm, you'll most likely need these plugins. In
    // some cases you'll need additional configuration -
    // consult the documentation for details:
    // https://github.com/rollup/plugins/tree/master/packages/commonjs
    resolve({
      browser: true,
      dedupe: ["svelte"],
    }),

    json(),

    // If we're building for production (npm run build
    // instead of npm run dev), minify
    production && terser(),
  ],
};
