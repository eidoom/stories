#!/usr/bin/env python3

import argparse, math, pathlib, json, datetime, shutil

import jinja2, mistletoe, yaml

try:
    from yaml import CLoader as yaml_loader
except ImportError:
    from yaml import Loader as yaml_loader

from lib import slugify


class MyJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.date):
            return obj.isoformat()


def load_md(entry):
    with open(entry, "r") as f:
        raw = f.read()

    if raw[:3] != "---":
        raise Exception("Content file format unrecognised")

    yaml_data, markdown = raw.split("---", maxsplit=2)[1:]

    try:
        meta = yaml.load(yaml_data, Loader=yaml_loader)
    except (yaml.scanner.ScannerError, yaml.parser.ParserError):
        raise Exception(f"Error in YAML of {entry}")

    data = {}

    if meta:
        data["slug"] = slugify(meta["titles"][0])

        data["meta"] = meta

        try:
            data["meta"]["taxonomies"]["years"] = sorted(
                data["meta"]["taxonomies"]["years"]
            )
        except KeyError:
            pass

        words = len(markdown.split())
        # https://help.medium.com/hc/en-us/articles/214991667-Read-time
        data["meta"]["mins"] = math.ceil(words / 265)
    else:
        data["slug"] = entry.stem

    data["html"] = markdown.strip()

    return data


def process_md(md):
    return mistletoe.markdown(md).strip().translate(str.maketrans("\n", " "))


def date_format(value, format="%d %b %Y"):
    return value.strftime(format)


def get_args():
    parser = argparse.ArgumentParser(
        description="Compile static pages and json data from markdown content"
    )
    parser.add_argument(
        "-b", "--base", type=str, default="", help="Base URL of website"
    )
    return parser.parse_args()


if __name__ == "__main__":
    print("Compiling")

    args = get_args()

    content = pathlib.Path("content")
    layout = pathlib.Path("layout")
    static = pathlib.Path("static")
    public = pathlib.Path("public")
    src = pathlib.Path("src")

    public.mkdir(exist_ok=True)

    for file in static.iterdir():
        shutil.copy(file, public)

    env = jinja2.Environment(loader=jinja2.FileSystemLoader(layout))
    env.filters["date_format"] = date_format
    env.filters["slugify"] = slugify

    template = {
        filename.stem.split(".")[0]: env.get_template(filename.name)
        for filename in layout.glob("*.jinja2")
    }

    base = {"base": args.base}

    metadata = {
        "entries": [],
        "collections": {},
        "taxonomies": {},
    }

    path_map = {}

    for entry in content.glob("**/*.md"):
        category = entry.parts[1]

        if category == "entries":
            data = load_md(entry)

            if data["slug"] in (e["slug"] for e in metadata["entries"]):
                data["slug"] += "-"

            metadata["entries"].append(data)

        elif category == "collections":
            data = load_md(entry)
            data["html"] = process_md(data["html"])
            data["meta"] |= {"entries": [], "intersections": set()}

            # assume unique names
            metadata["collections"][data["slug"]] = data

        else:
            data = load_md(entry)
            data["html"] = process_md(data["html"])
            html = template["entries"].render(data | base)
            (public / f"{entry.stem}.html").write_text(html)

        parent = "/".join(entry.parts[1:-1]) + "/" if len(entry.parts) > 2 else ""
        path_map["/".join(entry.parts[1:])] = f"{parent}{data['slug']}.html"

    entries = public / "entries"
    entries.mkdir(exist_ok=True)
    for entry in metadata["entries"]:
        # TODO process internal links with path_map (and also do for top level entries and collections)

        entry["html"] = process_md(entry["html"])

        dest = entries / f"{entry['slug']}.html"
        html = template["entries"].render(entry | base)
        dest.write_text(html)

    metadata["entries"] = [
        {k: v for k, v in e.items() if k != "html"} for e in metadata["entries"]
    ]
    metadata["entries"] = sorted(metadata["entries"], key=lambda x: x["slug"])

    for entry in (e for e in metadata["entries"] if "collections" in e["meta"]):
        for col in entry["meta"]["collections"]:
            slug = slugify(col["name"])
            if slug not in metadata["collections"]:
                metadata["collections"][slug] = {
                    "meta": {
                        "titles": [col["name"]],
                        "entries": [],
                        "intersections": set(),
                    }
                }
            try:
                metadata["collections"][slug]["meta"]["entries"].append(
                    {
                        "number": col["number"],
                        "slug": entry["slug"],
                        "title": entry["meta"]["titles"][0],
                        "year": entry["meta"]["taxonomies"]["years"][0],
                        "media": entry["meta"]["taxonomies"]["media"],
                    }
                )
            except KeyError:
                raise Exception(f"Check metadata of {entry['slug']}")
            metadata["collections"][slug]["meta"]["intersections"] |= set(
                c["name"]
                for c in entry["meta"]["collections"]
                if c["name"] != col["name"]
            )

    metadata["collections_index"] = sorted(
        [
            {
                "slug": slug,
                "name": entry["meta"]["titles"][0],
                "count": len(entry["meta"]["entries"]),
            }
            for slug, entry in metadata["collections"].items()
        ],
        key=lambda x: x["name"],
    )

    collections = public / "collections"
    collections.mkdir(exist_ok=True)
    for slug, col in metadata["collections"].items():
        dest = collections / f"{slug}.html"
        html = template["collections"].render(col | base)
        dest.write_text(html)

    for entry in (e for e in metadata["entries"] if "taxonomies" in e["meta"]):
        for taxonomy, items in entry["meta"]["taxonomies"].items():
            if taxonomy not in metadata["taxonomies"]:
                metadata["taxonomies"][taxonomy] = {}
            for item in items:
                slug = slugify(item)
                if slug not in metadata["taxonomies"][taxonomy]:
                    metadata["taxonomies"][taxonomy][slug] = {
                        "name": item,
                        "entries": [],
                    }
                metadata["taxonomies"][taxonomy][slug]["entries"].append(
                    {
                        "name": entry["meta"]["titles"][0],
                        "slug": entry["slug"],
                        "year": entry["meta"]["taxonomies"]["years"][0],
                        "media": entry["meta"]["taxonomies"]["media"],
                    }
                )

    metadata["taxonomies_index"] = {
        "media": sorted(
            [entry["name"] for _, entry in metadata["taxonomies"]["media"].items()]
        ),
        "formats": sorted(
            [entry["name"] for _, entry in metadata["taxonomies"]["formats"].items()]
        )
    }

    for taxonomy, items in metadata["taxonomies"].items():
        dest = public / f"taxonomies/{slugify(taxonomy)}/index.html"
        dest.parent.mkdir(parents=True, exist_ok=True)
        html = template["taxonomy_list"].render(
            {"name": taxonomy, "items": items} | base
        )
        dest.write_text(html)

        for slug, item in items.items():
            dest = public / f"taxonomies/{slugify(taxonomy)}/{slug}.html"
            dest.parent.mkdir(parents=True, exist_ok=True)
            html = template["taxonomy_item"].render(item | base)
            dest.write_text(html)

    for thing in ("entries", "collections"):
        html = template[f"{thing}_index"].render(base)
        (public / f"{thing}/index.html").write_text(html)

    html = template["taxonomies"].render(
        {
            "taxonomies": metadata["taxonomies"],
            "total": sum(
                [
                    len(taxonomy_dict)
                    for _, taxonomy_dict in metadata["taxonomies"].items()
                ]
            ),
        }
        | base
    )
    (public / "taxonomies/index.html").write_text(html)

    for thing in ("entries", "collections_index", "taxonomies_index"):
        with open(src / f"{thing}.json", "w") as f:
            json.dump(
                metadata[thing],
                f,
                cls=MyJSONEncoder,
            )
