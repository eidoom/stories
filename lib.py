#!/usr/bin/env python3

import re


def slugify(phrase):
    phrase = str(phrase).strip().lower().replace("–", "-").replace(" ", "-")
    phrase = re.sub(r"[^\w\-]+", "", phrase)
    phrase = re.sub(r"-+", "-", phrase)
    return phrase


if __name__ == "__main__":
    pass
