# [stories](https://gitlab.com/eidoom/stories)

DEPRECATED [see new project](https://gitlab.com/eidoom/stories2)

[Live here](https://eidoom.gitlab.io/stories)

## Description

This is an in-progress rewrite of my project [musings](https://gitlab.com/eidoom/musings/) ([live](https://somestories.vercel.app/)).
The project is simply a website where I write little comments to myself about various stories I've enjoyed.
The purpose of this rewrite is to create a dynamic user interface.

## Usage

### External dependencies

* LibYAML

### Developing

```shell
source init-venv.sh && ./compile.py
npm run dev
python3 -m http.server --directory public
```

### Building

```shell
source init-venv.sh && ./compile.py
npm run build
```
Builds to `public`.

## TODO

* Fix macros in content
* Showing metadata on an entry
    * Format date ranges
    * Taxonomies should show their overlaps like collections do
* CSS: improve styling
    * Light mode (use CSS only: https://stevenwaterman.uk/you-dont-need-js/)
    * Font
    * Responsive
        * Looks good on mobile so far (mobile first tick)
        * Multiple columns when showing tags on wider screens
        * Maximum width on wider screens
        * Top bar will need redesigned for smaller screens if anything else added
* Metadata organisation
    * Split places into places and provenance
    * Replace `completed: true` on entries with `dates>experienced>finished: ?` and update Svelte
    * Use completed for collections
* Data visualisation
    * Display entries on a calendar?
    * Visual representation for number of items in a tag?
    * Can I make any graphs?
* Showing entries
    * Search
    * Option to paginate entries?
    * Format dates so repeat years, months, dates are not shown. Like I did in the old archive.
* JSON feed

## Support

Please report any issues on the [issue tracker](https://gitlab.com/eidoom/stories/-/issues).

## Contributing

This is currently a personal project, but please feel welcome to [fork](https://gitlab.com/eidoom/stories/-/forks/new).

## License

Copyright (C) 2021 Ryan Moodie

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program at `./LICENSE`. If not, see <https://www.gnu.org/licenses/>.
