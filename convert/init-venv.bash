if [ "$0" != "-bash" ]; then
	echo "This must be run as \`source init-venv.bash\`"
else
	DIR=.venv

	if [ ! -d "${DIR}" ]; then
		python3 -m venv "${DIR}"
		source "${DIR}/bin/activate"
		if [ -f "requirements.txt" ]; then
			python3 -m pip install -r requirements.txt
		fi
	else
		source "${DIR}/bin/activate"
	fi

	echo "venv activated"
	echo "use \`deactivate\` when done"
fi
