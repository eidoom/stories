#!/usr/bin/env python3

import copy
import datetime
import pathlib
import collections

import ruamel.yaml
import tomli


def restructure(data, folder):
    new_data = {
        "titles": [],
    }

    if "taxonomies" in data:
        new_data["taxonomies"] = collections.defaultdict(list)

        if ("series" in data["taxonomies"] and data["taxonomies"]["series"]) or (
            "universe" in data["taxonomies"] and data["taxonomies"]["universe"]
        ):
            new_data["collections"] = []

    if ("date" in data) or ("extra" in data and "date" in data["extra"]):
        new_data["dates"] = {}

    for key, value in data.items():
        if key == "extra":
            for ekey, evalue in value.items():
                if evalue:
                    if ekey == "consumed":
                        new_data["dates"]["experienced"] = []
                        for entry in evalue:
                            started = datetime.datetime.fromisoformat(entry[0]).date()
                            new = {"started": started}
                            if len(entry) == 1:
                                new["finished"] = copy.copy(started)
                            else:
                                if entry[1]:
                                    new["finished"] = datetime.datetime.fromisoformat(
                                        entry[1]
                                    ).date()
                            new_data["dates"]["experienced"].append(new)
                    elif ekey == "published":
                        pass
                    elif ekey in ("number", "chrono"):
                        continue
                    elif ekey == "alt":
                        new_data["titles"].append(evalue.strip())
                    elif ekey == "links":
                        new_data["links"] = [
                            link.strip() for link in evalue
                        ]
                    elif ekey == "active":
                        new_data["active"] = bool(evalue)
                    elif ekey == "language":
                        new_data["taxonomies"]["languages"].append(evalue.strip())
                    elif ekey == "players":
                        new_data["players"] = {"min": evalue[0], "max": evalue[1]}
                    else:
                        print(f"What is extra::{ekey}?")
                        new_data[ekey] = evalue
        elif key == "date":
            if isinstance(value, datetime.datetime):
                new_data["dates"]["published"] = value.date()
            else:
                new_data["dates"]["published"] = value
        elif key == "updated":
            if isinstance(value, datetime.datetime):
                new_data["dates"]["updated"] = value.date()
            else:
                new_data["dates"]["updated"] = value
        elif key == "taxonomies":
            for ekey, evalue in value.items():
                if evalue:
                    if ekey == "years":
                        for year in evalue:
                            if year != "unreleased":
                                year = int(year)
                            new_data["taxonomies"]["years"].append(year)
                    elif ekey == "series":
                        for series, number in zip(evalue, data["extra"]["number"]):
                            new_data["collections"].append(
                                {"name": series.strip(), "number": number}
                            )
                    elif ekey == "universe":
                        universe = evalue[0].strip()
                        try:
                            if universe in data["taxonomies"]["series"]:
                                universe += " universe"
                        except KeyError:
                            pass
                        new_data["collections"].append(
                            {"name": universe, "number": data["extra"]["chrono"]}
                        )
                    elif ekey == "formats":
                        for tag in evalue:
                            tag = tag.strip()
                            if tag == "computer game":
                                new_data["taxonomies"]["formats"].append("video game")
                            new_data["taxonomies"]["formats"].append(tag)
                    elif ekey == "media":
                        new_data["taxonomies"]["media"]+=(evalue)
                    elif ekey == "artists":
                        new_data["taxonomies"]["artists"]+=(evalue)
                    elif ekey == "tags":
                        places = [
                            "london",
                            "united kingdom",
                            "england",
                            "britain",
                            "america",
                            "united states",
                            "china",
                        ]
                        for tag in evalue:
                            if tag in places:
                                new_data["taxonomies"]["places"].append(tag)
                            else:
                                new_data["taxonomies"]["genres"].append(tag)
                    else:
                        print(f"What is {ekey}?")
                        for tag in evalue:
                            tag = tag.strip()
                            new_data["taxonomies"]["tags"].append(tag)
        elif key == "draft":
            pass
        elif key == "title":
            new_data["titles"].append(value.strip())
        elif key == "description":
            if value:
                new_data["description"] = value.strip()
        else:
            print(f"What is {key}?")
            if value:
                if isinstance(value, str):
                    new_data[key] = value.strip()
                else:
                    new_data[key] = value

    if folder == "posts" and "dates" in new_data and "experienced" not in new_data["dates"]:
        new_data["dates"]["experienced"] = ["?"]

    if "taxonomies" in data:
        new_data["taxonomies"] = dict(new_data["taxonomies"])

    return new_data


def recurse_dirs(from_path, to_path):
    for entry in from_path.iterdir():
        if entry.is_dir():
            recurse_dirs(entry, to_path)

        elif entry.suffix == ".md":
            folder = "/".join(str(entry.parent).split("/")[4:])

            with open(entry, "r") as f:
                raw = f.read()

            if raw[:3] != "+++":
                raise Exception("Content file format unrecognised")

            toml, markdown = raw.split("+++")[1:]

            old_metadata = tomli.loads(toml)

            metadata = restructure(old_metadata, folder)

            yamler = ruamel.yaml.YAML(pure=True)

            new_folder = to_path / (
                "entries" if folder in ("posts", "todo") else folder
            )
            new_folder.mkdir(parents=True, exist_ok=True)
            with open(new_folder / entry.name, "w") as f:
                f.write("---\n")
                yamler.dump(metadata, f)
                f.write("---")
                f.write(markdown)


if __name__ == "__main__":
    dest = pathlib.Path("../content")
    dest.mkdir(parents=True, exist_ok=True)

    recurse_dirs(pathlib.Path("../../musings/content"), dest)
