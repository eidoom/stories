import Entries from "./Entries.svelte";

const app = new Entries({
  target: document.getElementById("app"),
});

export default app;
