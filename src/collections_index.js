import Collections from "./Collections.svelte";

const app = new Collections({
  target: document.getElementById("app"),
});

export default app;
