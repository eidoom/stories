export { slugify, capitalise, date_format };

function slugify(string) {
  return string
    .toString()
    .trim()
    .toLowerCase()
    .replace(/\s+/g, "-")
    .replace(/[^\w\-]+/g, "")
    .replace(/\-{2,}/g, "-");
}

function capitalise(string) {
  return string.toLowerCase().replace(/^\w/, (c) => c.toUpperCase());
}

const date_format = {
  full: {
    day: "numeric",
    month: "short",
    year: "numeric",
  },
  day_month: {
    day: "numeric",
    month: "short",
  },
  day: {
    day: "numeric",
  },
};
